/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author schubert
 */
public class ReadList {
    ArrayList<ArrayList<String>> input0;

    public ArrayList<ArrayList<String>> ReadList(String pathname, String filename, int userdef) {
        BufferedReader reader =null;
        InputStream in=null;
        input0 = new ArrayList<>();
        //input0 = null;
        //StringBuffer fileparam = new StringBuffer();
        String text = "";
        String locname="";
        //double[][] list0= new double[6][4];
        String numb;
        int il=0;
        
        int counter = 0;
        
        String path=null;
        
        String target_file=null;  // fileThatYouWantToFilter
        URL res = getClass().getResource("");
        
        /*if (userdef==0){
            for (int i = 0; i < filelist.length; i++) {
                target_file=filelist[i];
                //System.out.println("filelist "+i+": "+target_file);
                if (target_file.matches(filename)) {
                //You can add these files to fileList by using "list.add" here
                     //DebugInfos.append("found" + " " + target_file+"\n");
                     locname=pathname+target_file;
                }
            }
        } else {
            locname=pathname+filename;
        }*/
        
        locname=pathname+filename;
        //System.out.println("debug --- plotXY: locname = "+locname);
        // Scanner in = new Scanner(new FileReader("C:\\Users\\ellwarth\\Documents\\VTF\test_list.txt"));
        try {
            //reader = new BufferedReader(new FileReader(locname));
            if (VTF_IPC_GUI.standalone && userdef==0){
                //System.out.println("startswith jar");
                in=getClass().getResourceAsStream(locname);
                reader=new BufferedReader(new InputStreamReader(in));
            }else{
                reader = new BufferedReader(new FileReader(locname));
            }
            try {
                while ( (text=reader.readLine()) != null){
                    counter+=1;
                    //if (res.toString().startsWith("jar"))
                    //System.out.println("readout: "+text+"   - line "+Integer.toString(counter));
                    //DebugInfos.append(text+"\n "+Integer.toString(counter)+"\n");
                    //System.out.println("else 3");
                    String[] parts = text.split("\\t");
                    int dimp=parts.length;
                    input0.add(new ArrayList<>());
                    
                    for (int j = 0; j < dimp; j++) {
                        //numb = Double.parseDouble(parts[j]);
                        numb = parts[j];
                        input0.get(il).add(String.valueOf(numb));
                    }
                    il +=1;
                    //System.out.println("row (input): "+Integer.toString(il)+"\n");
                }
                reader.close();
                if (VTF_IPC_GUI.standalone && userdef==0){
                    //System.out.println("else 4");
                    in.close();
                    //System.out.println("else 5");
                }
            } catch(IOException e) {
                e.printStackTrace(); 
            }
        } catch(FileNotFoundException fnfe) {
             //System.out.println(fnfe.getMessage());
             VTF_IPC_GUI.out2.append(fnfe.getMessage()+ "\n");
             VTF_IPC_GUI.out2.append(System.getProperty("line.separator"));
        }
        return input0;
    }

}
