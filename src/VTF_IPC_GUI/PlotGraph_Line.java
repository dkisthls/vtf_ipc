/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author schubert
 */
public class PlotGraph_Line {
    ReadList myReader;
    public void PlotGraph_Line(int lineidx, int tabidx, double[] input,int xs_plot,double plotrange,boolean isCustom){
        myReader = new ReadList();
        //System.out.println("debug --- PlotGraph_Line started");
        init_plot(lineidx,tabidx,input,xs_plot,plotrange,isCustom);
    }
    
    void init_plot(int lineidx, int tabidx, double[] input_val,int xs_plot,double xsp, boolean isCustom){
        double [] scan_grid;
        //double xsp;//plt xrange in nm
        String filepath;
        
        if (!isCustom){
            if (input_val[0]>0.1){
                if (tabidx==2 && VTF_IPC_GUI.udscansteps[lineidx]==1){
                    ArrayList<ArrayList<String>> scangrid_ud= new ArrayList<>();
                    if(VTF_IPC_GUI.udscansteps_files[lineidx].isEmpty()){
                        //System.out.println("no user defined scan steps from file where provided -read in textArea");
                        String[] sg_string=null;
                        sg_string=VTF_IPC_GUI.udss_output;
                        scan_grid=new double[sg_string.length];
                        for(int i=0;i<sg_string.length;i++){
                            if (sg_string[i].isEmpty()){}else
                                scan_grid[i]=Double.parseDouble(sg_string[i]);
                        }
                        System.out.println("msdebug (PLOTGRAPH) ---- length of scan_grid: "+scan_grid.length);
                        //System.out.println("last index of scan_grid: "+scan_grid[scan_grid.length-1]);
                        //scan_grid[0]=0;
                    }else{
                        Path filepath_ud=Paths.get(VTF_IPC_GUI.udscansteps_files[lineidx]);
                        String folder_ud=filepath_ud.toAbsolutePath().toString();
                        int index=folder_ud.lastIndexOf(File.separatorChar);
                        String path_ud=folder_ud.substring(0, index);
                        if (!VTF_IPC_GUI.OpSys.startsWith("Windows")) {
                            path_ud=path_ud.concat("/");
                            //System.out.print("Path variables with Linux/MacOS syntax!!!!\n");
                        } else {
                            path_ud=path_ud.concat("\\");
                        //System.out.print("Path variables with Windows syntax!!!!\n");
                        }
                        String name_ud=filepath_ud.getFileName().toString();
                        System.out.print("msdebug (PLOTGRAPH) ---- file path ud: "+path_ud+"\n");
                        //System.out.print("file name ud: "+name_ud+"\n");
                        scangrid_ud=myReader.ReadList(path_ud,name_ud,1);
                        scan_grid=new double[scangrid_ud.size()];
                        int udscansteps_size=scangrid_ud.size();
                        //System.out.print("scan grid size: "+scangrid_ud.size()+"\n");
                        //System.out.print("first step in grid: "+scangrid_ud.get(lineidx).toString()+"\n");
                        for (int k=0;k<scangrid_ud.size();k++){
                            if(scangrid_ud.get(k).get(0).isEmpty()){    
                            }else{
                                scan_grid[k]=Double.parseDouble(scangrid_ud.get(k).get(0));
                            }
                            //System.out.print("scan steps "+k+": "+scan_grid[k]+"\n");
                        }
                    }
                } else{
                scan_grid=new double[(int)input_val[18]]; //nr scan steps
                    for (int jj=0;jj<((int)input_val[18]);jj++){
                        //scan_grid[jj]=(double)(jj-(scan_grid.length/2))*input_val[3]; //step size
                        scan_grid[jj]=(input_val[2]+jj)*input_val[3];
                    }
                }
                File pfad =new File("");
                if (VTF_IPC_GUI.standalone){
                    filepath="";
                }else{
                    filepath=pfad.getAbsolutePath();
                }
                
                if (VTF_IPC_GUI.Intensity.isSelected()){
                        PlotXYLineScans(input_val[0],input_val[6],input_val[13],scan_grid, xsp, filepath);
                }else{
                    //System.out.println("size of input file: "+scan_grid.length);
                    //for user defined scan steps we put the scan limit to +- 0.25nm
                    PlotXYLineScans(input_val[0],input_val[6],0,scan_grid, xsp, filepath);
                }
            }else{
                //JOptionPane.showMessageDialog(null,"Plot: no wavelength was set! Tabindex/Lineindex "+tabidx+"/"+lineidx,"warning",JOptionPane.WARNING_MESSAGE);
            }
        }else {
           // JOptionPane.showMessageDialog(null,"Custom Mode -> no fts profile available!\n (plot range is limited by the prefilter FWHM)","warning",JOptionPane.WARNING_MESSAGE);
        }
        
        
        
    }
    
    private void PlotXYLineScans(double cwave, double pfFWHM, double waveImode, double [] srange, double xrange_plot, String dirname){
                    double [] xaxis_in, yaxis_in, filter_in;
                    double ymax0;
                    ArrayList<ArrayList<String>> data_in; //array for input values
                    data_in = new ArrayList<>();
                    ymax0=0;
                    double[][] lineprofile_sim, lineprofile_raw, filter_profile;
                    String filefolder;
                    int plotsteps_data0, cnr;
                    cnr=VTF_IPC_GUI.pfcavity_nr;//two cavity PF ->cnr=2 / three cavity PF -> cnr=3
                    
                    plotsteps_data0=(int) (Math.round((xrange_plot*1E-9)/2.d/2E-13)); //deltaxaxis_data0=2mA - lambda_null plus/minus plotsteps
                    
                    //DebugInfos.append("\n central wave: "+cwave+"\n"+"pfFHWM: "+pfFWHM+"\n plot steps (plus/minus) for input data: "+plotsteps_data0+"\n");
                    //DebugInfos.append("Tabindex: "+Integer.toString(Tabidx)+" - Lineindex: "+LineIdx+" -> wavelength= "+Double.toString(cwave)+"\n");
                    
                    if (!VTF_IPC_GUI.OpSys.startsWith("Windows") || VTF_IPC_GUI.standalone) {
                        filefolder=dirname.concat("/config_input/wavelength_intensity_of_FTS_atlas/");
                    } else {
                        filefolder=dirname.concat("\\config_input\\wavelength_intensity_of_FTS_atlas\\");
                    }
                    
                    //find proper file name from wavelength number for readlist()
                    String filename="(.*)"+Integer.toString((int)(cwave*10))+"(.*)";
                    String target_file="";
                    
                    /*String[] filelist=new String[VTF_IPC_GUI.list0.size()];
                    for (int i=0;i<(VTF_IPC_GUI.list0.size());i++){
                        filelist[i]=VTF_IPC_GUI.list0.get(i).get(7);
                    }*/
                    
                    for (int i = 0; i < VTF_IPC_GUI.filelist.length; i++) {
                        target_file=VTF_IPC_GUI.filelist[i];
                        //System.out.println("VTF_IPC_GUI.filelist "+i+": "+target_file);
                        if (target_file.matches(filename)) {
                            //You can add these files to fileList by using "list.add" here
                            //DebugInfos.append("found" + " " + target_file+"\n");
                            filename=target_file;
                        }
                    }
                    
                    //System.out.println("plot line filefolder+name: "+filefolder+filename);
                    data_in=myReader.ReadList(filefolder,filename,0);
                    xaxis_in=new double[data_in.size()];
                    yaxis_in=new double[data_in.size()];
                    filter_in=new double[data_in.size()];
                    
                    for (int j=0;j<data_in.size();j++){
                        xaxis_in[j] = 0.1d*Double.parseDouble(data_in.get(j).get(0)); //wave length grid should be nm but it is in angstrom
                        filter_in[j]=1/(1+Math.pow((2*(0.1d*Double.parseDouble(data_in.get(j).get(0))-cwave))/(pfFWHM),cnr*2));
                        yaxis_in[j] = Double.parseDouble(data_in.get(j).get(1));
                        if (yaxis_in[j] > ymax0)
                            ymax0=yaxis_in[j];                        
                    }
                    
                    //normalize yxaxis to maximum
                    for (int k=0;k<yaxis_in.length;k++)
                        yaxis_in[k]=yaxis_in[k]/ymax0;
                    
                    /*for (int k=0;k<data1.size();k++){
                        xaxis1[k] = data1.get(k).get(0);
                        yaxis1[k] = data1.get(k).get(1);
                    }*/
                    if (VTF_IPC_GUI.Intensity.isSelected()){
                        lineprofile_sim=CalculateLineScan(xaxis_in,yaxis_in,srange,cwave,waveImode,pfFWHM,cnr);
                    } else {
                        lineprofile_sim=CalculateLineScan(xaxis_in,yaxis_in,srange,cwave,0,pfFWHM,cnr);
                    }
                    /*for (int l=0;l<lineprofile_sim.length;l++)
                        DebugInfos.append("y-value of simulated profile: "+String.format("%.3f",lineprofile_sim[l][1])+"\n");*/
                    lineprofile_raw = new double[plotsteps_data0*2+1][2];
                    filter_profile = new double[plotsteps_data0*2+1][2];
                    
                    int waveindex=0;
                    double minval=0.0;
                    minval=yaxis_in[0];
                    //find local minimum to plot around - provided fts atlas data need to have a local minimum within vector_length/2 +- 80
                    for (int i=xaxis_in.length/2-80;i<xaxis_in.length/2+80;i++){
                        if (yaxis_in[i]<minval){
                            waveindex=i;
                            minval=yaxis_in[i];
                        }
                    }
                    //System.out.println("waveindex= "+waveindex+" - wave length= "+cwave + " xaxis_in[5000]= "+xaxis_in[5000]);
                    
                    for (int i=(-1*plotsteps_data0);i<(plotsteps_data0+1);i++){
                        /* lineprofile_raw[i+plotsteps_data0][0]=xaxis_in[(xaxis_in.length/2)+i];
                        filter_profile[i+plotsteps_data0][0]=xaxis_in[(xaxis_in.length/2)+i];
                        lineprofile_raw[i+plotsteps_data0][1]=yaxis_in[(yaxis_in.length/2)+i];
                        filter_profile[i+plotsteps_data0][1]=filter_in[(filter_in.length/2)+i];*/
                        lineprofile_raw[i+plotsteps_data0][0]=xaxis_in[waveindex+i];
                        filter_profile[i+plotsteps_data0][0]=xaxis_in[waveindex+i];
                        lineprofile_raw[i+plotsteps_data0][1]=yaxis_in[waveindex+i];
                        filter_profile[i+plotsteps_data0][1]=filter_in[waveindex+i];
                    }
                    
                    XYDataset dat1 = createXYDataset(lineprofile_sim,"simulated line scan",0);
                    XYDataset dat2 = createXYDataset(lineprofile_raw,"FTS profile",0);
                    XYDataset dat3 = createXYDataset(filter_profile,"prefilter profile",0);
                    
                    NumberAxis xaxis=new NumberAxis("wavelength in nm");
                    xaxis.setAutoRangeIncludesZero(false);
                    NumberAxis yaxis=new NumberAxis("intensity");
                    yaxis.setRange(0,1.2);
                    
                    XYPlot plot1=new XYPlot(dat1,xaxis,yaxis,null); //fts profile
                    plot1.setDataset(2, dat3); // PF profile
                    plot1.setDataset(1, dat2); //simulated line scan
                    
                    XYItemRenderer renderer1 = new XYLineAndShapeRenderer(false, true); //scattered or line plot, or both
                    XYItemRenderer renderer2 = new XYLineAndShapeRenderer(true,false);//scattered or line plot, or both
                    XYItemRenderer renderer3 = new XYLineAndShapeRenderer(true,false);//scattered or line plot, or both
                    
                    plot1.setRenderer(0,renderer1);
                    plot1.setRenderer(1, renderer2);
                    plot1.setRenderer(2,renderer3);
                    
                    //JFreeChart chart;
                    JFreeChart chart = new JFreeChart("Simulated Line Scan: "+Double.toString(cwave)+" nm", JFreeChart.DEFAULT_TITLE_FONT, plot1, true);
                    
                    //JPanel chart_panel;
                    JPanel chart_panel=new ChartPanel(chart);
                    chart_panel.setSize(VTF_IPC_GUI.graphicWindow.getSize());
                    chart_panel.revalidate();
                    VTF_IPC_GUI.graphicWindow.removeAll();
                    VTF_IPC_GUI.graphicWindow.add(chart_panel);
                    VTF_IPC_GUI.graphicWindow.getParent().validate();
                    chart_panel.setVisible(true);
                    chart.fireChartChanged();
                   //System.out.println("debug --- plotXY: graph plotted");
                    
                    
    }
    
    private double[][] CalculateLineScan(double[] xax, double[] yax, double[] scanrange0, 
        double centralwave0, double waveImode, double filterFWHM, double PFcav_nr){
        //!!! xax, centralwave and filterFWHM must be in nm !!!
        // ***** parameters for FPIs to do line scan ****
        // VTF =>  FPI1:d1=0.00055m - r1=95% - FPI2: e2=0.389 - r2=84.0% - FPI3: e3=0.270 - r3=84%
        
        double refFPI1=0.95, gapFPI1=0.00055, refFPI2=0.84, gapFPI2=0.389*0.00055, transmax;
        double  transmax0, transparency,FF,Fall, Ftheta,Fref, gap1,order1;
        double[] yline, ytrans, yfilter, alltransfct, scangrid;
        double[][] linescan, filterfct, transfct;
        double cwave,cfwave;
        
        if (VTF_IPC_GUI.Intensity.isSelected()){
            cwave=waveImode;
            cfwave=centralwave0;
        } else{
            cwave=centralwave0;
            cfwave=cwave;
        }
        //int cnr=4; // two cavity PF -> 2*n for n=2
        int fnr=200;
        
        Ftheta=(cwave*1E-9)/(gapFPI1*Math.pow(1.d/(2.d*fnr),2)); // for fnr=200
        //DebugInfos.append("Finesse factor angle: "+Ftheta+"\n");
        Fref=Math.PI*Math.sqrt(refFPI1)/(1-refFPI1);
        //DebugInfos.append("Finesse factor reflectivity: "+Fref+"\n");
        Fall=1/(Math.sqrt(1/Math.pow(Fref, 2)+1/Math.pow(Ftheta,2)));
        FF=Math.pow(Fall*2.D/Math.PI,2);
        //DebugInfos.append("Finesse factor all: "+FF+"\n");
        
        //maximum transparency for the instrument
        transparency=1-(0.002/(1-refFPI1)); // absorption = 0.002
        
        // gap1 for wave length - mode lock: -> for FPIs calculate order2,gap2,...
        order1=Math.rint(2.d*gapFPI1/(cwave*1E-9));
        gap1=(cwave*1E-9)*order1/2.d;
        //DebugInfos.append("\n locked order: "+order1+"\n"+"locked gap1: "+gap1+"\n");
        
        scangrid=new double[scanrange0.length];
       //System.out.println("debug --- scanrange:\n"+Arrays.toString(scanrange0));
        for (int ll=0;ll<scanrange0.length;ll++){
            scangrid[ll]=(1E-12)*scanrange0[ll]*gap1/(cwave*1E-9);//scanrange in pm, central wave in nm, gap in m
            //DebugInfos.append("delta gap: "+scangrid[ll]+" for number: "+ll+"\n");
        }
        
        filterfct=new double[xax.length][2];
        transfct=new double[xax.length][2];
        yfilter=new double[xax.length];
        ytrans=new double[xax.length];
        
        alltransfct=new double[xax.length];
        linescan=new double[scanrange0.length][2];
        yline=new double[scanrange0.length];
        
        double trmaxv=0;
        int cc=0; //for approximation of fwhm
        
        transmax=0.d;
        double filtertrans=1.0;
        for (int kk=0;kk<xax.length;kk++){
            yfilter[kk]=filtertrans/(1+Math.pow((2*(xax[kk]-cfwave))/(filterFWHM),PFcav_nr*2));
            //centralwave and filterFWHM in nm!!
        }
        //dummy scan for normalization
        double [] dummytrans;
        dummytrans=new double[xax.length];
        double gapint;
        order1=Math.rint(2.d*gapFPI1/(cfwave*1E-9));
        gapint=(cfwave*1E-9)*order1/2.d;
        transmax0=0;
        for (int i=0;i<xax.length;i++){
            dummytrans[i]=Math.pow(transparency,2)/(1+FF*Math.pow(Math.sin(2*Math.PI*(gapint)/(xax[i]*1E-9)),2));
            transmax0+=dummytrans[i]*yfilter[i];
        }
        
        for (int mm=0;mm<scanrange0.length;mm++){
            //transmax0=0.d;
            if (VTF_IPC_GUI.Intensity.isSelected()){
                // gap1 for wave length - mode lock: -> for FPIs calculate order2,gap2,...
                order1=Math.rint(2.d*gapFPI1/(cfwave*1E-9));
                gapint=(cfwave*1E-9)*order1/2.d;
                for (int k=0;k<xax.length;k++){
                    ytrans[k]=0.d;
                    ytrans[k]=Math.pow(transparency,2)/(1+FF*Math.pow(Math.sin(2*Math.PI*(gapint)/(xax[k]*1E-9)),2));
                    //transmax0+=yfilter[k]*ytrans[k];
                }
            }
            for (int k=0;k<xax.length;k++){
                ytrans[k]=0.d;
                //ytrans[k]=Math.pow(1-refFPI1-0.002,2)/(Math.pow((1-refFPI1),2)+4*Math.pow(refFPI1,2)*Math.pow(Math.sin(2*Math.PI*(gap1+scangrid[mm])/(xax[k]*1E-10)),2));
                //xaxis in angstrom!!!
                ytrans[k]=Math.pow(transparency,2)/(1+FF*Math.pow(Math.sin(2*Math.PI*(gap1+scangrid[mm])/(xax[k]*1E-9)),2));
                alltransfct[k]=0.d;               
                /*if (!VTF_IPC_GUI.Intensity.isSelected() && scanrange0[mm]==0){
                    transmax0+=yfilter[k]*ytrans[k];
                }*/
                
                alltransfct[k]=(yfilter[k]*ytrans[k]*yax[k]);
                //if ((yfilter[k]*ytrans[k]) > trmaxv)
                //    trmaxv=yfilter[k]*ytrans[k];
                //if ((ytrans[k]) > trmaxv)
                //    trmaxv=ytrans[k];
            }
            //DebugInfos.append("trans_tot: "+transmax0+"\n");
            
            if (transmax<transmax0) {
                transmax=transmax0;
            }
            
            yline[mm]=0;
            for (int jj=0;jj<xax.length;jj++){                
                yline[mm]+=alltransfct[jj];
            }
            //DebugInfos.append("y-value: "+yline[mm]+"\n");
            
            if (mm==scanrange0.length/2){
                cc=0;
                for (int k=0;k<xax.length;k++){
                    transfct[k][0]=xax[k];
                    filterfct[k][0]=xax[k];
                    filterfct[k][1]=yfilter[k];
                    transfct[k][1]=ytrans[k]*yfilter[k];
                    if ((transfct[k][1])>(0.5*trmaxv)){
                        cc++;
                    }
                }
            }
        }
        
        for (int j=0;j<scanrange0.length;j++){
           linescan[j][1]=yline[j]/transmax;
           linescan[j][0]=(1E-3)*scanrange0[j]+cwave;
           //System.out.println("linescan x = "+linescan[j][0]);
        }
        return linescan;
    }
    
    private XYDataset createXYDataset(double [][] input_line1, String datname1, int normalize) {
        
        //the plot is normalized to line profile continuum
        XYSeriesCollection dataset=new XYSeriesCollection(); 
        XYSeries data1;
        data1 = new XYSeries(datname1);
        double valmax=1.d;
        
        if (normalize==1){
            for (double[] input_line : input_line1) {
                if (valmax < input_line[1]) {
                    valmax = input_line[1];
                }
            }
        }
        
        if (input_line1 != null){
            for (double[] input_line : input_line1) {
                data1.add(input_line[0], input_line[1] / valmax);
                //DebugInfos.append("y-wert:" + Double.toString(y[k]));
            }
        }
              
        if (input_line1 != null)
            dataset.addSeries(data1);
        
        return dataset;
    }

}
