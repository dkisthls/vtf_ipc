/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;
/**
 *
 * @author schubert
 */
public class CalculateSNR_Line {
    int result;
    public int CalculateSNR_Line(boolean isCustom, int wl_index, double[] input, double lightLevel){
        result=0;
        if(!isCustom){
            if(input[0]>1.0 && input[1]>0 && input[4]>0 && input[9]>0 && input[15]>0){
                //result=(int)Math.sqrt(Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText())*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(5))/0.75d)*input[9]*(input[1]/25d)*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(2))*2*input[4]);//SNR
                result=(int)Math.sqrt(lightLevel*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(5))/0.75d)*input[9]*(input[1]/25d)*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(2))*2*input[4]);//SNR
               //System.out.println("Parameter:\nlist0[5]="+VTF_IPC_GUI.list0.get(wl_index).get(5)+"\nlist0[wl_idx]="+VTF_IPC_GUI.list0.get(wl_index).get(2)+"\nlightLevel="+Double.toString(lightLevel));
               //System.out.println("input: "+input[0] +" - "+input[1]+" - "+input[4]+" - "+input[9]);
            }else{
               System.out.println("not enough parameter to calculate SNR!\n(set proper wavelength, exp. time, acquisitions and binning)");
            }
        }
       //System.out.println("SNR result = "+result);
        return result;
    }
}
