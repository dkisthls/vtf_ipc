/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author schubert
 */
public class UserEditor  extends JFrame implements ActionListener{
    private int lineidx;
    private int tabidx;
        
    private final JButton saveCloseB=new JButton("Save File and Close");
    private final JButton applyB=new JButton("Apply Scan Steps");
    private final JButton loadB=new JButton("Load Existing File"); 
    private final JButton resetB=new JButton("Reset and Close");
    private final JFrame frame_edit=new JFrame();
    private final JTextArea textArea=new JTextArea();
    String filename_all=null;
    //public String[] udss_readout=null;
    ArrayList<ArrayList<String>> inputarray;
    ReadList myReader;
    CalculateTiming_Line calcT;
    CalculateTiming_Tab calcTab;
    PlotGraph_Line plot;
    double[] inval;
    JTextField userText,cycleTime;
    
    public void UserEditor(int lindex, int tindex, String filename_all,double[] input, JTextField userT, JTextField cycT){
        userText=userT;
        cycleTime=cycT;
        tabidx=tindex;
        lineidx=lindex;
        inval=input;
        myReader=new ReadList();
        calcT=new CalculateTiming_Line();
        calcTab=new CalculateTiming_Tab();
        plot=new PlotGraph_Line();
        loadB.addActionListener(this);
        applyB.addActionListener(this);
        saveCloseB.addActionListener(this);
        resetB.addActionListener(this);
        frame_edit.setTitle("User Defined Scan Steps for Line "+(lineidx+1));
        
        //applyB.setEnabled(false);
        panels(input,filename_all,userText,cycT);
            
        //if already defined user scan steps, display applied steps for manipulation
            
        //filename_all=readUserSteps(lineidx);
        System.out.println("length of filename "+filename_all+":\n "+filename_all.length());
        if (filename_all.equals("no user steps")){
            for (int i=0;i<11;i++){
                if (i==0){
                    textArea.setText(Integer.toString((i-5)*4)+"\n");
                }else{
                    textArea.append(Integer.toString((i-5)*4)+"\n");
                }
            }
        }else{
                inputarray = new ArrayList<>();
                String directory=filename_all.substring(0, filename_all.lastIndexOf(File.separator)+1);
                String name=filename_all.substring(filename_all.lastIndexOf(File.separator)+1, filename_all.length());
                VTF_IPC_GUI.udscansteps_files[lineidx]=filename_all;
                VTF_IPC_GUI.udscansteps[lineidx]=1;
                inputarray=myReader.ReadList(directory,name,1);
                for (int i=0;i<inputarray.size();i++){
                    if (i==0){
                        textArea.setText(inputarray.get(i).toString().replaceAll("\\[|\\]", "")+"\n");
                    }else{
                        textArea.append(inputarray.get(i).toString().replaceAll("\\[|\\]", "")+"\n");
                    }
                }
            }
        
        }
        
        private void panels(double[] input, String filename_all, JTextField userT, JTextField cycT){
            JPanel panel = new JPanel(new GridLayout(1,1));
            panel.setBorder(new EmptyBorder(5, 5, 5, 5));
            JPanel rightPanel = new JPanel(new GridLayout(15,0,10,10));
            rightPanel.setBorder(new EmptyBorder(15, 5, 5, 10));
            JScrollPane scrollBarForTextArea=new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            panel.add(scrollBarForTextArea); 
            frame_edit.add(panel);
            frame_edit.getContentPane().add(rightPanel,BorderLayout.EAST);
            rightPanel.add(loadB);
            rightPanel.add(applyB);
            rightPanel.add(saveCloseB);
            rightPanel.add(resetB);
            //frame_edit.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            frame_edit.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            frame_edit.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we){
                    String ObjButtons[] = {"Yes","No"};
                    int PromptResult = JOptionPane.showOptionDialog(null,"EXIT WITHOUT SAVING?\n","User Defined Scan Steps",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
                    //System.out.println("JOptionePane Result: "+PromptResult);
                    if(PromptResult==0){
                        System.out.println("debug --- filename "+filename_all);
                        if(filename_all.equals("no user steps"))
                            VTF_IPC_GUI.udscansteps[lineidx]=0;
                        else
                            VTF_IPC_GUI.udscansteps_files[lineidx]=filename_all;
                        //System.out.println("indices: Tab="+tabidx+" Line="+lineidx);
                        boolean isAdvanced=true;
                        double plotrange=checkPlotRange_uds(input, 0, VTF_IPC_GUI.udss_output);
                        plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,false);                       
                        frame_edit.dispose();
                    }
                }
            });
            frame_edit.setSize(400, 600);
            frame_edit.setVisible(true);   
            //frame_edit.setLocationRelativeTo(null);
        }
        
        @Override
        public void actionPerformed(ActionEvent e){
            if (e.getSource()==loadB){
                File pfad = new File("");
                String startdir;
                startdir = pfad.getAbsolutePath();
                //System.out.println("Start directory for file chooser is: "+startdir);
                JFileChooser filechooser = new JFileChooser(startdir);
                filechooser.showOpenDialog(null);
                if (filechooser.getSelectedFile().exists()){
                    inputarray = new ArrayList<>();
                    File file = filechooser.getSelectedFile();
                    filename_all = file.getAbsolutePath();
                    //applyB.setEnabled(true);
                    String directory=filename_all.substring(0, filename_all.lastIndexOf(File.separator)+1);
                    String name=filename_all.substring(filename_all.lastIndexOf(File.separator)+1, filename_all.length());
                    //System.out.println("directory: "+directory+"  - name: "+name);
                    //setUserSteps(lineidx,filename_all);
                    userText.setText(filename_all);
                    inputarray=myReader.ReadList(directory,name,1);
                    for (int i=0;i<inputarray.size();i++){
                        if (i==0){
                            textArea.setText(inputarray.get(i).toString().replaceAll("\\[|\\]", "")+"\n");
                        }else{
                            textArea.append(inputarray.get(i).toString().replaceAll("\\[|\\]", "")+"\n");
                        }
                    }
            }
        }
        if (e.getSource()==saveCloseB){
            if(textArea.getText().equals("no user steps")){
                //System.out.println("no user scan steps defined in editor!!!\n");
                JOptionPane.showMessageDialog(null,"no user scan steps defined in editor!","warning",JOptionPane.WARNING_MESSAGE);
                VTF_IPC_GUI.udscansteps[lineidx]=0;
                VTF_IPC_GUI.udscansteps_files[lineidx]="";
                //textArea.append("no user scan steps defined in editor!!!\n");                    
            }else{
                JFileChooser chooser = new JFileChooser();
                int retrival = chooser.showSaveDialog(null);
                if (retrival == JFileChooser.APPROVE_OPTION) {
                    try {
                        //System.out.println("file to save: "+chooser.getSelectedFile().toString());
                        /*
                        FileWriter fw = new FileWriter(chooser.getSelectedFile());
                        //filename_all=chooser.getSelectedFile().toString();
                        fw.write(textArea.getText());
                        fw.flush();
                        fw.close();
                        frame_edit.dispose();
                        */
                        VTF_IPC_GUI.udscansteps_files[lineidx]=chooser.getSelectedFile().toString();
                        VTF_IPC_GUI.udscansteps[lineidx]=1;
                        //__________  setUserSteps(lineidx,chooser.getSelectedFile().toString());
                        String udscansteps_s=textArea.getText();
                        String[] list0=udscansteps_s.split("\n");
                        String[] list0r;
                        int count0=0;
                        for (String list01 : list0) {
                            if (list01.isEmpty() || !isNumber(list01)) {
                                System.out.println("msdebug (USEREDITOR) --- empty String or not a number");
                            } else {
                                System.out.println("msdebug (USEREDITOR) --- number String = "+list01);
                                count0++;
                            }
                        }
                        list0r=new String[count0];
                        count0=0;
                        for (String list01 : list0) {
                            if (list01.isEmpty() || !isNumber(list01)) {
                            } else {
                                list0r[count0] = list01;
                                count0++;
                            }
                        }
                        FileWriter fw = new FileWriter(chooser.getSelectedFile());
                        //filename_all=chooser.getSelectedFile().toString();
                        fw.write(Arrays.toString(list0r).replace(",", "\n").replace("[", "").replace("]", ""));
                        fw.flush();
                        fw.close();
                        frame_edit.dispose();
                        
                        VTF_IPC_GUI.udss_output=list0r;
                        
                        //VTF_IPC_GUI.udss_output=udscansteps_s.split("\n");
                        userText.setText(chooser.getSelectedFile().toString());
                        
                        //setScanSteps_uds(lineidx,Integer.toString(udss_output.length));
                        //System.out.println("indices: Tab="+tabidx+" Line="+lineidx);
                        boolean isAdvanced=true;
                        double[] results_calc = calcT.CalculateTiming_Line(inval,lineidx);
                        inval[11]=results_calc[0];
                        VTF_IPC_GUI.time[lineidx]=results_calc[0];
                        VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
                        cycleTime.setText(Double.toString(results_calc[0]));
                        //System.out.println("UserEditor input params: time/space =\ntime: "+Arrays.toString(VTF_IPC_GUI.time)+"\nspace: "+Arrays.toString(VTF_IPC_GUI.dspace)+"\n params: "+VTF_IPC_GUI.repeatsNumb_text.getText()+", "+ VTF_IPC_GUI.delayObs_text.getText());
                        double[] resultTab;
                        resultTab = calcTab.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                        int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                        String roundRes=Double.toString(((double)roundval)/100.d);
                        double nachkomma=(resultTab[0]*100.d)%100;
                        VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                        VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
                        System.out.println("msdebug (USEREDITOR) ------ save/exit old values: "+Arrays.toString(inval));
                        
                        double plotrange=checkPlotRange_uds(inval, 0, VTF_IPC_GUI.udss_output);
                        plot.PlotGraph_Line(lineidx,tabidx,inval,0,plotrange,false);                       
                        
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        if (e.getSource()==applyB){
            if(textArea.getText()!=""){
                //frame_edit.dispose();
                String udscansteps_s=textArea.getText();
                String[] scansteps=udscansteps_s.split("\n");
                VTF_IPC_GUI.udss_output=udscansteps_s.split("\n");
                //System.out.println("textArea for lineidx "+lineidx+" is: \n"+VTF_IPC_GUI.udscansteps_s);
                //System.out.println("textArea [3] for lineidx "+lineidx+" is: \n"+udss_output[3]);
                VTF_IPC_GUI.udscansteps_files[lineidx]="";
                VTF_IPC_GUI.udscansteps[lineidx]=1;
                //setUserSteps(lineidx,filename_all);
                double plotrange=checkPlotRange_uds(inval, 0, VTF_IPC_GUI.udss_output);
                plot.PlotGraph_Line(lineidx,tabidx,inval,0,plotrange,false);                       
                        
            }else{
                //System.out.println("no file loaded!");
                JOptionPane.showMessageDialog(null,"no input data avalaible!","warning",JOptionPane.WARNING_MESSAGE);
            }
        }
        if (e.getSource()==resetB){
            frame_edit.dispose();
            VTF_IPC_GUI.udscansteps_files[lineidx]="";
            VTF_IPC_GUI.udscansteps[lineidx]=0;
            filename_all="no user steps";
            userText.setText(filename_all);
            //----------   setUserSteps(lineidx,"");
            boolean isAdvanced=true;
            //old scan steps where stored in uneditable Textfields....
            if (inval[2]<inval[17])
                inval[18]=inval[17]-inval[2]+1;
            else
                inval[18]=-inval[17]+inval[2]+1;
            double[] results_calc = calcT.CalculateTiming_Line(inval,lineidx);
            inval[11]=results_calc[0];
            VTF_IPC_GUI.time[lineidx]=results_calc[0];
            VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
            cycleTime.setText(Double.toString(results_calc[0]));
            //System.out.println("UserEditor input params: time/space =\ntime: "+Arrays.toString(VTF_IPC_GUI.time)+"\nspace: "+Arrays.toString(VTF_IPC_GUI.dspace)+"\n params: "+VTF_IPC_GUI.repeatsNumb_text.getText()+", "+ VTF_IPC_GUI.delayObs_text.getText());
            double[] resultTab;
            resultTab = calcTab.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
            int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
            String roundRes=Double.toString(((double)roundval)/100.d);
            double nachkomma=(resultTab[0]*100.d)%100;
            VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
            VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
            //System.out.println("msdebug (USEREDITOR) ------ resetLine old values: "+Arrays.toString(inval));
                        
            double plotrange=checkPlotRange_uds(inval, 0, VTF_IPC_GUI.udss_output);
            plot.PlotGraph_Line(lineidx,tabidx,inval,0,plotrange,false);
        }
    }

    public static boolean isNumber(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public double checkPlotRange_uds(double[] input, double xs_plot, String[] udssout){
        double plotrange;
        int wl_index=VTF_IPC_GUI.get_wavelength_nr(String.valueOf(input[0]));
        //maximum plot range is 2nm
        
        double[] minmax_sg=new double[2];
        double[] xvalues=new double[udssout.length];
        int count=0;
        for(String name:udssout){
            if(name.length()>0){
                xvalues[count]=Double.parseDouble(name);
                count++;
            }
        }
        minmax_sg=VTF_IPC_GUI.minmax(xvalues);
        double maxplot;
        if(Math.abs(minmax_sg[0])>Math.abs(minmax_sg[1]))
            maxplot=Math.abs(minmax_sg[0]);
        else
            maxplot=Math.abs(minmax_sg[1]);
        
        if (xs_plot!=0 && xs_plot<2.d){
            plotrange=xs_plot;
        }else{
            if(0.002*maxplot>input[6])
                plotrange=0.002*maxplot;
            else
                plotrange=input[6];
        }    
        
        if ((minmax_sg[0]<(-250.0)) || (minmax_sg[1]>(250.0))){
            if (VTF_IPC_GUI.udscansteps[lineidx]==1){
                //VTF_IPC_GUI.setUserSteps(lineidx,"");
                VTF_IPC_GUI.udscansteps[lineidx]=0;
            }
            JOptionPane.showMessageDialog(null,"Polarimetric/DI for user defined scan steps: Plot is out of range!\n(Plot range is dx = "+input[0]+"+/- 0.25 nm)\nReset to default values!","warning",JOptionPane.WARNING_MESSAGE);        
        }else{
            //minmax is in pm - plotrange is in nm -> *2/1000
            double maxabs=0;
            if(Math.abs(minmax_sg[1])>Math.abs(minmax_sg[0]))
                maxabs=Math.abs(minmax_sg[1]);
            else
                maxabs=Math.abs(minmax_sg[0]);
            System.out.println("debug --- plot range = "+0.0002*maxabs+"nm");
            if (plotrange<0.002*maxabs){
                plotrange=0.002*maxabs;
            }else{
                if (xs_plot!=0 && xs_plot<2.d){
                    plotrange=xs_plot;
                }
            }
            //pm to nm and factor 2 for symmetric plot
            //System.out.println("treshhold plot uds = "+ plotrange);
        }
        return plotrange;
    }    

}
