/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

//import static VTF_IPC_GUI.VTF_IPC_GUI.out2;
import static VTF_IPC_GUI.VTF_IPC_GUI.myblue;
import static VTF_IPC_GUI.VTF_IPC_GUI.mydarkgreen;
import static VTF_IPC_GUI.VTF_IPC_GUI.mygray;
import static VTF_IPC_GUI.VTF_IPC_GUI.myyellow;
import static VTF_IPC_GUI.VTF_IPC_GUI.out2;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

/**
 *
 * @author schubert
 */
public class LineBody extends JPanel implements ActionListener, FocusListener, ItemListener{
    private JPanel linePanel;
    private JComboBox<String> selectLine;
    private JComboBox<String> selectScanMode;
    protected JComboBox<String> selectBinning;
    protected JComboBox<String> selectROI;
    public JTextField waveName;
    protected JTextField expTime;
    private JTextField scanLeft;
    private JTextField scanRight;
    private JTextField scanSum;
    private JTextField specStep;
    private JComboBox<String> selectspecStep;
    private JTextField acquisitions;
    private JTextField snr;
    private JTextField imScanPos;
    private JTextField iterations;
    private JTextField lineTime;
    private JTextField userdefSteps;
    private JButton resetB;
    
    CalculateTiming_Line calcTimeL;
    CalculateTiming_Tab calcTimeT;
    CalculateSNR_Line calcSNR;
    CalculateExpTime_Line calcET;
    UserEditor ue;
    DocumentListener myDocumentListener;
    
    private int lineidx, tabidx;
    private double[] results_calc;
    private double[] resultTab;
    boolean isActive=false;
    boolean isCustom=false;
    PlotGraph_Line plot;
    //VTF_IPC_GUI newInst;
    
    public LineBody(int lineNr, int tabNr){
        calcTimeL=new CalculateTiming_Line();
        calcTimeT=new CalculateTiming_Tab();
        calcSNR= new CalculateSNR_Line();
        calcET = new CalculateExpTime_Line();
        plot = new PlotGraph_Line();
        lineidx=lineNr;
        tabidx=tabNr;
        results_calc=new double[2];
        resultTab=new double[2];
        myDocumentListener=new DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                System.out.println("msdebug (LINEBODY) ----  DocumentListener - insertUpdate");
                String change="";
                try {
                    change = e.getDocument().getText(e.getOffset(), e.getLength());
                    if("no user steps".equals(userdefSteps.getText())){
                        scanLeft.setEditable(true);
                        scanLeft.setBackground(VTF_IPC_GUI.myyellow);
                        scanRight.setEditable(true);
                        scanRight.setBackground(VTF_IPC_GUI.myyellow);
                        System.out.println("msdebug (LINEBODY) ----- DocumentListener scanSum="+Integer.toString(1+Integer.parseInt(scanRight.getText())-Integer.parseInt(scanLeft.getText())));
                        scanSum.setText(Integer.toString(Integer.parseInt(scanRight.getText())-Integer.parseInt(scanLeft.getText())+1));
                        
                        methodActionLost_scanSteps();
                        printChange(e);
                    }else{
                        System.out.println("DocumentListener - Text changed -> userdefined steps were set!");
                        scanLeft.setEditable(false);
                        scanLeft.setBackground(VTF_IPC_GUI.mydarkgreen);
                        scanRight.setEditable(false);
                        scanRight.setBackground(VTF_IPC_GUI.mydarkgreen);
                        if(VTF_IPC_GUI.udss_output!=null)
                            scanSum.setText(Integer.toString(VTF_IPC_GUI.udss_output.length));
                        printChange(e);
                    }
                } catch (BadLocationException ex) {
                    Logger.getLogger(LineBody.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                System.out.println("msdebug (LINEBODY) ----  DocumentListener - removeUpdate");
                //printChange(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                System.out.println("msdebug (LINEBODY) ----  DocumentListener - changedUpdate");
                printChange(e);
            }

            private void printChange(DocumentEvent e){
                try {
                    System.out.println("DocumentListener - Text changed to:"+e.getDocument().getText(e.getOffset(), e.getLength()));
                } catch (BadLocationException ex) {
                    Logger.getLogger(LineBody.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

    };
        initPanel(lineidx,tabidx);
    }

    private void initPanel(int lineNr, int tabNr){
        //newInst = new VTF_IPC_GUI();
        VTF_IPC_GUI.Polar.addItemListener(this);
        VTF_IPC_GUI.Doppler.addItemListener(this);
        VTF_IPC_GUI.Intensity.addItemListener(this);

        lineidx=lineNr;        
        linePanel = new JPanel();
        linePanel.setName("linePanel"+lineNr+"_"+tabNr);
        Dimension dim0;
        dim0=linePanel.getPreferredSize();
        linePanel.setPreferredSize(new Dimension(135,467));
        linePanel.addFocusListener(this);
        GridBagLayout gridB = new GridBagLayout();
        GridBagConstraints cons = new GridBagConstraints();
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.insets = new Insets(4,0,1,0);
        //gridB.columnWeights = new double[] {0.3, 0.3, 0.4};
        linePanel.setLayout(gridB);
        
        JLabel lineN = new JLabel("Line "+Integer.toString(lineidx+1));
        lineN.setName("lineLabel");
        setLayouts(lineN,linePanel,0,0,3,cons,gridB);
        
        cons.insets = new Insets(1,0,1,0);
        
        selectLine = new JComboBox<String>();
        selectLine.setName("selectLine");
        selectLine.addActionListener(this);
        selectLine.addFocusListener(this);
        //selectLine.addPropertyChangeListener(this);
        setLayouts(selectLine,linePanel,0,1,3,cons,gridB);
        selectScanMode = new JComboBox<String>();
        selectScanMode.setName("selectScanMode");
        selectScanMode.addFocusListener(this);
        setLayouts(selectScanMode,linePanel,0,2,3,cons,gridB);
        
        selectBinning = new JComboBox<String>();
        selectBinning.setName("selectBinning");
        //selectBinning.setActionCommand("binningChanged");
        selectBinning.addFocusListener(this);
        //selectBinning.addItemListener(this);
        setLayouts(selectBinning,linePanel,0,3,3,cons,gridB);
        
        selectROI = new JComboBox<String>();
        selectROI.setName("selectROI");
        //selectROI.setActionCommand("roiChanged");
        selectROI.addFocusListener(this);
        //selectROI.addItemListener(this);
        setLayouts(selectROI,linePanel,0,4,3,cons,gridB);
        
        waveName = new JTextField("0");
        waveName.setName("waveName");
        waveName.addFocusListener(this);
        waveName.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        dim0 = waveName.getPreferredSize();
        waveName.setPreferredSize(new Dimension(128,dim0.height));// define the column width for cells of gridbaglayout (it looks for the widest component)
        setLayouts(waveName,linePanel,0,5,3,cons,gridB);
        
        expTime = new JTextField("25.0");
        expTime.setName("expTime");
        //expTime.setEnabled(false);
        expTime.addFocusListener(this);
        expTime.addActionListener(this);
        expTime.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        setLayouts(expTime,linePanel,0,6,3,cons,gridB);
        
        cons.weightx = 0.5;
        scanLeft = new JTextField("0");
        scanLeft.setName("scanLeft");
        //scanLeft.setPreferredSize(new Dimension(36,dim0.height));
        scanLeft.addFocusListener(this);
        scanLeft.addActionListener(this);
        scanLeft.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        cons.gridx=0;
        cons.gridy=7;
        cons.gridwidth=1;
        gridB.setConstraints(scanLeft, cons);
        linePanel.add(scanLeft,cons);

        scanRight = new JTextField("0");
        scanRight.setName("scanRight");
        //scanRight.setPreferredSize(new Dimension(36,dim0.height));
        scanRight.addFocusListener(this);
        scanRight.addActionListener(this);
        scanRight.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        cons.gridx=1;
        cons.gridy=7;
        cons.gridwidth=1;
        //cons.insets.set(5, 0, 5, 0);
        gridB.setConstraints(scanRight, cons);
        linePanel.add(scanRight,cons);

        scanSum = new JTextField("0");
        scanSum.setName("scanSum");
        scanSum.addFocusListener(this);
        scanSum.addActionListener(this);
        scanSum.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        //scanSum.addPropertyChangeListener(this);
        cons.gridx=2;
        cons.gridy=7;
        cons.gridwidth=1;
        //cons.insets.set(5, 0, 5, 10);
        gridB.setConstraints(scanSum, cons);
        linePanel.add(scanSum,cons);
        
        cons.weightx=0.5;
        cons.insets.set(2,0,2,0);
        cons.fill = GridBagConstraints.BOTH;
        
        specStep = new JTextField("0");
        specStep.setName("specStep");
        specStep.addFocusListener(this);
        if (tabNr>0){
            setLayouts(specStep,linePanel,0,8,2,cons,gridB);
            selectspecStep = new JComboBox<String>();
            selectspecStep.setName("selectSpecSteps");
            selectspecStep.addFocusListener(this);
            selectspecStep.addActionListener(this);
            setLayouts(selectspecStep,linePanel,2,8,1,cons,gridB);
        }else{
           setLayouts(specStep,linePanel,0,8,3,cons,gridB); 
        }
        acquisitions = new JTextField("0");
        acquisitions.setName("acquisitions");
        acquisitions.addFocusListener(this);
        acquisitions.addActionListener(this);
        acquisitions.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        //acquisitions.addPropertyChangeListener(this);
        setLayouts(acquisitions,linePanel,0,9,3,cons,gridB);
        snr = new JTextField("0");
        snr.setName("snr");
        snr.addFocusListener(this);
        snr.addActionListener(this);
        snr.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        setLayouts(snr,linePanel,0,10,3,cons,gridB);
        imScanPos = new JTextField("0");
        imScanPos.setName("imScanPos");
        imScanPos.addFocusListener(this);
        imScanPos.addActionListener(this);
        imScanPos.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        setLayouts(imScanPos,linePanel,0,11,3,cons,gridB);
        iterations = new JTextField("0");
        iterations.setName("iterations");
        iterations.addFocusListener(this);
        iterations.addActionListener(this);
        iterations.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });
        //iterations.addPropertyChangeListener(this);
        setLayouts(iterations,linePanel,0,12,3,cons,gridB);
        lineTime = new JTextField("0");
        lineTime.setName("lineTime");
        lineTime.addFocusListener(this);
        //lineTime.addPropertyChangeListener(this);
        setLayouts(lineTime,linePanel,0,13,3,cons,gridB);
        if(tabNr == 2){
        userdefSteps = new JTextField("no user steps");
        userdefSteps.setName("userdefSteps");
        userdefSteps.addFocusListener(this);
        userdefSteps.getDocument().addDocumentListener(myDocumentListener);
        userdefSteps.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                ue = new UserEditor();
                double[] input;
                input=getLineParams();
                VTF_IPC_GUI.udscansteps[lineidx]=1;
                ue.UserEditor(lineidx, tabidx, userdefSteps.getText(), input, userdefSteps,lineTime);
                
            }
        }
        );
        //if(tabNr == 2){
            setLayouts(userdefSteps,linePanel,0,14,3,cons,gridB);
        }
        resetB = new JButton(" RESET ");
        resetB.setName("resetB");
        resetB.setBackground(VTF_IPC_GUI.myorange);
        resetB.addFocusListener(this);
        resetB.addActionListener(this);
        setLayouts(resetB,linePanel,0,15,3,cons,gridB);
        
        // fill line combobox with wavenames
        int diml_input=VTF_IPC_GUI.list0.size();
        String[] wavetable=new String[diml_input+2];
        wavetable[diml_input+1]="Custom";
                        
        for(int i=0;i<(diml_input+1);i++){
          if (i==0){
            selectLine.addItem("0");
              wavetable[0]="0";
          }  else {        
            selectLine.addItem((VTF_IPC_GUI.list0.get(i-1).get(6)));
            wavetable[i]=(VTF_IPC_GUI.list0.get(i-1).get(0));
          }
        }
        selectLine.addItem(wavetable[diml_input+1]);
        //System.out.println("debug --- init_line: set line inactive for Tab "+tabNr+" -> VTF Tab = "+VTF_IPC_GUI.whichTab);
        setLineEditable(tabNr);
        setLineInActive(tabNr);
        
        selectBinning.addItem("1x1");
        selectBinning.addItem("2x2");
        selectBinning.addItem("4x4");
        
        selectROI.addItem("4128x4104");
        selectROI.addItem("2048x2048");
        selectROI.addItem("1024x1024");
        
        selectScanMode.addItem("blue-red");
        selectScanMode.addItem("nested");
        
        if(tabNr>0){
            selectspecStep.addItem("x1");
            selectspecStep.addItem("x2");
            if (tabNr==2)
                selectspecStep.addItem("x3");
        }
        //selectLine.setSelectedItem("FeI_630.25");
        
    }
    
    private void setLayouts (Component input, JPanel panel, int xpos, int ypos, int width, GridBagConstraints inc, GridBagLayout griddy){
        inc.gridx=xpos;
        inc.gridy=ypos;
        inc.gridwidth=width;
        inc.weightx=0.0;
        inc.weighty=0.0;
        griddy.setConstraints(input, inc);
        panel.add(input,inc);
    }
    
    public JPanel getLine(){
        return this.linePanel;
    }
    
    public JTextField accessUserDef(){
        return this.userdefSteps;
    }
    
    public void resetLine(){
        VTF_IPC_GUI.line_active[lineidx]=false;
        VTF_IPC_GUI.udscansteps[lineidx]=0;
        expTime.setText("25.0");
        selectLine.setSelectedIndex(0);
        selectScanMode.setSelectedIndex(0);
        selectBinning.setSelectedIndex(0);
        selectROI.setSelectedIndex(0);
        snr.setText("");            
        if(tabidx>0)
            selectspecStep.setSelectedIndex(0);
    }
    
    public void setLineEditable(int tabindex){
        boolean[] bb= new boolean[18];
        boolean cust=false;
        boolean intens=false;
        boolean intens_scan=true;
        
        if(((String)selectLine.getSelectedItem()).equals("Custom"))
            cust=true;
        if(VTF_IPC_GUI.Intensity.isSelected()){
            intens=true;
            intens_scan=false;
        }
        if(VTF_IPC_GUI.udscansteps[lineidx]==1 && tabidx==2){
            intens_scan=false;
           //System.out.println("msdebug (LINEBODY) ------ setLineEditable: user defined scan steps");
        }
        
        switch(tabindex){
            case 0:
                bb[0]=false;   //selectLine
                bb[1]=false;   //selectScanMode
                bb[2]=false;   //selectBinning
                bb[3]=false;  //selectROI
                bb[4]=cust;  //central wave
                bb[5]=false;  //expTime
                bb[6]=cust;  //scanL
                bb[7]=cust;  //scanR
                bb[8]=false;  //scanSum
                bb[9]=cust; //specstep
                bb[10]=false; //selectspecStep
                bb[11]=false; //asccum
                bb[12]=cust; //snr
                bb[13]=intens; //imScanPos
                bb[14]=false; //iterations
                bb[15]=false; //lineTime
                bb[16]=false; //userdef
                bb[17]=false; //reset
                break;
            case 1:
                bb[0]=false;   //selectLine
                bb[1]=false;   //selectScanMode
                bb[2]=false;   //selectBinning
                bb[3]=false;  //selectROI
                bb[4]=cust;  //central wave
                bb[5]=true;  //expTime
                bb[6]=intens_scan;  //scanL
                bb[7]=intens_scan;  //scanR
                bb[8]=false;  //scanSum
                bb[9]=cust; //specstep
                bb[10]=false; //selectspecStep
                bb[11]=true; //asccum
                bb[12]=true; //snr
                bb[13]=intens; //imScanPos
                bb[14]=true; //iterations
                bb[15]=false; //lineTime
                bb[16]=false; //userdef
                bb[17]=false; //reset
                break;
            case 2:
                bb[0]=false;   //selectLine
                bb[1]=false;   //selectScanMode
                bb[2]=false;   //selectBinning
                bb[3]=false;  //selectROI
                bb[4]=cust;  //central wave
                bb[5]=true;  //expTime
                bb[6]=intens_scan;  //scanL
                bb[7]=intens_scan;  //scanR
                bb[8]=false;  //scanSum
                bb[9]=cust; //specstep
                bb[10]=false; //selectspecStep
                bb[11]=true; //asccum
                bb[12]=true; //snr
                bb[13]=intens; //imScanPos
                bb[14]=true; //iterations
                bb[15]=false; //lineTime
                bb[16]=false; //userdef
                bb[17]=false; //reset
                break;
            default:
                break;                
        }
        
        selectLine.setEditable(bb[0]);
        selectScanMode.setEditable(bb[1]);
        selectBinning.setEditable(bb[2]);
        selectROI.setEditable(bb[3]);
        if (tabindex==0)
            selectROI.setEnabled(false);
        else
            selectROI.setEnabled(true);
        
        waveName.setEditable(bb[4]);
        expTime.setEditable(bb[5]);
        scanLeft.setEditable(bb[6]);
        scanRight.setEditable(bb[7]);
        scanSum.setEditable(bb[8]);
        specStep.setEditable(bb[9]);
        if(tabindex>0)
            selectspecStep.setEditable(bb[10]);
        acquisitions.setEditable(bb[11]);
        snr.setEditable(bb[12]);
        imScanPos.setEditable(bb[13]);
        iterations.setEditable(bb[14]);
        lineTime.setEditable(bb[15]);
        if(tabindex==2)
            userdefSteps.setEditable(bb[16]);
        

    }
    
    public double[] getLineParams () {
        double [] lineParams;
        lineParams= new double[VTF_IPC_GUI.input_nr_max];
        //System.out.println("size of lineParams = "+lineParams.length);
        int wl_index = 0;
        
        if (waveName.getText().equals("Custom") && waveName.getText().isEmpty()){
            out2.append("+++ set proper wavelength (in nm)!\n");
            out2.append(System.getProperty("line.separator"));
            JOptionPane.showMessageDialog(null, "set proper wavelength for active Line!", "Error",JOptionPane.WARNING_MESSAGE);
            waveName.setText("0.0");
        } else {
            if (waveName.getText().isEmpty()) {} else {
                lineParams[0]=Double.parseDouble(waveName.getText());
                //System.out.println("waveName = "+lineParams[0]);
                wl_index=VTF_IPC_GUI.get_wavelength_nr(String.format("%.3f", lineParams[0]));
            }
            if (expTime.getText().isEmpty()) {} else 
                lineParams[1]=Double.parseDouble(expTime.getText());
            if (scanLeft.getText().isEmpty()) {} else 
                lineParams[2]=Double.parseDouble(scanLeft.getText());
            if (specStep.getText().isEmpty()) {} else 
                lineParams[3]=Double.parseDouble(specStep.getText());
            if (acquisitions.getText().isEmpty()) {} else 
                lineParams[4]=Double.parseDouble(acquisitions.getText());
            if (snr.getText().isEmpty()) {} else 
                lineParams[5]=Double.parseDouble(snr.getText());
            if ((VTF_IPC_GUI.list0.get(wl_index).get(3)).isEmpty()) {} else 
                lineParams[6]=Double.parseDouble((VTF_IPC_GUI.list0.get(wl_index).get(3)));
            if ((VTF_IPC_GUI.list0.get(wl_index).get(5)).isEmpty()) {} else 
                lineParams[7]=Double.parseDouble((VTF_IPC_GUI.list0.get(wl_index).get(5)));
                    
            int binindex=selectBinning.getSelectedIndex();
            lineParams[8]=binindex;                   
            switch(binindex){
                case 0:
                    lineParams[9]=1;
                    break;
                case 1:
                    lineParams[9]=4;
                    break;
                case 2:
                    lineParams[9]=16;
                    break;
                case 3:
                    lineParams[9]=1; //if roi is selected the camera will be read out as usual
                    break;
                default:
                    lineParams[9]=1;
                    break;
            }
            if (iterations.getText().isEmpty()){
                lineParams[10]=0;
            } else{
                lineParams[10]=Double.parseDouble(iterations.getText()); //nr of iterations
            }
            if (lineTime.getText().isEmpty()){} else 
                lineParams[11]=Double.parseDouble(lineTime.getText());
        }
        if (tabidx>0)
            lineParams[12]=selectROI.getSelectedIndex();
                    
        if (imScanPos.getText().isEmpty()){
        }else {
            lineParams[13]=Double.parseDouble(imScanPos.getText());
        }
        lineParams[14]=selectScanMode.getSelectedIndex();
        lineParams[15]=selectLine.getSelectedIndex();
        if(tabidx>0)
            lineParams[16]=selectspecStep.getSelectedIndex();
        
        if (!scanLeft.getText().isEmpty())
            lineParams[2]=Double.parseDouble(scanLeft.getText());
        if (!scanRight.getText().isEmpty())
            lineParams[17]=Double.parseDouble(scanRight.getText());
        if(!scanSum.getText().isEmpty())
            lineParams[18]=Double.parseDouble(scanSum.getText());
        if(tabidx==2){
            if(userdefSteps.getText().contains("no user steps"))
                lineParams[19]=0;
            else
                lineParams[19]=1;
        }
        
        return lineParams;
    }
    
    public void setLineParams(double[] input){
        if (input[15]>0){
            waveName.setText(Double.toString(input[0]));
            expTime.setText(Double.toString(input[1]));
            scanLeft.setText(Integer.toString((int)(input[2])));
            scanRight.setText(Integer.toString((int)(input[17])));
            scanSum.setText(Integer.toString((int)(input[18])));
            specStep.setText(Double.toString(input[3]));
            acquisitions.setText(Integer.toString((int)(input[4])));
            snr.setText(Integer.toString((int)(input[5])));
            iterations.setText(Integer.toString((int)(input[10])));
            lineTime.setText(Double.toString(input[11]));
            imScanPos.setText(Double.toString(input[13]));
        }
    }
    
    public void setSelections(double[] input){
        //attention with action behaviour
       //System.out.println("read in input params:\n"+Arrays.toString(input)+"\n");
        selectROI.setSelectedIndex((int)(input[12]));
        selectScanMode.setSelectedIndex((int)(input[14]));
        //attention with action behaviour
        if(tabidx>0)
            selectspecStep.setSelectedIndex((int)(input[16]));
        //attention with action behaviour
        ////System.out.println("binning index = "+(int)(input[8]));
        selectBinning.setSelectedIndex((int)input[8]);
        //attention with action behaviour
       //System.out.println("debug LineBody.setSelectedIndex() input: \n"+Arrays.toString(input));
       //System.out.println("debug LineBody.setSelectedIndex(): "+input[15]);
        selectLine.setSelectedIndex((int) input[15]);
       //System.out.println("debug LineBody.setSelectedIndex(): DONE");
       
    }
    
    public double[] getCalculationsLine(){
        return results_calc;
    }
    
    /*public void setLineNr(int numb){
        lineidx=numb;
    }*/

    public JTextField accessCycTime(){
        return this.lineTime;
    }
    public Object getBinning(){
        return this.selectBinning.getSelectedItem();
    }
    
    public void setBinning(Object selection){
        selectBinning.setSelectedItem(selection);
        methodActionLost_selectBinning();
    }
    
    public Object getROI(){
        return selectROI.getSelectedItem();
    }
    
    public void setROI(Object roi){
        selectROI.setSelectedItem(roi);
        double[] input=getLineParams();
        methodItemChange_selectROI(input);
    }
    
    public String getExpTime(){
        return expTime.getText();
    }
    
    public void setExpTime(String expt){
        expTime.setText(expt);
        double[] input=getLineParams();
       //System.out.println("ms-debug (LineBody) -------- setExpTime (line/tab = "+lineidx+"/"+tabidx+") - input[1]="+input[1]);
        methodActionLost_ExpAccumRepeats(input,true);
    }
    
    public void setLineActive(int whichTab ) {
               
        ArrayList<Color> cc =new ArrayList<>();
        //System.out.println("debug --- set line active for Tab "+whichTab+" -> VTF Tab = "+VTF_IPC_GUI.whichTab);
        
        switch(whichTab){
            case 0:
                cc.add(mydarkgreen); //waveName
                cc.add(mydarkgreen); //expTime
                cc.add(mydarkgreen); //scanLeft
                cc.add(mydarkgreen); //scanRight
                cc.add(mydarkgreen); //scanSum
                cc.add(mydarkgreen); //specStep
                cc.add(mydarkgreen); //acquisitions
                cc.add(mydarkgreen); //snr
                cc.add(mydarkgreen); //imScanPos
                cc.add(mydarkgreen); //iterations
                cc.add(myblue);      //lineTime
                
                break;
            case 1:
                cc.add(mydarkgreen); //waveName
                cc.add(myyellow); //expTime
                if(VTF_IPC_GUI.Intensity.isSelected()){
                    cc.add(mygray); //scanLeft
                    cc.add(mygray); //scanRight
                    cc.add(mygray); //scanSum
                }else{
                    cc.add(myyellow); //scanLeft
                    cc.add(myyellow); //scanRight
                    cc.add(mydarkgreen); //scanSum
                }
                cc.add(mydarkgreen); //specStep
                cc.add(myyellow); //acquisitions
                cc.add(myyellow); //snr
                if(VTF_IPC_GUI.Intensity.isSelected()){
                    cc.add(myyellow);
                }else{
                    cc.add(mygray); //imScanPos
                }
                cc.add(myyellow); //iterations
                cc.add(myblue);      //lineTime
                
                break;
            case 2:
                cc.add(mydarkgreen); //waveName
                cc.add(myyellow); //expTime
                if(VTF_IPC_GUI.Intensity.isSelected()){
                    cc.add(mygray); //scanLeft
                    cc.add(mygray); //scanRight
                    cc.add(mygray); //scanSum
                }else{
                    cc.add(myyellow); //scanLeft
                    cc.add(myyellow); //scanRight
                    cc.add(mydarkgreen); //scanSum
                }
                cc.add(mydarkgreen); //specStep
                cc.add(myyellow); //acquisitions
                cc.add(myyellow); //snr
                if(VTF_IPC_GUI.Intensity.isSelected()){
                    cc.add(myyellow);
                }else{
                    cc.add(mygray); //imScanPos
                }
                cc.add(myyellow); //iterations
                cc.add(myblue);      //lineTime
                cc.add(Color.WHITE); //userdef
                break;
            default:
                break;
        }      
              
        waveName.setBackground(cc.get(0));
        expTime.setBackground(cc.get(1));
        scanLeft.setBackground(cc.get(2));
        scanRight.setBackground(cc.get(3));
        scanSum.setBackground(cc.get(4));
        specStep.setBackground(cc.get(5));
        acquisitions.setBackground(cc.get(6));
        snr.setBackground(cc.get(7));
        
        imScanPos.setBackground(cc.get(8));
        
        iterations.setBackground(cc.get(9));
        
        lineTime.setBackground(cc.get(10));
        if(whichTab==2)
            userdefSteps.setBackground(cc.get(11));
        
        if ("Custom".equals((String)selectLine.getSelectedItem())) {
            scanLeft.setBackground(myyellow);
            scanRight.setBackground(myyellow);
            scanSum.setBackground(mydarkgreen);
            specStep.setBackground(myyellow);
            snr.setBackground(myyellow);
            waveName.setBackground(myyellow);
        }
    }

    public void setLineInActive(int whichTab){
        ArrayList<Color> cc =new ArrayList<>();
        //System.out.println("debug --- set line inactive for Tab "+whichTab+" -> VTF Tab = "+VTF_IPC_GUI.whichTab);
        switch(whichTab){
            case 0:
                cc.add(mygray); //waveName
                cc.add(mygray); //expTime
                
                cc.add(mygray); //scanLeft
                cc.add(mygray); //scanRight
                cc.add(mygray); //scanSum
                
                cc.add(mygray); //specStep
                cc.add(mygray); //acquisitions
                cc.add(mygray); //snr
                cc.add(mygray); //imScanPos
                cc.add(mygray); //iterations
                cc.add(mygray);      //lineTime
                
                break;
            case 1:
                cc.add(mygray); //waveName
                cc.add(Color.WHITE); //expTime
                
                cc.add(Color.WHITE); //scanLeft
                cc.add(Color.WHITE); //scanRight
                cc.add(mygray); //scanSum
                
                cc.add(mygray); //specStep
                cc.add(Color.WHITE); //acquisitions
                cc.add(Color.WHITE); //snr
                cc.add(mygray); //imScanPos
                cc.add(Color.WHITE); //iterations
                cc.add(mygray);      //lineTime
                
                break;
            case 2:
                cc.add(mygray); //waveName
                cc.add(Color.WHITE); //expTime
                
                cc.add(Color.WHITE); //scanLeft
                cc.add(Color.WHITE); //scanRight
                cc.add(Color.WHITE); //scanSum
                
                cc.add(mygray); //specStep
                cc.add(Color.WHITE); //acquisitions
                cc.add(Color.WHITE); //snr
                cc.add(mygray); //imScanPos
                cc.add(Color.WHITE); //iterations
                cc.add(mygray);      //lineTime
                cc.add(Color.WHITE); //userdef
                break;
            default:
                break;
        }      
              
        waveName.setBackground(cc.get(0));
        expTime.setBackground(cc.get(1));
        scanLeft.setBackground(cc.get(2));
        scanRight.setBackground(cc.get(3));
        scanSum.setBackground(cc.get(4));
        specStep.setBackground(cc.get(5));
        acquisitions.setBackground(cc.get(6));
        snr.setBackground(cc.get(7));
        
        imScanPos.setBackground(cc.get(8));
        
        iterations.setBackground(cc.get(9));
        
        lineTime.setBackground(cc.get(10));
        if(whichTab==2)
            userdefSteps.setBackground(cc.get(11));
        /*if ("Custom".equals((String)selectLine.getSelectedItem())) {
            scanLeft.setBackground(myyellow);
            specStep.setBackground(myyellow);
            snr.setBackground(myyellow);
            waveName.setBackground(myyellow);
        }*/        
    }
    
    public final boolean Custom(){
        return isCustom;
    }
    
    public final boolean Active(){
        return isActive;
    }
    
    public void setUserDefFiles(String filename){
        userdefSteps.setText(filename);
    }

    public void setSNR_Line(int sig){
        snr.setText(Integer.toString(sig));
    }
    
    public void addFocusListener_Line(FocusListener lis, String name){
        switch(name){
            case "selectLine":
                selectLine.addFocusListener(lis);
                break;
            case "selectScanMode":
                selectScanMode.addFocusListener(lis);
                break;
            case "selectBinning":
                selectBinning.addFocusListener(lis);
                break;
            case "selectROI":
                selectROI.addFocusListener(lis);
                break;
            case "waveName":
                waveName.addFocusListener(lis);
                break;
            case "expTime":
                expTime.addFocusListener(lis);
                break;
            case "scanLeft":
                scanLeft.addFocusListener(lis);
                break;
            case "scanRight":
                scanRight.addFocusListener(lis);
                break;
            case "scanSum":
                scanSum.addFocusListener(lis);
                break;
            case "specStep":
                specStep.addFocusListener(lis);
                break;
            case "selectspecStep":
                selectspecStep.addFocusListener(lis);
                break;
            case "acquisitions":
                acquisitions.addFocusListener(lis);
                break;
            case "snr":
                snr.addFocusListener(lis);
                break;
            case "imScanPos":
                imScanPos.addFocusListener(lis);
                break;
            case "iterations":
                iterations.addFocusListener(lis);
                break;
            case "lineTime":
                lineTime.addFocusListener(lis);
                break;
            case "userdefSteps":
                userdefSteps.addFocusListener(lis);
                break;
            default:
                //System.out.println("debug --- nothing selected");
        }
    }

    public void removeActionListenerBinning(ActionListener al){
        selectBinning.removeActionListener(al);
    }
    
    public void addActionListenerBinning(ActionListener al,String ac){
        selectBinning.setActionCommand(ac);
        selectBinning.addActionListener(al);
    }
    
    public void addActionListenerROI(ActionListener al, String ac){
        selectROI.setActionCommand(ac);
        selectROI.addActionListener(al);
    }
    
    public void removeActionListenerROI(ActionListener al){
        selectROI.removeActionListener(al);
    }
    
    public void addActionFocusListenerExpTime(ActionListener al, String ac){
        expTime.setActionCommand(ac);
        expTime.addActionListener(al);
    }
    
    public void removeActionFocusListenerExpTime(ActionListener al){
        expTime.removeActionListener(al);
    }
    
    public void addActionListener_Line(ActionListener lis, String name){
        switch(name){
            case "selectLine":
                selectLine.addActionListener(lis);
                break;
            case "selectScanMode":
                selectScanMode.addActionListener(lis);
                break;
            case "selectBinning":
                selectBinning.addActionListener(lis);
                break;
            case "selectROI":
                selectROI.addActionListener(lis);
                break;
            case "waveName":
                waveName.addActionListener(lis);
                break;
            case "expTime":
                expTime.addActionListener(lis);
                break;
            case "scanLeft":
                scanLeft.addActionListener(lis);
                break;
            case "scanRight":
                scanRight.addActionListener(lis);
                break;
            case "scanSum":
                scanSum.addActionListener(lis);
                break;
            case "specStep":
                specStep.addActionListener(lis);
                break;
            case "selectspecStep":
                selectspecStep.addActionListener(lis);
                break;
            case "acquisitions":
                acquisitions.addActionListener(lis);
                break;
            case "snr":
                snr.addActionListener(lis);
                break;
            case "imScanPos":
                imScanPos.addActionListener(lis);
                break;
            case "iterations":
                iterations.addActionListener(lis);
                break;
            case "lineTime":
                lineTime.addActionListener(lis);
                break;
            case "userdefSteps":
                userdefSteps.addActionListener(lis);
                break;
            default:
                //System.out.println("debug --- nothing selected");
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        /*
        if (e.getSource()instanceof JTextField)
          //System.out.println("ms debug --- LineBody actionPerformed!!! for line/tab = "+lineidx+" / "+tabidx+"\n      source text "+((JTextField)e.getSource()).getName()+"="+((JTextField)e.getSource()).getText());
        if(e.getSource()instanceof JComboBox)
           System.out.println("ms debug --- LineBody actionPerformed!!! for line/tab = "+lineidx+" / "+tabidx+"\n      source comboBox "+((JComboBox)e.getSource()).getName()+"="+((JComboBox)e.getSource()).getSelectedItem());
        */
        double[] input= new double[VTF_IPC_GUI.input_nr_max];
        //input[15]=selectLine.getSelectedIndex();
        //System.out.println("msdebug (LINEBODY) ---- actionPerformed (line/tab: "+lineidx+"/"+tabidx+")- input 15 = "+input[15]+" / check:"+selectLine.getSelectedIndex());
        if(e.getSource()==selectLine){
            input=getLineParams();
           //System.out.println("msdebug (LINEBODY) ---- actionPerformed (line/tab: "+lineidx+"/"+tabidx+")- input 15 = "+input[15]+" / check:"+selectLine.getSelectedIndex());
            methodActionLost_selectLine(input);
        }
        
        if(e.getSource()==expTime || e.getSource()==acquisitions || e.getSource()==iterations){
            input=getLineParams();
            boolean expAccum=false;
            if (e.getSource()==expTime || e.getSource()==acquisitions)
                expAccum=true;
            methodActionLost_ExpAccumRepeats(input,expAccum);
        }
                
        if (e.getSource()==resetB){
            resetLine();
        }

        if(e.getSource()==scanLeft || e.getSource()==scanRight){
            methodActionLost_scanSteps();
        }

        if(e.getSource()==selectspecStep){
            methodActionLost_selectspecStep(input);
        }
        
        if(e.getSource()==snr){
            input = getLineParams();
            input=calcET.CalculateExpTime_Line(lineidx,tabidx,input,isCustom);
            setLineParams(input);
        }
        if (e.getSource()==imScanPos && VTF_IPC_GUI.Intensity.isSelected()){
            input=getLineParams();
            boolean isAdvanced=false;
            if(tabidx==2)
                isAdvanced=true;
            double plotrange=checkPlotRange(input, isAdvanced, 0);
            plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,isCustom);
        }
        
    }
    
    @Override
    public void focusGained (FocusEvent e) {
       //System.out.println("ms debug --- LineBody focusGained!!! source="+e.getSource());
        setLineActive(tabidx);
        double[] input=getLineParams();
        double urange_plot=0; // plot rnage in nm
        if(input[0]>0.1 && !isCustom && selectLine.getSelectedIndex()!=0){
            //System.out.println("debug --- focusGained: plot graph");
                    boolean isAdvanced=false;
                    if(tabidx==2)
                        isAdvanced=true;
                    if(VTF_IPC_GUI.udscansteps[lineidx]==1){
                        double[] minmax_sg=new double[2];
                        double[] xvalues=new double[VTF_IPC_GUI.udss_output.length];
                        int count=0;
                        for(String name:VTF_IPC_GUI.udss_output){
                            xvalues[count]=Double.parseDouble(name);
                            count++;
                        }
                        minmax_sg=VTF_IPC_GUI.minmax(xvalues);
                        double maxplot;
                        if(Math.abs(minmax_sg[0])>Math.abs(minmax_sg[1]))
                            maxplot=Math.abs(minmax_sg[0]);
                        else
                            maxplot=Math.abs(minmax_sg[1]);
        
                        if(0.002*maxplot>input[6])
                            urange_plot=0.002*maxplot;
                    }
                    double plotrange=checkPlotRange(input, isAdvanced, urange_plot);
                    
                    plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,isCustom);
                    
        }    
        isActive=true;
        VTF_IPC_GUI.whichLine=lineidx;
    }

    @Override
    public void focusLost(FocusEvent e) {
        //setLineInActive(tabidx);
        isActive=false;
        double[] input=new double[VTF_IPC_GUI.input_nr_max];
        /*
        if (e.getSource()instanceof JTextField)
           System.out.println("ms debug --- LineBody focusLost!!! source "+((JTextField)e.getSource()).getName()+"="+((JTextField)e.getSource()).getText());
        else if (e.getSource() instanceof JComboBox)
            System.out.println("ms debug --- LineBody focusLost!!! source="+((JComboBox)e.getSource()).getName()+"="+((JComboBox)e.getSource()).getSelectedItem());
        */
        if(e.getSource()==expTime || e.getSource()==acquisitions || e.getSource()==iterations){
        //if(e.getSource()==acquisitions || e.getSource()==iterations){
            input=getLineParams();
            boolean expAccum=false;
            if (e.getSource()==expTime || e.getSource()==acquisitions)
                expAccum=true;
            methodActionLost_ExpAccumRepeats(input,expAccum);
        }
        
        if(e.getSource()==scanLeft || e.getSource()==scanRight){
            methodActionLost_scanSteps();
        }
        
       if(e.getSource()==snr){
           if(!isCustom){
            input = getLineParams();
            input=calcET.CalculateExpTime_Line(lineidx,tabidx,input,isCustom);
            setLineParams(input);
           }
        }
    }


    private void methodActionLost_selectLine(double[] input){
            /*for (int i=0;i<VTF_IPC_GUI.maxLines;i++){
                VTF_IPC_GUI.line_active[i] = false;
            }*/
            VTF_IPC_GUI.line_active[lineidx] = true;
            VTF_IPC_GUI.whichLine=lineidx;
            isActive=true;
            String wselect = selectLine.getSelectedItem().toString();
            input[15] = selectLine.getSelectedIndex();
            
            int wl_index;
            for (int k=0;k<input.length;k++){
                input[k]=0;
            }
            if ("".equals(expTime.getText())) {
                expTime.setText("25.0");
            } else {        
                input[1] = Double.parseDouble(expTime.getText());  //exposure time
            }
            if (acquisitions.getText().equals("0")){
                if (VTF_IPC_GUI.Polar.isSelected()){
                    input[4]=8;
                }else {
                    input[4]=1;
                }
            }else{
                input[4]=Double.parseDouble(acquisitions.getText());
            }
            //input[4]=Double.parseDouble(acquisitions.getText());
            acquisitions.setText(Integer.toString((int)input[4]));
            //System.out.println("methodActionLost_selectLine: wselect="+wselect);
            if(null != wselect )switch (wselect) {
                case "Custom":
                    waveName.setEditable(true);
                    waveName.setText("0");
                    //VTF_IPC_GUI.customD[lineidx]=1;
                    isCustom=true;
                    specStep.setEditable(true);
                    specStep.setText("0");
                    snr.setEditable(true);
                    snr.setText("0");
                    scanLeft.setEditable(true);
                    scanLeft.setText("0");
                    scanRight.setEditable(true);
                    scanRight.setText("0");
                    scanLeft.setBackground(VTF_IPC_GUI.myyellow);
                    scanRight.setBackground(VTF_IPC_GUI.myyellow);
                    specStep.setBackground(VTF_IPC_GUI.myyellow);
                    snr.setBackground(VTF_IPC_GUI.myyellow);
                    waveName.setBackground(VTF_IPC_GUI.myyellow);
                    break;
                case "0":
                    VTF_IPC_GUI.line_active[0] = false;
                    isActive=false;
                    isCustom=false;
                    waveName.setText("0");
                    scanLeft.setText("0");
                    scanRight.setText("0");
                    scanSum.setText("0");
                    specStep.setText("0");
                    snr.setText("0");
                    imScanPos.setText("0.0");
                    iterations.setText("0");
                    if(tabidx==2)
                        userdefSteps.setText("no user steps");
                    break;
                default:
                    //VTF_IPC_GUI.customD[lineidx]=0;
                    isCustom=false;
                    setLineEditable(tabidx);
                    //wavelength=Double.parseDouble(wselect);
                    wl_index=VTF_IPC_GUI.get_wavelength_nr(wselect);
                    System.out.println("methodActionLost_selectLine: wl_index="+wl_index);
                    input[0]=Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(0)); //wavelength
                    if(VTF_IPC_GUI.Intensity.isSelected()){
                        input[13]=input[0];
                        imScanPos.setText(Double.toString(input[13]));
                    }
                    waveName.setText(VTF_IPC_GUI.list0.get(wl_index).get(0));
                    if (VTF_IPC_GUI.Polar.isSelected() || VTF_IPC_GUI.Doppler.isSelected()){
                        input[18]=Integer.parseInt(VTF_IPC_GUI.list0.get(wl_index).get(4)); //scanSum
                        String stephalf=Integer.toString((int)input[18]/2);
                        scanLeft.setText("-"+stephalf);
                        
                        input[2]=(-1)*(int)input[18]/2;
                        scanRight.setText(stephalf);
                        
                        input[17]=(int)input[18]/2;
                        scanSum.setText(Integer.toString((int)input[18]));
                    }else{
                        input[2]=0;
                        scanLeft.setText("0");
                        
                        input[17]=0;
                        scanRight.setText("0");
                        
                        input[18]=1; //nr scan steps
                        scanSum.setText("1");
                        input[13]=input[0];
                    }
                    input[3]=(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(1)))/2d; // spectral bandwidth - fwhm/2
                    specStep.setText(Double.toString(input[3]));
                    input[6]=Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(3));//PF width
                    input[7]=Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(5));// PF transmission
                    input[8]=selectBinning.getSelectedIndex();//binning index
                    ////System.out.println("selected binning index = "+(int)input[8]);
                    switch ((int)input[8]){
                        case 0:
                            input[9]=1;
                            break;
                        case 1:
                            input[9]=4;
                            break;
                        case 2:
                            input[9]=16;
                            break;
                        default:
                            break;
                    }
                    ////System.out.println("selected binning factor = "+(int)input[9]);
                    if(tabidx!=0)
                        input[16]=selectspecStep.getSelectedIndex();
                    
                    if(input[16]>0){
                        //System.out.println("debug --- specSteps > 0 :"+input[16]);
                        input[3]=(input[16]+1)*input[3];
                        specStep.setText(Double.toString(input[3]));
                    }
                    
                    if (("").equals(iterations.getText())) {
                        input[10] = 0;
                        iterations.setText("0");
                    } else {
                        input[10] = Double.parseDouble(iterations.getText());
                    }
                    input[12] = selectROI.getSelectedIndex();
                    input[14] = selectScanMode.getSelectedIndex();
                    input[15] = selectLine.getSelectedIndex();
                    
                    input[5]=calcSNR.CalculateSNR_Line(isCustom,wl_index,input,Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText()));
                    System.out.println("msdebug (LINEBODY)  ---- calculated SNR (wavelength:"+input[0]+")="+Double.toString(input[5])+"\n      params: "+Arrays.toString(input));
                    snr.setText(Integer.toString((int)input[5]));
                    
                    break;
            }
        
            //setLineParams(input);
            results_calc = calcTimeL.CalculateTiming_Line(input,lineidx);
            input[11]=results_calc[0];
            VTF_IPC_GUI.time[lineidx]=results_calc[0];
            VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
            lineTime.setText(Double.toString(results_calc[0]));
            if (results_calc[0]==0.0) {
               //System.out.println("debug LineBody.methodActionLost_selectLine ----  no line selected -> no timing to calculate");
            } else {
               //System.out.println("debug --- ActPerformed Tabidx="+tabidx+" for Line "+lineidx+": time="+VTF_IPC_GUI.time[lineidx]+"  dspace="+VTF_IPC_GUI.dspace[lineidx]+" RN: "+VTF_IPC_GUI.repeatsNumb_text.getText()+"  delay:"+VTF_IPC_GUI.delayObs_text.getText());
                resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
               //System.out.println("debug -- LineBody.methodActionLost_selectLine resultTab:"+Arrays.toString(resultTab));
                int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                String roundRes=Double.toString(((double)roundval)/100.d);
                double nachkomma=(resultTab[0]*100.d)%100;
                VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
            }   
            //System.out.println("debug --- lineindex / tabindex : "+lineidx+" / "+tabidx);
            if(input[0]>0.1 && !isCustom && selectLine.getSelectedIndex()!=0){
                //System.out.println("debug --- actionPerformed selectLine: plot graph");
                double urange_plot=0;
                    boolean isAdvanced=false;
                    if(tabidx==2)
                        isAdvanced=true;
                    if(VTF_IPC_GUI.udscansteps[lineidx]==1){
                        double[] minmax_sg=new double[2];
                        double[] xvalues=new double[VTF_IPC_GUI.udss_output.length];
                        int count=0;
                        for(String name:VTF_IPC_GUI.udss_output){
                            xvalues[count]=Double.parseDouble(name);
                            count++;
                        }
                        minmax_sg=VTF_IPC_GUI.minmax(xvalues);
                        double maxplot;
                        if(Math.abs(minmax_sg[0])>Math.abs(minmax_sg[1]))
                            maxplot=Math.abs(minmax_sg[0]);
                        else
                            maxplot=Math.abs(minmax_sg[1]);
        
                        if(0.002*maxplot>input[6])
                            urange_plot=0.002*maxplot;
                    }
                    double plotrange=checkPlotRange(input, isAdvanced, urange_plot);
                    
                    plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,isCustom);
                    
            }
            
            waveName.requestFocusInWindow();
            //System.out.println("debug ---  ActionPerformed: lineTime was set!!!");
            //waveName.requestFocusInWindow();// for doing the plot we need to set focus outside the ComboBox !?! -- it is a hack :(
        
    }

    private void methodActionLost_selectBinning(){
        double[] inval=new double[VTF_IPC_GUI.input_nr_max];
           
        //System.out.println("ms-debug LineBody.setBinning");
        if (VTF_IPC_GUI.ObsModeGroup.getSelection().getActionCommand()=="SI"){
            switch(selectBinning.getSelectedIndex()){
                case 0:
                    inval[4]=8;
                    inval[1]=25;
                    inval[9]=1.0;
                    break;
                case 1:
                    inval[4]=2;
                    inval[1]=25;
                    inval[9]=4.0;
                    break;
                case 2:
                    inval[4]=1;
                    if (12.5<Double.parseDouble(expTime.getText()))
                            inval[1]=12.5;
                    else
                        inval[1]=Double.parseDouble(expTime.getText());
                    inval[9]=16.0;
                    break;
                default:
                    break;                
            }
        }else{
            inval[1]=Double.parseDouble(expTime.getText());
            inval[4]=1;
        }
        expTime.setText(Double.toString(inval[1]));
        acquisitions.setText(Integer.toString((int)inval[4]));
        if(selectLine.getSelectedItem().equals("0") || selectLine.getSelectedItem().equals("Custom")){
            snr.setText("0");
        }else{
           //System.out.println("ms-debug LineBody.setBinning - line/tab = "+lineidx+"/"+tabidx);
            inval = getLineParams();
            int wl_index=VTF_IPC_GUI.get_wavelength_nr(selectLine.getSelectedItem().toString());
            int sig;
            sig=calcSNR.CalculateSNR_Line(isCustom,wl_index,inval,Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText()));
            snr.setText(Integer.toString(sig));
            results_calc=calcTimeL.CalculateTiming_Line(inval,lineidx);
            inval[11]=results_calc[0];
            VTF_IPC_GUI.time[lineidx]=results_calc[0];
            VTF_IPC_GUI.dspace[lineidx]=results_calc[1];

            lineTime.setText(Double.toString(results_calc[0]));
            resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
            int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
            String roundRes=Double.toString(((double)roundval)/100.d);
            double nachkomma=(resultTab[0]*100.d)%100;
            VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
            VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
        }
        //if (VTF_IPC_GUI.whichLine==lineidx)
        //        waveName.requestFocus();
    }
    
    private void methodItemChange_selectROI(double[] input){
        input[12]=selectROI.getSelectedIndex();
        results_calc=calcTimeL.CalculateTiming_Line(input,lineidx);
        VTF_IPC_GUI.time[lineidx]=results_calc[0];
        VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
        
        input[11]=results_calc[0];
        
        lineTime.setText(Double.toString(input[11]));
        resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
        int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(resultTab[0]*100.d)%100;
        VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
    }
    
    private void methodActionLost_selectspecStep(double[] input){
            double specs,specs0;
            int wl_index=VTF_IPC_GUI.get_wavelength_nr(selectLine.getSelectedItem().toString());
            specs0=(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(1)))/2d;
            int choice=selectspecStep.getSelectedIndex();
            switch(choice){
                case 0:
                    specs=specs0;
                    specStep.setText(Double.toString(specs));
                    break;
                case 1:
                    specs=(Math.rint(200*specs0))/100.;
                    specStep.setText(Double.toString(specs));
                    break;
                case 2:
                    specs=(Math.rint(300*specs0))/100.;
                    specStep.setText(Double.toString(specs));
            }
            input=getLineParams();
            if(input[0]>0.1 && !isCustom && selectLine.getSelectedIndex()!=0){
                double urange_plot=0;
                //System.out.println("debug --- actionPerformed selectspecSteps: plot graph");
                    boolean isAdvanced=false;
                    if(tabidx==2)
                        isAdvanced=true;
                    if(VTF_IPC_GUI.udscansteps[lineidx]==1){
                        double[] minmax_sg=new double[2];
                        double[] xvalues=new double[VTF_IPC_GUI.udss_output.length];
                        int count=0;
                        for(String name:VTF_IPC_GUI.udss_output){
                            xvalues[count]=Double.parseDouble(name);
                            count++;
                        }
                        minmax_sg=VTF_IPC_GUI.minmax(xvalues);
                        double maxplot;
                        if(Math.abs(minmax_sg[0])>Math.abs(minmax_sg[1]))
                            maxplot=Math.abs(minmax_sg[0]);
                        else
                            maxplot=Math.abs(minmax_sg[1]);
        
                        if(0.002*maxplot>input[6])
                            urange_plot=0.002*maxplot;
                    }
                    double plotrange=checkPlotRange(input, isAdvanced, 0);
                    
                    plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,isCustom);
                    
            }

    }

    private void methodActionLost_ExpAccumRepeats(double[] input,boolean expAccum){
            //input = getLineParams();
            
            if (input[1] > 25.0){
                input[1]=25.0;
                expTime.setText(Double.toString(input[1]));
                //setLineParams(input);
                //DebugInfos.append("+++ max. exposure time is 25ms\n (due to speckle reconstructions)\n");
                //out2.println("+++ max. exposure time is 25ms\n (due to speckle reconstructions)\n");
                if (isActive)
                    JOptionPane.showMessageDialog(null,"max. exposure time is 25ms\n (due to speckle reconstructions)","warning",JOptionPane.WARNING_MESSAGE);
                out2.append("+++ max. exposure time is 25ms\n (due to speckle reconstructions)\n");
                out2.append(System.getProperty("line.separator"));
            }
           //System.out.println("ms-debug (LineBody) ------  methodActionLost_ExpAccumRepeats -> input[1]="+input[1]);
            if (input[1] < 0.01){
                input[1]=0.01;
                expTime.setText(Double.toString(input[1]));
                //setLineParams(input);
                //DebugInfos.append("+++ max. exposure time is 25ms\n (due to speckle reconstructions)\n");
                //out2.println("+++ max. exposure time is 25ms\n (due to speckle reconstructions)\n");
                JOptionPane.showMessageDialog(null,"ms-debug (LineBody) ---- methodActionLost_ExpAccumRepeats min. exposure time is 0.01ms\n (due to camera specs)","warning",JOptionPane.WARNING_MESSAGE);
                out2.append("+++ (LineBody) ---- methodActionLost_ExpAccumRepeats min. exposure time is 0.01ms\n (due to camera specs)\n");
                out2.append(System.getProperty("line.separator"));
            }
            

            if (input[15]>0 && Double.parseDouble(scanSum.getText())>0){
                results_calc=calcTimeL.CalculateTiming_Line(input,lineidx);
                VTF_IPC_GUI.time[lineidx]=results_calc[0];
                VTF_IPC_GUI.dspace[lineidx]=results_calc[1];

                input[11]=results_calc[0];

                if (expAccum){
                    if(input[0]>1 && !isCustom && input[15]>0){
                        int wl_index=VTF_IPC_GUI.get_wavelength_nr(selectLine.getSelectedItem().toString());
                        input[5]=calcSNR.CalculateSNR_Line(isCustom,wl_index,input,Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText()));
                        snr.setText(Integer.toString((int)input[5]));
                    }
                }
                lineTime.setText(Double.toString(input[11]));
                resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                String roundRes=Double.toString(((double)roundval)/100.d);
                double nachkomma=(resultTab[0]*100.d)%100;
                VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
                //lineTime.revalidate();
            }

    }

    private void methodActionLost_scanSteps(){
       //System.out.println("msdebug (LINEBODY) ------- methodActionLost_scanSteps: steps left/right selected!!! selected line index="+input[15]);
       double[] input=null;     
       if (scanLeft.getText().isEmpty() || scanRight.getText().isEmpty()){
               //System.out.println("msdebug (LINEBODY)  ------- methodActionLost_scanSTeps: scans left/right empty");
                scanSum.setText("0");
            }else if (selectLine.getSelectedIndex()>0){
               //System.out.println("msdebug (LINEBODY)  -----  methodActionLost_scanSteps:  line selected!!!");
                int rightX=Integer.parseInt(scanRight.getText());
                int leftX=Integer.parseInt(scanLeft.getText());
                int sum=1;
                if (leftX>0 && rightX>0){
                    sum=rightX-leftX+1;
                }else{
                    sum = 1+Math.abs((leftX)-(rightX));
                }
                scanSum.setText(Integer.toString(sum));
                input = getLineParams();
                results_calc=calcTimeL.CalculateTiming_Line(input,lineidx);
                VTF_IPC_GUI.time[lineidx]=results_calc[0];
                VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
                input[11]=results_calc[0];
                lineTime.setText(Double.toString(results_calc[0]));
                resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                //System.out.println("line parameter are correct to plot \n"+Arrays.toString(input));
                if(input[0]>0.1 && !isCustom && selectLine.getSelectedIndex()!=0){
                    double urange_plot=0;
                    //System.out.println("debug --- actionPerformed scanSteps: plot graph");
                    boolean isAdvanced=false;
                    if(tabidx==2)
                        isAdvanced=true;
                    if(VTF_IPC_GUI.udscansteps[lineidx]==1){
                        double[] minmax_sg=new double[2];
                        double[] xvalues=new double[VTF_IPC_GUI.udss_output.length];
                        scanSum.setText(Integer.toString(VTF_IPC_GUI.udss_output.length));
                        int count=0;
                        for(String name:VTF_IPC_GUI.udss_output){
                            xvalues[count]=Double.parseDouble(name);
                            count++;
                        }
                        minmax_sg=VTF_IPC_GUI.minmax(xvalues);
                        double maxplot;
                        if(Math.abs(minmax_sg[0])>Math.abs(minmax_sg[1]))
                            maxplot=Math.abs(minmax_sg[0]);
                        else
                            maxplot=Math.abs(minmax_sg[1]);
        
                        if(0.002*maxplot>input[6])
                            urange_plot=0.002*maxplot;
                    }
                    double plotrange=checkPlotRange(input, isAdvanced, 0);
                    
                    plot.PlotGraph_Line(lineidx,tabidx,input,0,plotrange,isCustom);
                    
                }
                int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                String roundRes=Double.toString(((double)roundval)/100.d);
                double nachkomma=(resultTab[0]*100.d)%100;
                VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
                //lineTime.revalidate();
            }
        
    }

    private double checkPlotRange(double[] input, boolean isAdvanced, double xs_plot){
        double plotrange;
        int wl_index=VTF_IPC_GUI.get_wavelength_nr(String.valueOf(input[0]));
        //maximum plot range is 2nm
        if (xs_plot!=0 && xs_plot<2.d){
            plotrange=xs_plot;
        }else{
            plotrange=input[6];
        }
        if (VTF_IPC_GUI.Intensity.isSelected() && input[0]>0.1 && !isCustom){
            if ((2*Math.abs(input[13]-input[0]))>input[6] && isAdvanced){
                plotrange=0.5;
            }
            if (input[13]==0){
                input[13]=input[0];
            }
            double limit=0.0;
            if (isAdvanced)
                limit=0.25;
            else
                limit=input[6]*0.5d;
            
            if ((input[13]<(input[0]-limit)) || (input[13]>(input[0]+limit))){
                input[18]=1;
                input[2]=0;
                input[17]=0;
                input[13]=input[0];
                setLineParams(input);
                //setParameter( input, tabidx, lineidx);
                JOptionPane.showMessageDialog(null,"IM: Plot is out of range! (Plot range is dx = "+input[0]+"+/- "+String.format("%.3f",(limit))+")","warning",JOptionPane.WARNING_MESSAGE);
                //calculate_timings(tabidx);
                
            }
        }else{
            double[] minmax_sg=new double[2];
            if(input[2]<input[17]){
                minmax_sg[0]=input[2]*input[3];
                minmax_sg[1]=input[17]*input[3];
            }else{
                minmax_sg[1]=input[2]*input[3];
                minmax_sg[0]=input[17]*input[3];                
            }
            //System.out.println("debug --- minX="+minmax_sg[0]+", maxX="+minmax_sg[1]);
            if (isAdvanced){
                if ((minmax_sg[0]<(-250.0)) || (minmax_sg[1]>(250.0))){
                    if (VTF_IPC_GUI.udscansteps[lineidx]==1){
                        //VTF_IPC_GUI.setUserSteps(lineidx,"");
                        VTF_IPC_GUI.udscansteps[lineidx]=0;
                    }
                    input[18]=Integer.parseInt(VTF_IPC_GUI.list0.get(wl_index).get(4));
                    input[3]=(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(1)))/2d;
                    input[2]=-(int)(input[18]/2);
                    input[17]=(int)(input[18]/2);
                    input[16]=0;
                    //setSelections(input);
                    setLineParams(input);
                    selectspecStep.setSelectedIndex((int)input[16]);
                    JOptionPane.showMessageDialog(null,"Polarimetric/DI for user defined scan steps: Plot is out of range!\n(Plot range is dx = "+input[0]+"+/- 0.25 nm)\nReset to default values!","warning",JOptionPane.WARNING_MESSAGE);
                }else{
                    //minmax is in pm - plotrange is in nm -> *2/1000
                    double maxabs=0;
                    if(Math.abs(minmax_sg[1])>Math.abs(minmax_sg[0]))
                        maxabs=Math.abs(minmax_sg[1]);
                    else
                        maxabs=Math.abs(minmax_sg[0]);
                    //System.out.println("debug --- plot range = "+0.0002*maxabs+"nm");
                    if (plotrange<0.002*maxabs){
                        plotrange=0.002*maxabs;
                    }else{
                        if (xs_plot!=0 && xs_plot<2.d){
                            plotrange=xs_plot;
                        }else{
                            plotrange=input[6]; //plotrange advanced is +-0.25nm
                        }
                    }
                            //pm to nm and factor 2 for symmetric plot
                            ////System.out.println("treshhold plot uds = "+ plotrange);
                }
            }else if ((minmax_sg[0]<(-input[6]*500.d)) || (minmax_sg[1]>(input[6]*500.d))){
                input[18]=Integer.parseInt(VTF_IPC_GUI.list0.get(wl_index).get(4));
                input[3]=(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_index).get(1)))/2d;
                input[2]=-(int)(input[18]/2);
                input[17]=(int)(input[18]/2);
                input[16]=0;
                //setSelections(input);
                setLineParams(input);
                selectspecStep.setSelectedIndex((int)input[16]);
                JOptionPane.showMessageDialog(null,"Polarimetric/DI: Plot is out of range! (Plot range is dx = "+input[0]+"+/- "+String.format("%.3f",(input[6]/2.d))+" nm)\n Reset to default values!","warning",JOptionPane.WARNING_MESSAGE);
            }
        }
        //System.out.println("debug --- plotrange="+plotrange);
        return plotrange;
    }

    
    @Override
    public void itemStateChanged(ItemEvent e) {
       //System.out.println("ms debug --- LineBody itemStateChanged!!! source="+e.getSource().toString());
        if (e.getSource()==selectBinning){
            double[] inval = getLineParams();
            
           //System.out.println("ms-debug item changed - selectBinning");
            if (VTF_IPC_GUI.ObsModeGroup.getSelection().getActionCommand()=="SI"){
                switch(selectBinning.getSelectedIndex()){
                    case 0:
                        inval[4]=8;
                        inval[1]=25;
                        break;
                    case 1:
                        inval[4]=2;
                        inval[1]=25;
                        break;
                    case 2:
                        inval[4]=1;
                        inval[1]=12.5;
                        break;
                    default:
                        break;                
                }
            }
            expTime.setText(Double.toString(inval[1]));
            acquisitions.setText(Integer.toString((int)inval[4]));
            if(inval[0]>0.1){
                int wl_index=VTF_IPC_GUI.get_wavelength_nr(selectLine.getSelectedItem().toString());
                int sig;
                sig=calcSNR.CalculateSNR_Line(isCustom,wl_index,inval,Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText()));
                snr.setText(Integer.toString(sig));
                results_calc=calcTimeL.CalculateTiming_Line(inval,lineidx);
                inval[11]=results_calc[0];
                VTF_IPC_GUI.time[lineidx]=results_calc[0];
                VTF_IPC_GUI.dspace[lineidx]=results_calc[1];

                lineTime.setText(Double.toString(results_calc[0]));
                resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                String roundRes=Double.toString(((double)roundval)/100.d);
                double nachkomma=(resultTab[0]*100.d)%100;
                VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
            }
            if (VTF_IPC_GUI.whichLine==lineidx)
                    waveName.requestFocus();
            
        }
        
        if(e.getSource()==VTF_IPC_GUI.Polar || e.getSource()==VTF_IPC_GUI.Doppler || e.getSource()==VTF_IPC_GUI.Intensity){
            String obsmode=VTF_IPC_GUI.ObsModeGroup.getSelection().getActionCommand();
            /*if(e.getSource()==VTF_IPC_GUI.Polar)
                obsmode="SI";
            if(e.getSource()==VTF_IPC_GUI.Doppler)
                obsmode="DI";
            if(e.getSource()==VTF_IPC_GUI.Intensity)
                obsmode="MI";*/
            //DebugInfos.append("Selected VTF Mode: "+obsmode+"\n");
            out2.append("Selected VTF Mode: " + obsmode + "\n");
            out2.append(System.getProperty("line.separator"));
            double[] input=getLineParams();
            if (input[0]>0.1 && obsmode!="MI" && !isCustom && VTF_IPC_GUI.udscansteps[lineidx]!=1)
                input[18]=Integer.parseInt(VTF_IPC_GUI.list0.get(selectLine.getSelectedIndex()-1).get(4)); //scanSum
            
            String stephalf=Integer.toString((int)input[18]/2);
            
            if(selectLine.getSelectedIndex()!=0){
                switch (obsmode){
                    case "SI":
                        //numb_cam=3;
                        //IntModeStepLabelD.setEnabled(false);
                        if(!isCustom){
                        imScanPos.setEnabled(false);
                        imScanPos.setText("0");
                        }
                        acquisitions.setText("8");
                        input[4]=8;
                        if(VTF_IPC_GUI.udscansteps[lineidx]!=1 && !isCustom){
                            scanLeft.setText("-"+stephalf);
                            scanLeft.setEnabled(true);
                            input[2]=(-1)*(int)input[18]/2;
                            scanRight.setText(stephalf);
                            scanRight.setEnabled(true);
                            input[17]=(int)input[18]/2;
                            scanSum.setText(Integer.toString((int)input[18]));
                        }
                        break;
                    case "DI":
                        if(!isCustom){
                            imScanPos.setEnabled(false);
                            imScanPos.setText("0");
                        }
                        acquisitions.setText("1");
                        input[4]=1;
                        if(VTF_IPC_GUI.udscansteps[lineidx]!=1 && !isCustom){
                            scanLeft.setEnabled(true);
                            scanRight.setEnabled(true);
                            scanLeft.setText("-"+stephalf);
                            input[2]=(-1)*(int)input[18]/2;
                            scanRight.setText(stephalf);
                            input[17]=(int)input[18]/2;
                            scanSum.setText(Integer.toString((int)input[18]));
                        }
                        break;
                    case "MI":
                        if(!isCustom){
                            imScanPos.setEnabled(true);
                            imScanPos.setEditable(true);
                            imScanPos.setText(Double.toString(input[0]));
                        }
                        if(VTF_IPC_GUI.udscansteps[lineidx]!=1 && !isCustom){
                            scanLeft.setText("0");
                            input[2]=0;
                            scanLeft.setEnabled(false);
                            scanRight.setText("0");
                            input[17]=0;
                            scanRight.setEnabled(false);
                            scanSum.setText("1");
                            //scanSum.setEnabled(false);
                            input[18]=1;
                        }
                        acquisitions.setText("1");
                        input[4]=1;
                        break;
                }
                snr.setText(Integer.toString(calcSNR.CalculateSNR_Line(isCustom, (int)input[15]-1, input, Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText()))));
                //System.out.println("debug --- switch mode to "+obsmode+" for line "+lineidx+": inputarray=\n"+Arrays.toString(input));
                results_calc=calcTimeL.CalculateTiming_Line(input,lineidx);
                input[11]=results_calc[0];
                VTF_IPC_GUI.time[lineidx]=results_calc[0];
                VTF_IPC_GUI.dspace[lineidx]=results_calc[1];
                //System.out.println("debug --- switch mode to "+obsmode+" for line "+lineidx+": time="+results_calc[0]+" diskVol="+results_calc[1]);
                lineTime.setText(Double.toString(results_calc[0]));
                resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                String roundRes=Double.toString(((double)roundval)/100.d);
                double nachkomma=(resultTab[0]*100.d)%100;
                VTF_IPC_GUI.durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
                if(VTF_IPC_GUI.whichLine==lineidx){
                    expTime.requestFocusInWindow();
                    //lineTime.revalidate();
                }
                //lineTime.firePropertyChange(this.lineTime.getName(), 0, 1);
                //lineTime.revalidate();                
            }
        }
        
        if(e.getSource()==selectROI){
            //System.out.println("debug --- selectROI = true - for line/tab: "+lineidx+" / "+tabidx);
            double[] input=getLineParams();
            methodItemChange_selectROI(input);
        }
        
    }
    
    private void Default_KeyTyped(java.awt.event.KeyEvent evt) {                                  
        char c = evt.getKeyChar();
        int unicode= evt.getKeyCode();
        //System.out.println("character : "+c);
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_PERIOD || Character.toString(c).equals("-"))) {
            evt.consume();
        }
    }

}   

