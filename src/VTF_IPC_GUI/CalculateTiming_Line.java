/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author schubert
 */
public class CalculateTiming_Line {
    
    double[] result = new double[2];
    int lineidx;
    
    public double[] CalculateTiming_Line(double[] inputval, int lineidx){
        
       //System.out.println("debug --- CalculateTimimg_Line");
        double tFPI=0.034;//time for fpi to change gap for next scan step
        double tconst;
        double nlambda,nacquisitions,nstokes,binning,texp,ts;
        nstokes=1;
        //String tes="";
        int roundval;
        int nr_iterations; //iterations for one line
        String roundRes;
        
        double rescalc0;
        rescalc0=0.0;
        //double [] inputval0;
        
        double diskSpace=0.0;
        
        int [] paramset; //readin Parameters valid? 0-> valid - 1 -> not valid!!!
        
        paramset=new int[VTF_IPC_GUI.maxLines];
        for (int k=0;k<VTF_IPC_GUI.maxLines;k++){
            paramset[k]=0;
        }
        
        String obsmode=VTF_IPC_GUI.ObsModeGroup.getSelection().getActionCommand();
        /*String obsmode="";
        if(VTF_IPC_GUI.Polar.isSelected())
            obsmode="SI";
        if(VTF_IPC_GUI.Doppler.isSelected())
            obsmode="DI";
        if(VTF_IPC_GUI.Intensity.isSelected())
            obsmode="MI";*/
        
        switch (obsmode) {
                case "DI":
                    nstokes=1.0;
                    break;
                case "MI":
                    nstokes=1.0;
                    break;
                case "SI":
                    nstokes=4.0;
                    break;
                default:
                    //DebugInfos.append("+++ Obs. Mode and Binning set correctly? \n");
                    JOptionPane.showMessageDialog(null,"Obs. Mode and Binning set correct?","warning",JOptionPane.WARNING_MESSAGE);
                    VTF_IPC_GUI.out2.append("+++ Obs. Mode and Binning set correctly? \n");
                    VTF_IPC_GUI.out2.append(System.getProperty("line.separator"));
                    //obsbinmodeset++;
                    nstokes = 1.0;
                    break;
        }
        
 
            if (inputval[0]>1.0){
                    if (inputval[1]==0){
                        //paramset[i]=1;
                        inputval[1]=25.0;
                        //setParameter(inputval,whichTab,i);
                        JOptionPane.showMessageDialog(null,"Exposure time for line not set!\n Reset to default value: "+inputval[1]+"ms","warning",JOptionPane.WARNING_MESSAGE);
                    }
                    if (inputval[18]==0){
                        //paramset[i]=2;
                        int wc;
                        wc = VTF_IPC_GUI.get_wavelength_nr(String.format("%.3f",inputval[0]));
                        inputval[18]=Double.parseDouble(VTF_IPC_GUI.list0.get(wc).get(4));
                        //setParameter(inputval,whichTab,i);
                        //DebugInfos.append("+++ spectral steps for Line "+(i+1)+" not set -> "+inputval[2]+"\n");
                        JOptionPane.showMessageDialog(null,"Spectral steps for line not set!! \n Reset to default value: "+inputval[18],"warning",JOptionPane.WARNING_MESSAGE);
                    }
                    if (inputval[4]==0){
                        //paramset[i]=3;
                        inputval[4]=1;
                        //setParameter(inputval,whichTab,i);
                        //DebugInfos.append("+++ acquisitionsulations for Line "+(i+1)+" not set -> "+inputval[4]+"\n");
                        JOptionPane.showMessageDialog(null,"Accumulations for line not set!!\n Reset to minimal value: "+inputval[4],"warning",JOptionPane.WARNING_MESSAGE);
                    }
                }
                
                                
                if (inputval[0]>0.1){
                    texp=inputval[1];
                    if(VTF_IPC_GUI.udscansteps[lineidx]==1)
                        nlambda=VTF_IPC_GUI.udss_output.length;
                    else
                        nlambda=inputval[18];
                    nacquisitions=inputval[4];
                    binning=inputval[9];
                    
                    if (inputval[10]==0){
                        nr_iterations=1;
                    } else {
                        nr_iterations=(int)(inputval[10])+1;  //iterations for one line
                    }
                    
                    Double x_double;
                    x_double = 4.d;
                    Double y_double;
                    y_double = 4.d;
                    if (inputval[12]==0){
                        x_double=4.128d;
                        y_double=4.104d;
                    }
                    if (inputval[12]==1){
                        x_double=2.048d;
                        y_double=x_double;
                    }
                    if (inputval[12]==2){
                        x_double=1.024d;
                        y_double=x_double;
                    }
                    /*if (!("4096".equals(ROI_x.getText()) || "".equals(ROI_x.getText()))) {
                        x_double = Double.parseDouble(ROI_x.getText());
                        x_double = x_double/1024.;
                    }
                    if (!("4096".equals(ROI_y.getText()) || "".equals(ROI_y.getText()))) {
                        y_double = Double.parseDouble(ROI_y.getText());
                        y_double = y_double/1024.;
                    }*/

                    diskSpace=(nr_iterations*x_double*y_double*nlambda*nacquisitions*nstokes*2.d/binning)/(1000.d);//4kx4k image as default 16bit resolution -> 2 byte - result in GB

                    if (VTF_IPC_GUI.Doppler.isSelected() || VTF_IPC_GUI.Intensity.isSelected()){
                        ts=0.000d;
                    } else {
                        ts=0.0084d;//time to switch the modulator state
                    }
                    
                    
                    double te=0.001d*texp; // millisec to sec
                    //tconst=te+ts
                    
                    //constant timing tconst for one frame is 40ms
                    tconst=0.040d;
                    
                    //intensity mode for a single line position -> FPI performs no steps....
                    if(VTF_IPC_GUI.Intensity.isSelected() && nlambda==1){
                        tFPI=0d;
                    }else{
                        //tFPI=0.034d;
                        tFPI=tconst;//tFPI == time for one frame -> attention: this time must be >= plate move time for FPI
                    }
                    
                    rescalc0 = (nr_iterations*(nstokes*nacquisitions*(tconst)+tFPI)*nlambda);
                    //calculations for  DHS data rate per second
                    double acqTime = nr_iterations*nstokes*nacquisitions*tconst*nlambda;
                    double dataRate = diskSpace/acqTime;
                    VTF_IPC_GUI.dataRates[lineidx]=dataRate;
                    double maxRate=0.0;
                    for (double rate:VTF_IPC_GUI.dataRates){
                        if(rate>maxRate)
                            maxRate=rate;
                    }
                    VTF_IPC_GUI.dataRateDHS_text.setText(new DecimalFormat("##.####").format(maxRate));
                    //take into account that the maximum frame rate is limited to 30 frames/sec - additional nfpisteps for the dead frames for each fpi move (mainly for intensity mode or exposure timings<0.2ms)
                    //if (((nr_iterations*(nstokes*nacquisitions*nlambda+nfpisteps))/rescalc0)>30){
                        //System.out.println("-------------------------------");
                        //System.out.println("attention - more than 30 f/s: actuall "+(nr_iterations*nstokes*nacquisitions*nlambda)/rescalc0+"f/s - old timing: "+rescalc0+"sec");
                        //System.out.println("Parameter - nr_iterations="+nr_iterations+" stokes="+nstokes+" acquisitions="+nacquisitions+" steps="+nlambda);
                    //    rescalc0=(nr_iterations*(nstokes*nacquisitions*nlambda+nfpisteps))/30.d;
                        //System.out.println(" -> new timing: "+rescalc0+"sec");
                    //}
                    
                    
                    roundval=(int)Math.rint(rescalc0*100.d);
                    roundRes=Double.toString(((double)roundval)/100.d);
                    inputval[11]=roundval/100.d;
                    //DebugInfos.append("scan time (filter position for tabindex/lineidex "+whichTab+"/"+i+"): "+inputval[11]+"\n");
                    //VTF_IPC_GUI.out2.append("scan time (filter position for tabindex/lineidex "+linecounter+"): "+inputval[11]+"\n");
                    VTF_IPC_GUI.out2.append(System.getProperty("line.separator"));
                    
                    //setParameter(inputval,whichTab,i);
                }            

        result[0]=inputval[11];
        result[1]=diskSpace;
        return result;
    }
}
