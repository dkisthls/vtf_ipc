/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 *
 * @author schubert
 */
public class TabBody  extends javax.swing.JPanel implements PropertyChangeListener, ActionListener, FocusListener, ItemListener{
    LineBody line1;
    LineBody line2;
    LineBody line3;
    LineBody line4;
    LineBody line5;
    LineBody line6;
    LineBody line7;
    LineBody line8;
    LineBody line9;
    LineBody line10;
    CalculateTiming_Tab ctab;
    String binningCommand;
    String roiCommand;
    String expCommand;
    
    //component list
    //Component[] comp_line;
    //event listener lists
    ActionListener[] lis;
    FocusListener[] flis;
    ItemListener[] ilis;
    MouseListener[] mlis;
     
    private int tabName;
    private JPanel tab;
    private JScrollPane scrollPane;
    private JPanel lineBoard;
    String[] value_names;
    //LineBody lineRaw;
    
    public TabBody (int tabidx){
        ctab=new CalculateTiming_Tab();
        tabName=tabidx;
        binningCommand="binningChanged";
        roiCommand="roiChanged";
        expCommand="expChanged";
        init_Tab(tabName);
        /*
        lis=new ActionListener[20][5];
        flis=new FocusListener[20][5];
        ilis=new ItemListener[20][5];
        mlis=new MouseListener[20][5];
        System.out.println("debug ms (TAB) -------- tab component counter ="+tab.getComponentCount()+"\n    name (0/1)="+tab.getComponent(0).getName()+" / "+tab.getComponent(1).getName());
        System.out.println("debug ms (TAB) counter "+tab.getComponent(1).getName()+" = "+((Container)tab.getComponent(1)).getComponentCount());
        System.out.println("debug ms (TAB) counter "+scrollPane.getViewport().getComponent(0).getName()+" = "+((Container)scrollPane.getViewport().getComponent(0)).getComponentCount());
        Component[] list0=lineBoard.getComponents();
        for(Component comp:list0){
           System.out.println("    -> name="+comp.getName()+"  -- String:"+comp.toString());
        }
        
        Component[] list = ((Container)scrollPane.getViewport().getComponent(0)).getComponents();
        
        int cc0=0;
        for (Component comp:list){
            Object name=comp.getName();
            if(name==null){
               //System.out.println("ms-debug (TAB) ----- name (comp "+cc0+"): no name defined");
            }else{
               System.out.println("ms-debug (TAB) ----- name (comp "+cc0+"): "+name);
            }
            comp_line = ((Container)comp).getComponents();
            //if(cc0==0){
                //int ccs=0;
                for(Component scomp:comp_line){
                    Object sname=scomp.getName();
                    ActionListener[] acl=scomp.getListeners(ActionListener.class);
                    System.out.println("msdebug (TABBODY) ----- get all registered listeners ("+sname+" - tab="+tabidx+"): "+Arrays.toString(acl));
                    if (acl.length>0)
                        lis[cc0]=acl;
                    FocusListener[] fcl=scomp.getFocusListeners();
                    if (fcl.length>0)
                        flis[cc0]=fcl;
                    ItemListener[] isl=scomp.getListeners(ItemListener.class);
                    if (isl.length>0)
                        ilis[cc0]=isl;
                    MouseListener[] msl=scomp.getMouseListeners();
                    if (msl.length>0)
                        mlis[cc0]=msl;
                    
                    if(cc0==0 && lis!=null){
                        if(sname==null){
                           System.out.println("   => name (subcomp "+scomp.getName()+" - "+cc0+"): no name defined");
                        }else{
                           System.out.println("   => name (subcomp "+scomp.getName()+" - "+cc0+"): "+sname);
                        }
                        if(lis!=null)
                            System.out.println("     --> actionListeners ("+sname+": length="+lis.length+"): "+Arrays.toString(lis[cc0]));
                        else
                            System.out.println("     --> no action listeners registered to "+sname);
                        if(flis!=null)
                            System.out.println("     --> focusListeners ("+sname+": length="+flis.length+"): "+Arrays.toString(flis[cc0]));
                        else
                            System.out.println("     --> no focus listeners registered to "+sname);
                        if(ilis!=null)
                            System.out.println("     --> itemListeners ("+sname+": length="+ilis.length+"): "+Arrays.toString(ilis[cc0]));
                        else
                            System.out.println("     --> no item listeners registered to "+sname);
                        if(mlis!=null)
                            System.out.println("     --> mouseListeners ("+sname+": length="+mlis.length+"): "+Arrays.toString(mlis[cc0]));
                        else
                            System.out.println("     --> no mouse listeners registered to "+sname);
                    }
                    //ccs++;
                }
            //}
            cc0++;
        }
        System.out.println("ms-debug (TAB) ---- line1 (counts="+line1.getComponentCount()+"):"+line1.getName());
        */
    }

    private void init_Tab(int tabidx){
        tab = new JPanel();
        tab.setName("tab_"+tabidx);
        switch(tabidx){
            case 0:
                value_names=new String[]{"selectLine","selectScanMode","selectBinning","selectROI","waveName","expTime","scanLeft","scanRight","scanSum","specStep","acquisitions","snr","imScanPos","iterations","lineTime"};
                break;
            case 1:
                value_names=new String[]{"selectLine","selectScanMode","selectBinning","selectROI","waveName","expTime","scanLeft","scanRight","scanSum","specStep","selectspecStep","acquisitions","snr","imScanPos","iterations","lineTime"};
                break;
            case 2:
                value_names=new String[]{"selectLine","selectScanMode","selectBinning","selectROI","waveName","expTime","scanLeft","scanRight","scanSum","specStep","selectspecStep","acquisitions","snr","imScanPos","iterations","lineTime","userdefSteps"};
                break;
        }
        JPanel nameBoard = createNameBoard(tabidx);
        nameBoard.setName("nameBoard");
        VTF_IPC_GUI.Polar.addItemListener(this);
        VTF_IPC_GUI.Doppler.addItemListener(this);
        VTF_IPC_GUI.Intensity.addItemListener(this);
        //nameBoard.setPreferredSize(new Dimension(200,450));
        lineBoard = createLineBoard(tabidx);
        lineBoard.setName("lineBoard");
        //lineBoard.setPreferredSize(new Dimension(1360,500));
        scrollPane = new JScrollPane(lineBoard);
        scrollPane.setName("scrollPane");
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.setPreferredSize(new Dimension(360,485));
        
        scrollPane.setViewportView(lineBoard);
        GridBagLayout gridB = new GridBagLayout();
        GridBagConstraints cbc = new GridBagConstraints();

        tab.setLayout(gridB);
        cbc.fill = GridBagConstraints.BOTH;
        cbc.insets=new Insets(0,0,0,2);
        cbc.gridx=0;
        cbc.gridy=0;
        cbc.gridwidth=2;
        cbc.gridheight=16;
        gridB.setConstraints(nameBoard, cbc);
        tab.add(nameBoard);
        
        cbc.gridx=2;
        cbc.gridy=0;
        cbc.gridwidth=4;
        cbc.gridheight=16;
        gridB.setConstraints(scrollPane, cbc);
        //System.out.println("size lineBoard = "+lineBoard.getSize());
        tab.add(scrollPane);
        //tab.setVisible(true);

    }
    
    private JPanel createLineBoard(int tabidx){
        JPanel lineBoard0 = new JPanel();
        GridBagLayout gridB = new GridBagLayout();
        GridBagConstraints cons = new GridBagConstraints();
        cons.fill=GridBagConstraints.HORIZONTAL;
        cons.insets = new Insets(5,10,5,10);
        lineBoard0.setLayout(gridB);
        
        
        line1=createSingleLine(line1,gridB,cons,0,tabidx);
        lineBoard0.add(line1.getLine());
        
        line2=createSingleLine(line2,gridB,cons,1,tabidx);
        lineBoard0.add(line2.getLine());
        
        line3=createSingleLine(line3,gridB,cons,2,tabidx);
        lineBoard0.add(line3.getLine());
        
        line4=createSingleLine(line4,gridB,cons,3,tabidx);
        lineBoard0.add(line4.getLine());
        
        line5=createSingleLine(line5,gridB,cons,4,tabidx);
        lineBoard0.add(line5.getLine());
        
        line6=createSingleLine(line6,gridB,cons,5,tabidx);
        lineBoard0.add(line6.getLine());
        
        line7=createSingleLine(line7,gridB,cons,6,tabidx);
        lineBoard0.add(line7.getLine());
        
        line8=createSingleLine(line8,gridB,cons,7,tabidx);
        lineBoard0.add(line8.getLine());
        
        line9=createSingleLine(line9,gridB,cons,8,tabidx);
        lineBoard0.add(line9.getLine());
        
        line10=createSingleLine(line10,gridB,cons,9,tabidx);
        lineBoard0.add(line10.getLine());
        
        return lineBoard0;
    }
    
    private LineBody createSingleLine(LineBody line, GridBagLayout gridB, GridBagConstraints cons ,int lineindex, int tabindex){
        //todo get rid of code for each line...
        line = new LineBody(lineindex,tabindex);
        int nameIdx=lineindex+1;
        line.setName("line"+nameIdx);
        //line10.accessCycTime().addPropertyChangeListener(TabBody.this);
        addFocusListener_singleLine(line);
        addActionListener_binning(line,binningCommand);
        addActionListener_roi(line,roiCommand);
        addActionListener_expTime(line, expCommand);
        cons.gridx=lineindex;
        cons.gridy=0;
        gridB.setConstraints(line, cons);
        return line;
    }
    
    private JPanel createNameBoard(int tabidx){
        JPanel nameBoard = new JPanel();
        GridBagLayout gridB = new GridBagLayout();
        GridBagConstraints cons = new GridBagConstraints();
        cons.fill=GridBagConstraints.BOTH;
        cons.insets = new Insets(1,0,1,0);
        cons.weightx=0.0;
        nameBoard.setLayout(gridB);
        
        JLabel name_l = new JLabel("Line Counter");
        cons.gridx=0;
        cons.gridy=0;
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        gridB.setConstraints(name_l, cons);
        nameBoard.add(name_l,cons);
        
        JTextField name_cb = new JTextField("Line");
        name_cb.setBackground(Color.LIGHT_GRAY);
        name_cb.setEditable(false);
        name_cb.setPreferredSize(new Dimension(120,25));
        cons.gridx=0;
        cons.gridy=1;
        gridB.setConstraints(name_cb, cons);
        nameBoard.add(name_cb,cons);
        
        JTextField name_sm = new JTextField("Scan Mode");
        name_sm.setBackground(Color.LIGHT_GRAY);
        name_sm.setEditable(false);
        name_sm.setPreferredSize(new Dimension(120,25));
        cons.gridx=0;
        cons.gridy=2;
        gridB.setConstraints(name_sm, cons);
        nameBoard.add(name_sm,cons);
        
        JTextField name_bin = new JTextField("Binning");
        name_bin.setBackground(Color.LIGHT_GRAY);
        name_bin.setEditable(false);
        name_bin.setPreferredSize(new Dimension(120,25));
        cons.gridx=0;
        cons.gridy=3;
        gridB.setConstraints(name_bin, cons);
        nameBoard.add(name_bin,cons);
        
        JTextField name_ROI = new JTextField("ROI");
        name_ROI.setBackground(Color.LIGHT_GRAY);
        name_ROI.setEditable(false);
        name_ROI.setPreferredSize(new Dimension(120,25));
        cons.gridx=0;
        cons.gridy=4;
        gridB.setConstraints(name_ROI, cons);
        nameBoard.add(name_ROI,cons);
        
        cons.insets = new Insets(1,0,2,0);
        JTextField name_cw = new JTextField("Central Wavelength (nm)");
        name_cw.setBackground(Color.LIGHT_GRAY);
        name_cw.setEditable(false);
        cons.gridx=0;
        cons.gridy=5;
        gridB.setConstraints(name_cw, cons);
        nameBoard.add(name_cw,cons);
        
        JTextField name_et = new JTextField("Exposure Time");
        name_et.setBackground(Color.LIGHT_GRAY);
        name_et.setEditable(false);
        cons.gridx=0;
        cons.gridy=6;
        gridB.setConstraints(name_et, cons);
        nameBoard.add(name_et,cons);
        
        JTextField name_ss = new JTextField("Steps: Left-Right-Sum");
        name_ss.setBackground(Color.LIGHT_GRAY);
        name_ss.setEditable(false);
        cons.gridx=0;
        cons.gridy=7;
        gridB.setConstraints(name_ss, cons);
        nameBoard.add(name_ss,cons);
        
        JTextField name_spec = new JTextField("Spectral Step (pm)");
        name_spec.setBackground(Color.LIGHT_GRAY);
        name_spec.setEditable(false);
        cons.gridx=0;
        cons.gridy=8;
        gridB.setConstraints(name_spec, cons);
        nameBoard.add(name_spec,cons);
        
        JTextField name_ac = new JTextField("Acquisitions");
        name_ac.setBackground(Color.LIGHT_GRAY);
        name_ac.setEditable(false);
        cons.gridx=0;
        cons.gridy=9;
        gridB.setConstraints(name_ac, cons);
        nameBoard.add(name_ac,cons);
        
        JTextField name_snr = new JTextField("SNR");
        name_snr.setBackground(Color.LIGHT_GRAY);
        name_snr.setEditable(false);
        cons.gridx=0;
        cons.gridy=10;
        gridB.setConstraints(name_snr, cons);
        nameBoard.add(name_snr,cons);
        
        JTextField name_sp = new JTextField("Scan Position (nm)");
        name_sp.setBackground(Color.LIGHT_GRAY);
        name_sp.setEditable(false);
        cons.gridx=0;
        cons.gridy=11;
        gridB.setConstraints(name_sp, cons);
        nameBoard.add(name_sp,cons);
        
        JTextField name_rp = new JTextField("Iterations");
        name_rp.setBackground(Color.LIGHT_GRAY);
        name_rp.setEditable(false);
        cons.gridx=0;
        cons.gridy=12;
        gridB.setConstraints(name_rp, cons);
        nameBoard.add(name_rp,cons);
        
        JTextField name_ct = new JTextField("Timing (sec)");
        name_ct.setBackground(Color.LIGHT_GRAY);
        name_ct.setEditable(false);
        cons.gridx=0;
        cons.gridy=13;
        gridB.setConstraints(name_ct, cons);
        nameBoard.add(name_ct,cons);
        
        if(tabidx==2){
            JTextField name_usteps = new JTextField("Userdefined Scan Steps");
            name_usteps.setBackground(Color.LIGHT_GRAY);
            name_usteps.setEditable(false);
            cons.gridx=0;
            cons.gridy=14;
            gridB.setConstraints(name_usteps, cons);
            nameBoard.add(name_usteps,cons);
        }
        
        JTextField dummy = new JTextField("Push to Reset Line");
        dummy.setBackground(Color.LIGHT_GRAY);
        dummy.setEditable(false);
        cons.gridx=0;
        cons.gridy=15;
        cons.insets = new Insets(2,0,13,0);
        gridB.setConstraints(dummy, cons);
        nameBoard.add(dummy,cons);
        
        
        return nameBoard;

    }
    
    protected void removeActionListeners_all(){
        /*int cc=0;
        for(Component comp:comp_line){
            ActionListener[] sublist=lis[cc];
            
            for (ActionListener sublist1 : sublist) {
                if (comp instanceof JTextField) {
                    ((JTextField)comp).removeActionListener(sublist1);
                }
                if (comp instanceof JComboBox) {
                    ((JComboBox)comp).removeActionListener(sublist1);
                }   
            }
            cc++;
        }*/
        line1.removeActionFocusListenerExpTime(this);
        line1.removeActionListenerBinning(this);
        line1.removeActionListenerROI(this);
        line2.removeActionFocusListenerExpTime(this);
        line2.removeActionListenerBinning(this);
        line2.removeActionListenerROI(this);
        line3.removeActionFocusListenerExpTime(this);
        line3.removeActionListenerBinning(this);
        line3.removeActionListenerROI(this);
        line4.removeActionFocusListenerExpTime(this);
        line4.removeActionListenerBinning(this);
        line4.removeActionListenerROI(this);
        line5.removeActionFocusListenerExpTime(this);
        line5.removeActionListenerBinning(this);
        line5.removeActionListenerROI(this);
        line6.removeActionFocusListenerExpTime(this);
        line6.removeActionListenerBinning(this);
        line6.removeActionListenerROI(this);
        line7.removeActionFocusListenerExpTime(this);
        line7.removeActionListenerBinning(this);
        line7.removeActionListenerROI(this);
        line8.removeActionFocusListenerExpTime(this);
        line8.removeActionListenerBinning(this);
        line8.removeActionListenerROI(this);
        line9.removeActionFocusListenerExpTime(this);
        line9.removeActionListenerBinning(this);
        line9.removeActionListenerROI(this);
        line10.removeActionFocusListenerExpTime(this);
        line10.removeActionListenerBinning(this);
        line10.removeActionListenerROI(this);
    }
    
    protected void addActionListeners_all(){
        /*int cc=0;
        for(Component comp:comp_line){
            ActionListener[] sublist=lis[cc];
            
            for (ActionListener sublist1 : sublist) {
                if (comp instanceof JTextField) {
                    ((JTextField)comp).addActionListener(sublist1);
                }
                if (comp instanceof JComboBox) {
                    ((JComboBox)comp).addActionListener(sublist1);
                }   
            }
            cc++;
        }*/
        line1.addActionFocusListenerExpTime(this,expCommand);
        line1.addActionListenerBinning(this,binningCommand);
        line1.addActionListenerROI(this,roiCommand);
        line2.addActionFocusListenerExpTime(this,expCommand);
        line2.addActionListenerBinning(this,binningCommand);
        line2.addActionListenerROI(this,roiCommand);
        line3.addActionFocusListenerExpTime(this,expCommand);
        line3.addActionListenerBinning(this,binningCommand);
        line3.addActionListenerROI(this,roiCommand);
        line4.addActionFocusListenerExpTime(this,expCommand);
        line4.addActionListenerBinning(this,binningCommand);
        line4.addActionListenerROI(this,roiCommand);
        line5.addActionFocusListenerExpTime(this,expCommand);
        line5.addActionListenerBinning(this,binningCommand);
        line5.addActionListenerROI(this,roiCommand);
        line6.addActionFocusListenerExpTime(this,expCommand);
        line6.addActionListenerBinning(this,binningCommand);
        line6.addActionListenerROI(this,roiCommand);
        line7.addActionFocusListenerExpTime(this,expCommand);
        line7.addActionListenerBinning(this,binningCommand);
        line7.addActionListenerROI(this,roiCommand);
        line8.addActionFocusListenerExpTime(this,expCommand);
        line8.addActionListenerBinning(this,binningCommand);
        line8.addActionListenerROI(this,roiCommand);
        line9.addActionFocusListenerExpTime(this,expCommand);
        line9.addActionListenerBinning(this,binningCommand);
        line9.addActionListenerROI(this,roiCommand);
        line10.addActionFocusListenerExpTime(this,expCommand);
        line10.addActionListenerBinning(this,binningCommand);
        line10.addActionListenerROI(this,roiCommand);
    }
    
    public JPanel getPanel (){
        return this.tab;
    }
    
    public void setLine (int line_nr, double[] input){
       //System.out.println("debug ms TabBody.setLine "+line_nr);
       removeActionListeners_all();
        switch (line_nr){
            case 0:
                
                line1.setSelections(input); //order is important due to actionPerformed for selectLine
                line1.setLineParams(input);
                
                break;
            case 1:
                
                line2.setSelections(input);
                line2.setLineParams(input);
                
                break;
            case 2:
                line3.setSelections(input);
                line3.setLineParams(input);
                break;
            case 3:
                line4.setSelections(input);
                line4.setLineParams(input);
                break;
            case 4:
                line5.setSelections(input);
                line5.setLineParams(input);
                break;
            case 5:
                line6.setSelections(input);
                line6.setLineParams(input);
                break;
            case 6:
                line7.setSelections(input);
                line7.setLineParams(input);
                break;
            case 7:
                line8.setSelections(input);
                line8.setLineParams(input);
                break;
            case 8:
                line9.setSelections(input);
                line9.setLineParams(input);
                break;
            case 9:
                line10.setSelections(input);
                line10.setLineParams(input);
                break;
            default:
                //System.out.println("no input or line nr provided!!!");
                break;

        }
        addActionListeners_all();
    }

    public double[] getLine (int line_nr){
        double[] output=null;
        switch (line_nr){
            case 0:
                output=line1.getLineParams();
                break;
            case 1:
                output=line2.getLineParams();
                break;
            case 2:
                output=line3.getLineParams();
                break;
            case 3:
                output=line4.getLineParams();
                break;
            case 4:
                output=line5.getLineParams();
                break;
            case 5:
                output=line6.getLineParams();
                break;
            case 6:
                output=line7.getLineParams();
                break;
            case 7:
                output=line8.getLineParams();
                break;
            case 8:
                output=line9.getLineParams();
                break;
            case 9:
                output=line10.getLineParams();
                break;
            default:
                //System.out.println(" no output was generated ");
                break;

        }
        return output;
    }

    public double[][] getCalculationResults(){
        double[][] results=new double[10][2];
        results[0][0]=line1.getCalculationsLine()[0];
        results[0][1]=line1.getCalculationsLine()[1];

        results[1][0]=line2.getCalculationsLine()[0];
        results[1][1]=line2.getCalculationsLine()[1];

        results[2][0]=line3.getCalculationsLine()[0];
        results[2][1]=line3.getCalculationsLine()[1];

        results[3][0]=line4.getCalculationsLine()[0];
        results[3][1]=line4.getCalculationsLine()[1];

        results[4][0]=line5.getCalculationsLine()[0];
        results[4][1]=line5.getCalculationsLine()[1];

        results[5][0]=line6.getCalculationsLine()[0];
        results[5][1]=line6.getCalculationsLine()[1];

        results[6][0]=line7.getCalculationsLine()[0];
        results[6][1]=line7.getCalculationsLine()[1];

        results[7][0]=line8.getCalculationsLine()[0];
        results[7][1]=line8.getCalculationsLine()[1];

        results[8][0]=line9.getCalculationsLine()[0];
        results[8][1]=line9.getCalculationsLine()[1];

        results[9][0]=line10.getCalculationsLine()[0];
        results[9][1]=line10.getCalculationsLine()[1];

        return results;
    }
    
    public void setLineActive(int lineNr,int tabidx){
        //System.out.println("debug --- TAB: set line inactive for Tab "+tabidx+" -> VTF Tab = "+VTF_IPC_GUI.whichTab);
        switch(lineNr){
            case 0:
                this.line1.setLineActive(tabidx);
                break;
            case 1:
                this.line2.setLineActive(tabidx);
                break;
            case 2:
                this.line3.setLineActive(tabidx);
                break;
            case 3:
                this.line4.setLineActive(tabidx);
                break;
            case 4:
                this.line5.setLineActive(tabidx);
                break;
            case 5:
                this.line6.setLineActive(tabidx);
                break;
            case 6:
                this.line7.setLineActive(tabidx);
                break;
            case 7:
                this.line8.setLineActive(tabidx);
                break;
            case 8:
                this.line9.setLineActive(tabidx);
                break;
            case 9:
                this.line10.setLineActive(tabidx);
                break;
        }
    }

    public void setLineInActive(int lineNr, int tabidx){
        switch(lineNr){
            case 0:
                this.line1.setLineInActive(tabidx);
                break;
            case 1:
                this.line2.setLineInActive(tabidx);
                break;
            case 2:
                this.line3.setLineInActive(tabidx);
                break;
            case 3:
                this.line4.setLineInActive(tabidx);
                break;
            case 4:
                this.line5.setLineInActive(tabidx);
                break;
            case 5:
                this.line6.setLineInActive(tabidx);
                break;
            case 6:
                this.line7.setLineInActive(tabidx);
                break;
            case 7:
                this.line8.setLineInActive(tabidx);
                break;
            case 8:
                this.line9.setLineInActive(tabidx);
                break;
            case 9:
                this.line10.setLineInActive(tabidx);
                break;
        }
    }
    
    public boolean lineIsActive(int lineNr){
        boolean state=false;
        switch(lineNr){
            case 0:
                state=line1.isActive;
                break;
            case 1:
                state=line2.isActive;
                break;
            case 2:
                state=line3.isActive;
                break;
            case 3:
                state=line4.isActive;
                break;
            case 4:
                state=line5.isActive;
                break;
            case 5:
                state=line6.isActive;
                break;
            case 6:
                state=line7.isActive;
                break;
            case 7:
                state=line8.isActive;
                break;
            case 8:
                state=line9.isActive;
                break;
            case 9:
                state=line10.isActive;
                break;
        }
        return state;
    }

    public boolean lineIsCustom(int lineNr){
        boolean state=false;
        switch(lineNr){
            case 0:
                state=line1.isCustom;
                break;
            case 1:
                state=line2.isCustom;
                break;
            case 2:
                state=line3.isCustom;
                break;
            case 3:
                state=line4.isCustom;
                break;
            case 4:
                state=line5.isCustom;
                break;
            case 5:
                state=line6.isCustom;
                break;
            case 6:
                state=line7.isCustom;
                break;
            case 7:
                state=line8.isCustom;
                break;
            case 8:
                state=line9.isCustom;
                break;
            case 9:
                state=line10.isCustom;
                break;
        }
        return state;
    }

    public void setSNR_Tab(int lineNr, int sig){
        switch(lineNr){
            case 0:
                line1.setSNR_Line(sig);
                break;
            case 1:
                line2.setSNR_Line(sig);
                break;
            case 2:
                line3.setSNR_Line(sig);
                break;
            case 3:
                line4.setSNR_Line(sig);
                break;
            case 4:
                line5.setSNR_Line(sig);
                break;
            case 5:
                line6.setSNR_Line(sig);
                break;
            case 6:
                line7.setSNR_Line(sig);
                break;
            case 7:
                line8.setSNR_Line(sig);
                break;
            case 8:
                line9.setSNR_Line(sig);
                break;
            case 9:
                line10.setSNR_Line(sig);
                break;
        }
    }
    
    public void setUserSteps(int lineidx,String filename){
        switch(lineidx){
            case 0:
                line1.setUserDefFiles(filename);
                line1.setLineEditable(tabName);
                break;
            case 1:
                line2.setUserDefFiles(filename);
                line2.setLineEditable(tabName);
                break;
            case 2:
                line3.setUserDefFiles(filename);
                line3.setLineEditable(tabName);
                break;
            case 3:
                line4.setUserDefFiles(filename);
                line4.setLineEditable(tabName);
                break;
            case 4:
                line5.setUserDefFiles(filename);
                line5.setLineEditable(tabName);
                break;
            case 5:
                line6.setUserDefFiles(filename);
                line6.setLineEditable(tabName);
                break;
            case 6:
                line7.setUserDefFiles(filename);
                line7.setLineEditable(tabName);
                break;
            case 7:
                line8.setUserDefFiles(filename);
                line8.setLineEditable(tabName);
                break;
            case 8:
                line9.setUserDefFiles(filename);
                line9.setLineEditable(tabName);
                break;
            case 9:
                line10.setUserDefFiles(filename);
                line10.setLineEditable(tabName);
                break;
        }
    }

    public String getUserSteps(int lineidx){
        String filename="";
        switch(lineidx){
            case 0:
                filename=line1.accessUserDef().getText();
                break;
            case 1:
                filename=line2.accessUserDef().getText();
                break;
            case 2:
                filename=line3.accessUserDef().getText();
                break;
            case 3:
                filename=line4.accessUserDef().getText();
                break;
            case 4:
                filename=line5.accessUserDef().getText();
                break;
            case 5:
                filename=line6.accessUserDef().getText();
                break;
            case 6:
                filename=line7.accessUserDef().getText();
                break;
            case 7:
                filename=line8.accessUserDef().getText();
                break;
            case 8:
                filename=line9.accessUserDef().getText();
                break;
            case 9:
                filename=line10.accessUserDef().getText();
                break;
        }
        return filename;
    }

    public void resetTab(){
        removeActionListeners_all();
        line1.resetLine();
        line2.resetLine();
        line3.resetLine();
        line4.resetLine();
        line5.resetLine();
        line6.resetLine();
        line7.resetLine();
        line8.resetLine();
        line9.resetLine();
        line10.resetLine();
        addActionListeners_all();
    }
    public void addActionListener_Tab(LineBody line, String name){
        line.addActionListener_Line(this, name);
    }
    
    public void addActionListener_binning(LineBody line, String ac){
        line.addActionListenerBinning(this, ac);
    }
    
    public void addActionListener_roi(LineBody line, String ac){
        line.addActionListenerROI(this, ac);
    }
    
    public void addActionListener_expTime(LineBody line, String ac){
        line.addActionFocusListenerExpTime(this, ac);
    }
    
    public void addFocusListener_singleLine(LineBody line){
        for(String name:value_names){
            line.addFocusListener_Line(this, name);
        }
    }
    
    public void addFocusListener_Tab(int lineNr){
        switch(lineNr){
            case 0:
                for(String name:value_names){
                    line1.addFocusListener_Line(this, name);
                }
                break;
            case 1:
                for(String name:value_names){
                    line2.addFocusListener_Line(this, name);
                }
                break;
            case 2:
                for(String name:value_names){
                    line3.addFocusListener_Line(this, name);
                }
                break;
            case 3:
                for(String name:value_names){
                    line4.addFocusListener_Line(this, name);
                }
                break;
            case 4:
                for(String name:value_names){
                    line5.addFocusListener_Line(this, name);
                }
                break;
            case 5:
                for(String name:value_names){
                    line6.addFocusListener_Line(this, name);
                }
                break;
            case 6:
                for(String name:value_names){
                    line7.addFocusListener_Line(this, name);
                }
                break;
            case 7:
                for(String name:value_names){
                    line8.addFocusListener_Line(this, name);
                }
                break;
            case 8:
                for(String name:value_names){
                    line9.addFocusListener_Line(this, name);
                }
                break;
            case 9:
                for(String name:value_names){
                    line10.addFocusListener_Line(this, name);
                }
                break;
        }
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //System.out.println("debug --- PropertyChange Tab - lineTime was set!");
        double[][] resCalc;
        double[] result;
        
        result=ctab.CalculateTiming_Tab(VTF_IPC_GUI.time,VTF_IPC_GUI.dspace,Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()),Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
        
        int roundval=(int)Math.rint((result[0])*100.d+0.5);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(result[0]*100.d)%100;
        VTF_IPC_GUI.durationObs_text.setText((int)result[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        //VTF_IPC_GUI.durationObs_text.setText(Double.toString(result[0]));
        VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(result[1])+" GB");
        //System.out.println("cycleTime = "+(int)result[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        //System.out.println("disk space = "+result[1]+"GB");
        tab.revalidate();
        
    }

    @Override
    public void focusGained(FocusEvent e) {
        VTF_IPC_GUI.line_active[VTF_IPC_GUI.whichLine]=true;
        //setLineActive(VTF_IPC_GUI.whichLine);
        for(int i=0;i<VTF_IPC_GUI.maxLines;i++){
            if(i!=VTF_IPC_GUI.whichLine)
                setLineInActive(i,tabName);
        }
        /*double[][] resCalc=getCalculationResults();
        
        double[] result=ctab.CalculateTiming_Tab(VTF_IPC_GUI.time,VTF_IPC_GUI.dspace,Integer.parseInt(VTF_IPC_GUI.cycleNumb_text.getText()),Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
        int roundval=(int)Math.rint((result[0])*100.d+0.5);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(result[0]*100.d)%100;
        VTF_IPC_GUI.durationObs_text.setText((int)result[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        //VTF_IPC_GUI.durationObs_text.setText(Double.toString(result[0]));
        VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(result[1])+" GB");
        */
    }

    @Override
    public void focusLost(FocusEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        //double[][] resCalc=getCalculationResults();
        
        double[] result=ctab.CalculateTiming_Tab(VTF_IPC_GUI.time,VTF_IPC_GUI.dspace,Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()),Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
        int roundval=(int)Math.rint((result[0])*100.d+0.5);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(result[0]*100.d)%100;
        VTF_IPC_GUI.durationObs_text.setText((int)result[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        VTF_IPC_GUI.durationObs_text.revalidate();
        //VTF_IPC_GUI.durationObs_text.setText(Double.toString(result[0]));
        VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(result[1])+" GB");
        switch(VTF_IPC_GUI.whichLine){
            case 0:
                line1.accessCycTime().requestFocusInWindow();
                break;
            case 1:
                line2.accessCycTime().requestFocusInWindow();
                break;
            case 2:
                line3.accessCycTime().requestFocusInWindow();
                break;
            case 3:
                line4.accessCycTime().requestFocusInWindow();
                break;
            case 4:
                line5.accessCycTime().requestFocusInWindow();
                break;
            case 5:
                line6.accessCycTime().requestFocusInWindow();
                break;
            case 6:
                line7.accessCycTime().requestFocusInWindow();
                break;
            case 7:
                line8.accessCycTime().requestFocusInWindow();
                break;
            case 8:
                line9.accessCycTime().requestFocusInWindow();
                break;
            case 9:
                line10.accessCycTime().requestFocusInWindow();
                break;
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //System.out.println("ms debug (TAB) -------- actionPerformed  (Tab) Action Command fired ("+e.getActionCommand().toString()+")");
        if (e.getActionCommand()==(binningCommand)){
           //System.out.println("ms debug (TAB) -------- binning selected!!!! ActionEvent e ->"+e.getActionCommand());
            Object selection = null;
            int lineNum=0;
            lineNum=VTF_IPC_GUI.whichLine;

           //System.out.println("ms debug ---- active Line = "+lineNum+" in tab = "+tabName);
            switch(lineNum){
                case 0:
                    selection=line1.getBinning();
                    break;
                case 1:
                    selection=line2.getBinning();
                    break;
                case 2:
                    selection=line3.getBinning();
                    break;
                case 3:
                    selection=line4.getBinning();
                    break;
                case 4:
                    selection=line5.getBinning();
                    break;
                case 5:
                    selection=line6.getBinning();
                    break;
                case 6:
                    selection=line7.getBinning();
                    break;
                case 7:
                    selection=line8.getBinning();
                    break;
                case 8:
                    selection=line9.getBinning();
                    break;
                case 9:
                    selection=line10.getBinning();
                    break;
                default:
                    break;
            }
           //System.out.println("ms debug --- selection = "+selection.toString());
            removeActionListeners_all();
            line1.setBinning(selection);
            line2.setBinning(selection);
            line3.setBinning(selection);
            line4.setBinning(selection);
            line5.setBinning(selection);
            line6.setBinning(selection);
            line7.setBinning(selection);
            line8.setBinning(selection);
            line9.setBinning(selection);
            line10.setBinning(selection);
            addActionListeners_all();
            //selectLine(lineNum);
        }
        
        if (e.getActionCommand()==(roiCommand)){
           //System.out.println("ms debug (TAB) -------- roi selected!!!! ActionEvent e ->"+e.getActionCommand());
            Object selection = null;
            int lineNum=0;
            lineNum=VTF_IPC_GUI.whichLine;

           //System.out.println("ms debug ---- active Line = "+lineNum+" in tab = "+tabName);
            switch(lineNum){
                case 0:
                    selection=line1.getROI();
                    break;
                case 1:
                    selection=line2.getROI();
                    break;
                case 2:
                    selection=line3.getROI();
                    break;
                case 3:
                    selection=line4.getROI();
                    break;
                case 4:
                    selection=line5.getROI();
                    break;
                case 5:
                    selection=line6.getROI();
                    break;
                case 6:
                    selection=line7.getROI();
                    break;
                case 7:
                    selection=line8.getROI();
                    break;
                case 8:
                    selection=line9.getROI();
                    break;
                case 9:
                    selection=line10.getROI();
                    break;
                default:
                    break;
            }
           //System.out.println("ms debug --- selection = "+selection.toString());

            removeActionListeners_all();
            line1.setROI(selection);
            line2.setROI(selection);
            line3.setROI(selection);
            line4.setROI(selection);
            line5.setROI(selection);
            line6.setROI(selection);
            line7.setROI(selection);
            line8.setROI(selection);
            line9.setROI(selection);
            line10.setROI(selection);
            addActionListeners_all();//selectLine(lineNum);
        }
        
        if (e.getActionCommand()==(expCommand)){
           //System.out.println("ms debug (TAB) -------- exp selected!!!! ActionEvent e ->"+e.getActionCommand());
            String selection = null;
            int lineNum=0;
            lineNum=VTF_IPC_GUI.whichLine;

           //System.out.println("ms debug ---- active Line = "+lineNum+" in tab = "+tabName);
            switch(lineNum){
                case 0:
                    selection=line1.getExpTime();
                    break;
                case 1:
                    selection=line2.getExpTime();
                    break;
                case 2:
                    selection=line3.getExpTime();
                    break;
                case 3:
                    selection=line4.getExpTime();
                    break;
                case 4:
                    selection=line5.getExpTime();
                    break;
                case 5:
                    selection=line6.getExpTime();
                    break;
                case 6:
                    selection=line7.getExpTime();
                    break;
                case 7:
                    selection=line8.getExpTime();
                    break;
                case 8:
                    selection=line9.getExpTime();
                    break;
                case 9:
                    selection=line10.getExpTime();
                    break;
                default:
                    break;
            }
           //System.out.println("ms debug --- selection = "+selection.toString());

            removeActionListeners_all();
            line1.setExpTime(selection);
            line2.setExpTime(selection);
            line3.setExpTime(selection);
            line4.setExpTime(selection);
            line5.setExpTime(selection);
            line6.setExpTime(selection);
            line7.setExpTime(selection);
            line8.setExpTime(selection);
            line9.setExpTime(selection);
            line10.setExpTime(selection);
            addActionListeners_all();//selectLine(lineNum);
        }
    }

    private void selectLine(int lineNum) {
        switch(lineNum){
            case 0:
                line1.waveName.requestFocusInWindow();
                break;
            case 1:
                line2.waveName.requestFocusInWindow();
                break;
            case 2:
                line3.waveName.requestFocusInWindow();
                break;
            case 3:
                line4.waveName.requestFocusInWindow();
                break;
            case 4:
                line5.waveName.requestFocusInWindow();
                break;
            case 5:
                line6.waveName.requestFocusInWindow();
                break;
            case 6:
                line7.waveName.requestFocusInWindow();
                break;
            case 7:
                line8.waveName.requestFocusInWindow();
                break;
            case 8:
                line9.waveName.requestFocusInWindow();
                break;
            case 9:
                line10.waveName.requestFocusInWindow();
                break;
            default:
                break;
        }
    }

}
