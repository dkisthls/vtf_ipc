/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

/**
 *
 * @author schubert
 */
public class CalculateTiming_Tab {
    double[] result=new double[2];
    
    public double[] CalculateTiming_Tab(double[] timings, double[] diskSpace, int cycnumb, double delayObs){
        
        double diskVol, time, Delta_t, cycletiming,cycletime_all, roundval;
        int linecounter=0;
        cycletiming=0.0;
        cycletime_all=0.0;
        diskVol=0.0;
        time=0;
        roundval=0;
        Delta_t=0.0; //delay between two filter positions for cooling if line timimng is below 2sec
       //System.out.println("read in timings for "+timings.length+" lines");
        
        for (int i=0;i<timings.length;i++){
            //take the filter wheel cooling into account: if time for data aquisition per line is shorter than 2sec we have to wait!!!
            if (timings[i]>0.0005){
                if (linecounter==1)
                    cycletiming+=Delta_t;//taking the first line into account if more than one line is observed...
                        
                if (timings[i] < 2.0){
                    Delta_t = 2.0-timings[i]; //take the new line into account
                    if (linecounter>0)
                        cycletiming+=Delta_t;
                }
                    
                cycletiming+=timings[i];
               //System.out.println("cyclce timimng actual = "+cycletiming);
                diskVol+=diskSpace[i];
               //System.out.println("disk volume actual = "+diskVol);
                linecounter++;
            }
        }
        
        //roundval=(int)Math.rint(cycletiming*100.d);
            
        if(linecounter>0){
            if (cycnumb==1){
                linecounter=linecounter-1;
                if (linecounter==0)
                    Delta_t=0;
                cycletime_all=(cycletiming-Delta_t+2d*(double)linecounter)/60.d; //'-Delta_t': if only 1 cycle is done no need for extra cooling time at the end of the run
            }
            if (cycnumb > 1){
                if (linecounter==1){
                    cycletime_all=(((double)cycnumb)*(cycletiming+delayObs)-delayObs)/60.d;
                }else{
                    cycletime_all=((((double)cycnumb)*(cycletiming+(2d)*(double)linecounter+delayObs))-2.0-delayObs-Delta_t)/60.d;
                }
            } 
            time=cycletime_all;

            roundval=(int)Math.rint(cycnumb*diskVol*100.d);
            diskVol=(roundval)/100.d;
        }    
        
        result[0]=time;
        result[1]=diskVol;

        return result;
    }
}
