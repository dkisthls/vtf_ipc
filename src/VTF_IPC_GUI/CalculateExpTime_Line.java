/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

/**
 *
 * @author schubert
 */
public class CalculateExpTime_Line {
    public double[] CalculateExpTime_Line(int lineidx, int tabidx, double[] inval, boolean isCustom){
       //System.out.println("debug --- CaclulateExpTime_Line");
       
       //System.out.println("ms-debug (CaclulateExposureTime_line) selectLine - index="+inval[15]);
        
        if (!isCustom && inval[15]>0){        
            double ext0, signoise;
            int acc0;
            acc0=1;
            inval[4]=acc0;
            signoise=inval[5];
            if (inval[0]>0.0){
                double expTime0=inval[1];
                int wl_counter1=VTF_IPC_GUI.get_wavelength_nr(String.format("%.3f",inval[0]));
               //System.out.println("wave length number: "+wl_counter1);
                ext0=(Math.pow(signoise, 2)*25.d)/(Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText())*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(5))/0.75d)*inval[9]*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(2))*2*acc0);
                if (ext0>25.0){
                    int multipl;
                    multipl=(int)Math.rint((ext0)/25.d);
                   //System.out.println("ms-debug (CalculateExpTime_Line) multiple of acquisitions (2): "+multipl);
                    ext0=25.0;
                    inval[4]=acc0*multipl;
                    inval[5]=(int)Math.sqrt(Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText())*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(5))/0.75d)*inval[9]*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(2))*2*inval[4]);
                }
                inval[1]=(Math.rint(ext0*100))/100.d;
               //System.out.println("ms-debug (CalculateExpTime_Line) (old expTime="+expTime0+"):\nnew exposure time = "+inval[1]+"\nacquisitions = "+inval[4]+"\nSNR = "+inval[5]);
                /*if (inval[1]<0.01){
                    inval[1]=0.01;
                    inval[5]=(int)Math.rint(Math.sqrt(Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText())*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(5))/0.75d)*inval[9]*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(2))*inval[1]*2.d*inval[4]/25.d));
                    //recalcluate snr with minimum exposure time ext0=0.01
                }*/
                if(inval[1]<expTime0){
                    inval[1]=expTime0;
                    inval[5]=(int)Math.rint(Math.sqrt(Double.parseDouble(VTF_IPC_GUI.LevelOfLight.getText())*(Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(5))/0.75d)*inval[9]*Double.parseDouble(VTF_IPC_GUI.list0.get(wl_counter1).get(2))*inval[1]*2.d*inval[4]/25.d));
                    //recalcluate snr with set exposure time expTime0
                }
                
            }else{
                
            }
        }    
        return inval;
    }

}

