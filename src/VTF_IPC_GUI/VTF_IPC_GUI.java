/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VTF_IPC_GUI;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader; 
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import javax.swing.AbstractButton;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import org.jfree.chart.JFreeChart;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Robot;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.OutputStream;
import java.util.Arrays;
import javax.swing.JTextPane;


/**
 *
 * @author schubert
 */
public class VTF_IPC_GUI extends javax.swing.JFrame{
    /* variable declaration */
    double wavelength; //central wavelength
    int nr_line=1;  //which line for different filters is selected, default 1
    //String obsmode, binmode;  //variables for results of choosen observing mode and camera binning
    String obsmode = "";
    String binmode ="";
    String scanmode = "";
    String version_id="3.4";
    String frame_title="VTF Instrument Performance Calculator v"+version_id;
    //array to readout input file
    public static ArrayList<ArrayList<String>> list0;
    ArrayList<ArrayList<String>> linefile0= new ArrayList<>();
    ArrayList<ArrayList<String>> linefiledefault= new ArrayList<>();
    int wl_counter1,wl_counter2,wl_counter3,wl_counter4;  // index for wavelength for each Line X
    int line_counter;// counter for defined filters
    int numb_cam;//number of used cameras: PI->3 cams(2NB+1BB) - look in OCD - is set in obsmodeActionListener
    String foldername;//variable to store path to config files
    String image_path;//path variable for logo
    String icon_path; // path variable for icon
    protected static String OpSys;//variable to store the name of operating system
    double centralwave;//wave length
    double texp;//exposure time
    double nlambda;//number of scan steps
    double nacquisitions;//number acquisitions
    double nstokes;//number Stokes vectors -> I,Q,U,V 
    double stepwidth;//spectral step width
    double binning;//number of binned pixels
    double SNRval;//signal to noise ratio
    double pfFWHM;//pre filter fwhm
    double tFPI,te,ts;//tFPI:timing FPI - tes: exposure time - ts:time to change modulator state
    double diskSpace;//disk spoace to store files
    int obsbinmodeset = 0; //number to define if obs or bin mode were set -> 0 is set
    //info, if custom code is set -> customLxx=1 -- not set =0
    //public static int [] customD,customB,customA;
    public static int [] udscansteps;
    protected static String [] udscansteps_files;
    protected static int udscansteps_size=0;
    int tabindex;//index which tab is selected
    protected static int pfcavity_nr=2; //number of prefilter cavities
    public static int maxLines=10;//number of maximum number of possible lines
    public static int input_nr_max=20;//maximum input parameters
    String folder_image;
    ImageIcon image;
    String [] wavetable;//central wave length selection table
    protected static String [] udss_output;//output of Editor for user defined scan steps
    //Color myyellow0 = new Color(222,255,209);
    public static Color myyellow = new Color(255,255,204);
    public static Color myblue = new Color(128,203,206);
    public static Color mydarkgreen = new Color(176,231,191);
    public static Color mygray = new Color(229,229,229);
    public static Color myorange = new Color(255,204,153);
    
    public static double[] dataRates;
    
    protected String configlocation=null;
    protected static int whichLine = 0;  // first line is chosen
    JFreeChart chart;
    JPanel chart_panel;
    protected static int whichTab = 0;
    ImageIcon icon_adv = null;
    ImageIcon logo_vtf=null;
    boolean dontshow;
    int diml_input;
    static StringBuilder out2 = new StringBuilder();
    static PrintWriter printw;
    static boolean isselected = false;
    protected static boolean[] line_active;
    boolean first_2sec = false;
    protected static boolean standalone=true; //here you define wether to compile a standalone (standalone==true) or developing version
    //String keyword_standalone="";
    double Delta_t = 0.0;
    //PrintWriter out2;
    protected static String[] filelist;//list of the config readin files
    URL res0; //location of input files (included in jar file)
    protected static int nrAvailableFilters = 4;
    //for calculating disk space and timing
    protected static double[] time;
    protected static double[] dspace;
    
    boolean readConfig=false; //dismiss question "are you advanced" if configs were red in
    
    TabBody tab0;
    TabBody tab1;
    TabBody tab2;
    CalculateTiming_Line calci;
    CalculateTiming_Tab calcTimeT;
    VTF_IPC_GUI mainInst;
    JPanel tab_base;
    JPanel tab_default;
    JPanel tab_adv;
    JScrollPane scrollT;
    CalculateSNR_Line calcSNR;
    PlotGraph_Line plot;
    ReadList myReader;
    
    /**
     * Creates new form VTF_IPC_GUI
     */
    public VTF_IPC_GUI() {
        time=new double[maxLines];
        dspace=new double[maxLines];
        dataRates=new double[maxLines];
        line_active = new boolean[10];
        list0= new ArrayList<>();
        out2.append("Report");
        out2.append(System.getProperty("line.separator"));
        out2.append(System.getProperty("line.separator"));
        String Debugs_name = new SimpleDateFormat("'Report_'yyyy-MM-dd_HH-mm-ss'.txt'").format(new Date());
        plot = new PlotGraph_Line();
        calcTimeT = new CalculateTiming_Tab();
        calcSNR = new CalculateSNR_Line();
        calci = new CalculateTiming_Line();
        myReader=new ReadList();
        
        Locale.setDefault(Locale.ENGLISH);
        this.line_counter = 0;
        binning=1;//default setting
        udscansteps=new int[maxLines];
        udscansteps_files=new String[maxLines];

        initComponents();
        setTitle(frame_title);
        ProjectName.setBackground(myblue);
        ProjectName.setEditable(false);
        addTabListeners();
        
        //set SI, binning 1x1 and blue to red scans as default
        //attention: this is called before the action listener were implemented 
        Polar.doClick();
        OpSys = ManagementFactory.getOperatingSystemMXBean().getName();
        out2.append("Operating//System: " + OpSys+ "\n");
        out2.append(System.getProperty("line.separator"));
        
        File pfad = new File("");
        String directory;
       
        if(standalone){
            directory="";
        }else{
            directory = pfad.getAbsolutePath();
            //System.out.println("dir: "+directory);
        }    
        
        if (!OpSys.startsWith("Windows") || standalone) {

            foldername=directory.concat("/config_input/");
            image_path=foldername+"VTF_logo.png";
            icon_path = foldername + "icon.png";
        } else {
            foldername=directory.concat("\\config_input\\");
            image_path=foldername+"VTF_logo.png";
            icon_path = foldername + "icon.png";
        }
        this.readList(foldername, "input02.txt", list0,0);
        //readList(foldername, "input02.txt", list0);
        //System.out.println("readList foldername = "+foldername);
        //System.out.println("input02.txt read in - size: "+list0.size());
        filelist=new String[list0.size()];
        for (int i=0;i<(list0.size());i++){
            filelist[i]=list0.get(i).get(7);
        }
        
        
        if (standalone){
            try {
                Image img0 = ImageIO.read(getClass().getResource(icon_path));
                icon_adv=new ImageIcon(img0);
                Image img1 = ImageIO.read(getClass().getResource(image_path));
                logo_vtf = new ImageIcon(img1);            
            } catch (IOException ex) {
                //System.out.println(ex.getMessage());
                Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            icon_adv = new ImageIcon(icon_path);
            logo_vtf = new ImageIcon(image_path);
        }
        jLabel1.setIcon(logo_vtf); // NOI18N
        jLabel1.setText("logo");

        if (!OpSys.startsWith("Windows") || standalone) {
            foldername=directory.concat("/config_input/wavelength_intensity_of_FTS_atlas/");
            //DebugInfos.append("Path variables with Linux/MacOS syntax!!!!\n");
        } else {
            foldername=directory.concat("\\config_input\\wavelength_intensity_of_FTS_atlas\\");
            //DebugInfos.append("Path variables with Windows syntax!!!!\n");
        }
                
        this.readList(foldername,"wavelength_intensity_6302.50.txt", linefiledefault,0);
        
        double[][] values_indef=new double [linefiledefault.size()][2];
        
        for (int i=0;i<linefiledefault.size();i++){
            values_indef[i][0]= Double.parseDouble(linefiledefault.get(i).get(0));
            values_indef[i][1]= Double.parseDouble(linefiledefault.get(i).get(1));
        }
        
        configMain.setName("configMain");
        Default_Tab.setName("default");
        Basic_Tab.setName("basic");
        Advanced_Tab.setName("advanced");
        
        //string variable for wavelength selection
        diml_input=list0.size();
        nrAvailableFilters=diml_input;
        //System.out.println("size of list0: "+diml_input);
        wavetable=new String[diml_input+2];
             
        Polar.setActionCommand("SI");
        Doppler.setActionCommand("DI");
        Intensity.setActionCommand("MI");
        
        
        out2.append("Default settings:\n VTF Mode: SI\n Camera binning: 1x1\n Scan Mode: blue -> red\n");
        out2.append(System.getProperty("line.separator"));
        wavetable[diml_input+1]="Custom";
                        
        //System.out.println("debug ms VTF_IPC_GUI before setting tab0");
        tab0 = new TabBody(0);
        tab_default = tab0.getPanel();
        tab_default.setSize(540,510);
        Default_Tab.add(tab_default);
        
        //System.out.println("debug --- mainGui initial time and space:\ntime= "+Arrays.toString(time)+"\ndiskVol="+Arrays.toString(dspace));
        //System.out.println("debug ms VTF_IPC_GUI before setting tab1");
        tab1 = new TabBody(1);
        tab_base = tab1.getPanel();
        tab_base.setSize(540,510);
        Basic_Tab.add(tab_base);
        //System.out.println("debug ms VTF_IPC_GUI before setting tab2");
        tab2 = new TabBody(2);
        tab_adv = tab2.getPanel();
        tab_adv.setSize(540,510);
        Advanced_Tab.add(tab_adv);
        whichLine=0;
        double [] input={630.25,25.0,-5,3.15,8,667,Double.parseDouble(list0.get(1).get(4)),0.6,0,1,0,12.34,0,0,0,2,0,5,11};
        double result_time0 = calci.CalculateTiming_Line(input, 0)[0];
        input[11]=result_time0;
        tab0.setLine(0, input);
       //System.out.println("ms-debug (VTF_IPC_GUI) ---- INITIALIZATION DONE!!!");
        
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ObsModeGroup = new javax.swing.ButtonGroup();
        BinningModeGroup = new javax.swing.ButtonGroup();
        ScanModeD = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        configMain = new javax.swing.JTabbedPane();
        Default_Tab = new javax.swing.JPanel();
        Basic_Tab = new javax.swing.JPanel();
        Advanced_Tab = new javax.swing.JPanel();
        graphicWindow = new javax.swing.JPanel();
        ObsModePanelD = new javax.swing.JPanel();
        Polar = new javax.swing.JRadioButton();
        Intensity = new javax.swing.JRadioButton();
        Doppler = new javax.swing.JRadioButton();
        LevelOfLight_Slider = new javax.swing.JSlider();
        jLabel21 = new javax.swing.JLabel();
        LevelOfLight = new javax.swing.JTextField();
        repeatsNumb_text = new javax.swing.JTextField();
        delayObs_text = new javax.swing.JTextField();
        durationObs_text = new javax.swing.JTextField();
        duartion_label = new javax.swing.JLabel();
        repeatsNumb_label = new javax.swing.JLabel();
        delayObs_label = new javax.swing.JLabel();
        DiskSpaceText = new javax.swing.JTextField();
        diskSpaceLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        ProjectName = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        dataRateDHS_text = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        MenuItem_OpenConfig = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        MenuItem_SaveConfig = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        ResetGUI_menu = new javax.swing.JMenuItem();
        Help = new javax.swing.JMenu();
        writeDebug_checkbox = new javax.swing.JCheckBoxMenuItem();
        ShowHelp = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("VTF Instrument Performance Calculator v2.1");
        setSize(new java.awt.Dimension(0, 0));

        jScrollPane2.setPreferredSize(new java.awt.Dimension(1100, 730));
        jScrollPane2.setRequestFocusEnabled(false);

        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 670));

        configMain.setFocusable(false);
        configMain.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                configMainStateChanged(evt);
            }
        });

        Default_Tab.setAutoscrolls(true);
        Default_Tab.setPreferredSize(new java.awt.Dimension(517, 471));

        javax.swing.GroupLayout Default_TabLayout = new javax.swing.GroupLayout(Default_Tab);
        Default_Tab.setLayout(Default_TabLayout);
        Default_TabLayout.setHorizontalGroup(
            Default_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 518, Short.MAX_VALUE)
        );
        Default_TabLayout.setVerticalGroup(
            Default_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 520, Short.MAX_VALUE)
        );

        configMain.addTab("DEFAULT", Default_Tab);

        Basic_Tab.setAutoscrolls(true);
        Basic_Tab.setPreferredSize(new java.awt.Dimension(517, 471));

        javax.swing.GroupLayout Basic_TabLayout = new javax.swing.GroupLayout(Basic_Tab);
        Basic_Tab.setLayout(Basic_TabLayout);
        Basic_TabLayout.setHorizontalGroup(
            Basic_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 518, Short.MAX_VALUE)
        );
        Basic_TabLayout.setVerticalGroup(
            Basic_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 520, Short.MAX_VALUE)
        );

        configMain.addTab("BASIC", Basic_Tab);

        Advanced_Tab.setAutoscrolls(true);
        Advanced_Tab.setPreferredSize(new java.awt.Dimension(517, 471));

        javax.swing.GroupLayout Advanced_TabLayout = new javax.swing.GroupLayout(Advanced_Tab);
        Advanced_Tab.setLayout(Advanced_TabLayout);
        Advanced_TabLayout.setHorizontalGroup(
            Advanced_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 518, Short.MAX_VALUE)
        );
        Advanced_TabLayout.setVerticalGroup(
            Advanced_TabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 520, Short.MAX_VALUE)
        );

        configMain.addTab("ADVANCED", Advanced_Tab);

        graphicWindow.setPreferredSize(new java.awt.Dimension(417, 417));

        javax.swing.GroupLayout graphicWindowLayout = new javax.swing.GroupLayout(graphicWindow);
        graphicWindow.setLayout(graphicWindowLayout);
        graphicWindowLayout.setHorizontalGroup(
            graphicWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        graphicWindowLayout.setVerticalGroup(
            graphicWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 417, Short.MAX_VALUE)
        );

        ObsModeGroup.add(Polar);
        Polar.setText("SI");

        ObsModeGroup.add(Intensity);
        Intensity.setText("MI");

        ObsModeGroup.add(Doppler);
        Doppler.setText("DI");

        LevelOfLight_Slider.setValue(100);
        LevelOfLight_Slider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                LevelOfLight_SliderStateChanged(evt);
            }
        });

        jLabel21.setText("Level of Light");

        LevelOfLight.setEditable(false);
        LevelOfLight.setText("1.0");
        LevelOfLight.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 1));
        LevelOfLight.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });

        javax.swing.GroupLayout ObsModePanelDLayout = new javax.swing.GroupLayout(ObsModePanelD);
        ObsModePanelD.setLayout(ObsModePanelDLayout);
        ObsModePanelDLayout.setHorizontalGroup(
            ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObsModePanelDLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Polar)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ObsModePanelDLayout.createSequentialGroup()
                        .addComponent(LevelOfLight_Slider, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LevelOfLight, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ObsModePanelDLayout.createSequentialGroup()
                        .addComponent(Doppler)
                        .addGap(40, 40, 40)
                        .addComponent(Intensity)))
                .addGap(8, 8, 8))
        );
        ObsModePanelDLayout.setVerticalGroup(
            ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObsModePanelDLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Polar)
                    .addComponent(Doppler)
                    .addComponent(Intensity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ObsModePanelDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LevelOfLight_Slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(LevelOfLight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        repeatsNumb_text.setBackground(new java.awt.Color(255, 255, 204));
        repeatsNumb_text.setText("1");
        repeatsNumb_text.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                repeatsNumb_textFocusLost(evt);
            }
        });
        repeatsNumb_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repeatsNumb_textActionPerformed(evt);
            }
        });
        repeatsNumb_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });

        delayObs_text.setBackground(new java.awt.Color(255, 255, 204));
        delayObs_text.setText("0.0");
        delayObs_text.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                delayObs_textFocusLost(evt);
            }
        });
        delayObs_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delayObs_textActionPerformed(evt);
            }
        });
        delayObs_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Default_KeyTyped(evt);
            }
        });

        durationObs_text.setEditable(false);
        durationObs_text.setBackground(new java.awt.Color(128, 203, 206));

        duartion_label.setText("total run time");

        repeatsNumb_label.setText("No of repeats");

        delayObs_label.setText("Delay betw. repeats (s)");

        DiskSpaceText.setEditable(false);
        DiskSpaceText.setBackground(new java.awt.Color(128, 203, 206));

        diskSpaceLabel.setText("Data Volume");

        jLabel1.setText("logo");
        jLabel1.setDoubleBuffered(true);

        ProjectName.setBackground(new java.awt.Color(240, 240, 240));

        jLabel15.setText("Config File Name");

        jLabel11.setFont(new java.awt.Font("Cantarell", 0, 12)); // NOI18N
        jLabel11.setText("(per camera)");

        dataRateDHS_text.setEditable(false);
        dataRateDHS_text.setBackground(new java.awt.Color(128, 203, 206));
        dataRateDHS_text.setText("0.0");
        dataRateDHS_text.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        dataRateDHS_text.setMinimumSize(new java.awt.Dimension(70, 26));
        dataRateDHS_text.setPreferredSize(new java.awt.Dimension(70, 26));
        dataRateDHS_text.setRequestFocusEnabled(false);

        jLabel2.setText("DHS data rate (GB/s)");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(559, 559, 559)
                .addComponent(repeatsNumb_label, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(delayObs_text, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(repeatsNumb_text, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(duartion_label)
                    .addComponent(diskSpaceLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(durationObs_text)
                    .addComponent(DiskSpaceText))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(configMain, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ProjectName, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ObsModePanelD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dataRateDHS_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(graphicWindow, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                                    .addContainerGap())
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGap(0, 266, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(77, 77, 77)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addContainerGap())))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(delayObs_label)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {delayObs_text, repeatsNumb_text});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ProjectName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ObsModePanelD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(dataRateDHS_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))))
                        .addGap(20, 20, 20)
                        .addComponent(configMain, javax.swing.GroupLayout.PREFERRED_SIZE, 566, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(graphicWindow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(durationObs_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(duartion_label)
                            .addComponent(repeatsNumb_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(repeatsNumb_label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DiskSpaceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(diskSpaceLabel)
                            .addComponent(delayObs_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(delayObs_label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {DiskSpaceText, delayObs_text, durationObs_text, repeatsNumb_text});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {delayObs_label, diskSpaceLabel, duartion_label, repeatsNumb_label});

        configMain.getAccessibleContext().setAccessibleName("Help");

        jScrollPane2.setViewportView(jPanel1);

        jMenu1.setText("File");

        jMenu3.setText("Open ...");

        MenuItem_OpenConfig.setText("Open Config File");
        MenuItem_OpenConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readConfigButtonActionPerformed(evt);
            }
        });
        jMenu3.add(MenuItem_OpenConfig);

        jMenu1.add(jMenu3);

        jMenu4.setText("Save ...");

        MenuItem_SaveConfig.setText("Save Config File");
        MenuItem_SaveConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItem_SaveConfigActionPerformed(evt);
            }
        });
        jMenu4.add(MenuItem_SaveConfig);

        jMenu1.add(jMenu4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        ResetGUI_menu.setText("Reset GUI");
        ResetGUI_menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetGUI_menuActionPerformed(evt);
            }
        });
        jMenu2.add(ResetGUI_menu);

        jMenuBar1.add(jMenu2);

        Help.setText("Help");
        Help.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                HelpMouseClicked(evt);
            }
        });

        writeDebug_checkbox.setText("Write Debug Infos");
        writeDebug_checkbox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                writeDebug_checkboxStateChanged(evt);
            }
        });
        Help.add(writeDebug_checkbox);

        ShowHelp.setText("Show Help");
        ShowHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShowHelpActionPerformed(evt);
            }
        });
        Help.add(ShowHelp);

        jMenuBar1.add(Help);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1014, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 684, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void Default_KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Default_KeyTyped
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_PERIOD)) {
            evt.consume();
        }
    }//GEN-LAST:event_Default_KeyTyped

    private void readConfigButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readConfigButtonActionPerformed
        readConfig=true;
        double [] input;
        input = new double[input_nr_max];
        boolean loadSteps=true;
        JFileChooser filechooser = new JFileChooser();
        if (null!=configlocation)
            filechooser.setCurrentDirectory(new File(configlocation));
        
        int check=filechooser.showOpenDialog(null);
        if(check==JFileChooser.APPROVE_OPTION){
            //System.out.println("debug --- read in config prop file: "+filechooser.getSelectedFile().getAbsolutePath());
            File file = filechooser.getSelectedFile();
            String filename = file.getAbsolutePath();
            configlocation=file.getParentFile().getAbsolutePath();
            String linename = "";
            String binningname = "1x1";
            String roiname="4128x4104";
            String DoubleSteps;
            int linenumber = 0;
            int tabnumber = 0;
            String wavel_number2 = "";
            int binningindex = 0;
            int binningfac = 1;
            int doubleStepsIndex = 0;
            int ROIindex = 0;
            double expTime=25.0;  //default exposure time
            //set deafults
            input[1]=expTime;
        
            Properties prop = new Properties();
            InputStream input0 = null;
        
            if (!filename.contains("Config.properties")) {
                JOptionPane.showMessageDialog(null, "Choose proper Config.properties txt-file");
                MenuItem_OpenConfig.doClick();
            } else {
            
                //ProjectName.setText(filename);
                    
                try {
                    input0 = new FileInputStream(filename);
                    //load a properties file
                    prop.load(input0);
                    if(!prop.containsKey("IPC_version")){
                        JOptionPane.showMessageDialog(null, "Config File doesn't fit actual IPC version v"+version_id);
                        MenuItem_OpenConfig.doClick();
                    }else{
                        double vers_act=Double.parseDouble(prop.getProperty("IPC_version"));
                        if(vers_act<2.3){
                            JOptionPane.showMessageDialog(null, "Config File doesn't fit actual IPC version v"+version_id);
                            MenuItem_OpenConfig.doClick();
                        }else{
                            tab0.resetTab();
                            tab1.resetTab();
                            tab2.resetTab();
                            ProjectName.setText(filename);
                            if (prop.getProperty("instrMode").equals("SI")) Polar.setSelected(true);
                            if (prop.getProperty("instrMode").equals("DI")) Doppler.setSelected(true);
                            if (prop.getProperty("instrMode").equals("MI")) Intensity.setSelected(true);
                            Double lol = (Double.valueOf(prop.getProperty("levelOfLight"))*100);
                            LevelOfLight_Slider.setValue(lol.intValue());
                            repeatsNumb_text.setText((prop.getProperty("numberOfRepeats")));
                            configMain.revalidate();
                            if(prop.getProperty("repeatsDelay").isEmpty())
                                delayObs_text.setText("0.0");
                            else
                                delayObs_text.setText(prop.getProperty("repeatsDelay"));
                
                            if (prop.getProperty("modus").equals("Default")) {
                                tabindex = 0;
                                configMain.setSelectedComponent(Default_Tab);
                            }
                            if (prop.getProperty("modus").equals("Basic")) {
                                tabindex = 1;
                                configMain.setSelectedComponent(Basic_Tab);
                            }
                            if (prop.getProperty("modus").equals("Advanced")) {
                                tabindex = 2;
                                configMain.setSelectedComponent(Advanced_Tab);
                            }
                            //int numLines=Integer.parseInt(prop.getProperty("noLines"));
                            for (int j=maxLines-1; j>-1; j--) {
                                //System.out.println("ms debug readConfig - J = "+j);
                                linename = prop.getProperty("PFname" + j);
                                wavel_number2 = prop.getProperty("wavelength" + j);
                                
                                if (!(wavel_number2 == null)) {  // read in the saved lines

                                    linenumber = j;
                                    input[0] = Double.parseDouble(wavel_number2);

                                    binningname = prop.getProperty("binning" + j);
                                    /*if (binningname.equals("1x1")) binningindex = 0;
                                    if (binningname.equals("2x2")) binningindex = 1;
                                    if (binningname.equals("4x4")) binningindex = 2;*/
                                    switch(binningname){
                                        case "1x1":
                                            binningindex=0;
                                            binningfac=1;
                                            break;
                                        case "2x2":
                                            binningindex=1;
                                            binningfac=4;
                                            break;
                                        case "4x4":
                                            binningindex=2;
                                            binningfac=16;
                                            break;
                                    }
                                    if (prop.getProperty("ROI" + j).equals("4128x4104")) ROIindex = 0;
                                    if (prop.getProperty("ROI" + j).equals("2048x2048")) ROIindex = 1;
                                    if (prop.getProperty("ROI" + j).equals("1024x1024")) ROIindex = 2;
                                    roiname=prop.getProperty("ROI" + j);
                        
                                    input[8]=binningindex;
                                    input[9]=binningfac;
                                    input[12]=ROIindex;
                        
                        
                                    String scanmodename = prop.getProperty("scanMode" + j);
                                    if (scanmodename.equals("red-blue")) input[14]=0 ;
                                    if (scanmodename.equals("nested")) input[14]=1 ;
                                    input[1] = Double.parseDouble(prop.getProperty("expTime" + j));
                                    expTime=Double.parseDouble(prop.getProperty("expTime" + j));
                                    whichLine=j;
                                    if(prop.getProperty("userdefScanSteps"+j).equals("no user steps")){
                                        input[2] = Double.parseDouble(prop.getProperty("stepsLeft" + j));
                                        input[17] = Double.parseDouble(prop.getProperty("stepsRight" + j));
                                        input[18] = Double.parseDouble(prop.getProperty("noScanSteps" + j));
                                       //System.out.println("msdebug (VTF_IPC_GUI) ------ read in scan steps left/right!");
                                        loadSteps=true;
                                    }else{
                                       //System.out.println("ms-debug (VTF_IPC_GUI) ----- readConfig -> user defined scan steps!!");
                                        udscansteps[j]=1;
                                        
                                        udscansteps_files[j]=prop.getProperty("userdefScanSteps"+j);
                                        File datin=new File(udscansteps_files[j]);
                                        if(datin.exists()){
                                            ArrayList<ArrayList<String>> inputarray;
                                            String directory=udscansteps_files[j].substring(0, udscansteps_files[j].lastIndexOf(File.separator)+1);
                                            String name=udscansteps_files[j].substring(udscansteps_files[j].lastIndexOf(File.separator)+1, udscansteps_files[j].length());
                                            inputarray=myReader.ReadList(directory,name,1);
                                            udss_output=new String[inputarray.size()];
                                            for (int m=0;m<inputarray.size();m++)
                                                udss_output[m]=inputarray.get(m).toString().replaceAll("\\[|\\]", "")+"\n";
                                            input[2]=0;
                                            input[17]=0;
                                            input[18]=udss_output.length;
                                            loadSteps=true;
                                            /*results_calc=calci.CalculateTiming_Line(input);
                                           //System.out.println("debug --- read in user def, calculate timing="+results_calc[0]+" and disk vol="+results_calc[1]);
                                            input[11]=results_calc[0];
                                            time[j]=results_calc[0];
                                            dspace[j]=results_calc[1];*/
                                        }else{
                                            //System.out.println("debug --- couldn't read user defined scan steps file");
                                            JOptionPane.showMessageDialog(null, "Please open userdefined scan steps for line "+Integer.toString(j+1)+"\n -- choose file --","Warning",JOptionPane.WARNING_MESSAGE);
                                            File pfad = new File("");
                                            String startdir;
                                            startdir = pfad.getAbsolutePath();
                                            //System.out.println("Start directory for file chooser is: "+startdir);
                                    
                                            JFileChooser udfile = new JFileChooser(startdir);
                                            udfile.setDialogTitle("Please open userdefined scan steps for line "+Integer.toString(j+1)+" -- choose file --");
                                            int udcheck=udfile.showOpenDialog(null);
                                            if (udcheck==JFileChooser.APPROVE_OPTION){
                                                ArrayList<ArrayList<String>> inputarray;
                                                File ufile = udfile.getSelectedFile();
                                                filename = ufile.getAbsolutePath();
                                                udscansteps_files[j]=filename;
                                                String directory=filename.substring(0,filename.lastIndexOf(File.separator)+1);
                                                String name=filename.substring(filename.lastIndexOf(File.separator)+1, filename.length());
                                                //System.out.println("debug --- read in config file name="+name+" (directory="+directory+")");
                                                inputarray=myReader.ReadList(directory,name,1);
                                                udss_output=new String[inputarray.size()];
                                                for (int m=0;m<inputarray.size();m++)
                                                    udss_output[m]=inputarray.get(m).toString().replaceAll("\\[|\\]", "")+"\n";
                                                input[2]=0;
                                                input[17]=0;
                                                input[18]=udss_output.length;
                                                loadSteps=true;
                                            }else{
                                                udscansteps[j]=0;
                                                udscansteps_files[j]="";
                                                loadSteps=false;
                                                prop.setProperty("userdefScanSteps"+j, "no user steps");
                                            }
                                        }
                                    }
                            
                                    if (linename.equals("CustomMode")) {
                                        //System.out.println("read in config props for customMode!!!");
                                        input[3] = Double.parseDouble(prop.getProperty("spectralStep" + j));
                                        input[15]=nrAvailableFilters+1;
                            
                                    } else {
                                        int wl_index = get_wavelength_nr(wavel_number2);
                                        //input[3] = (Double.parseDouble(list0.get(wl_index).get(1)))/2d;
                                        
                                        DoubleSteps = prop.getProperty("doubleSteps" + j);
                                        int stepfac=0;
                                        switch(DoubleSteps){
                                            case "x1":
                                                stepfac=0;
                                                break;
                                            case "x2":
                                                stepfac=1;
                                                break;
                                            case "x3":
                                                stepfac=2;
                                                break;
                                        }
                                        //input[3]=(stepfac+1)*input[3];
                                        input[3]=Double.parseDouble(prop.getProperty("spectralStep"+j));
                                        input[16]=stepfac;
                                        input[15]=wl_index+1;
                                    }
                                    input[4] = Double.parseDouble(prop.getProperty("noAcquisitions" + j));  //acquisitions
                                    input[5] = Double.parseDouble(prop.getProperty("SNR" + j));  //SNR
                                    //String obs_mode = ObsModeGroup.getSelection( ).getActionCommand( );
                                    if (Intensity.isSelected()) {
                                        input[13]=Double.parseDouble(prop.getProperty("scanPositionInNM" + j));
                                    }
                                    input[10] = Double.parseDouble(prop.getProperty("iterations" + j));  //Repeats
                                    /*if (prop.getProperty("doubleSteps" + j).equals("x1")) doubleStepsIndex = 0;
                                    if (prop.getProperty("doubleSteps" + j).equals("x2")) doubleStepsIndex = 1;
                                    if (prop.getProperty("doubleSteps" + j).equals("x3")) doubleStepsIndex = 2;
                                    input[16]=doubleStepsIndex;*/
                                    //if (!linename.equals("CustomMode")){
                                    //}
                                    double[] results_calc=calci.CalculateTiming_Line(input,j);
                                    //System.out.println("debug --- read in config props line "+j+", binning="+input[9]+": timing="+results_calc[0]+" diskVol="+results_calc[1]);
                                    input[11]=results_calc[0];
                                    //System.out.println("ms_debug VTF_IPC_GUI.readConfigButtonAct... input = \n"+Arrays.toString(input));
                                    switch(tabindex){
                                        case 0:
                                            tab0.setLine(j, input);
                                            break;
                                        case 1:
                                            
                                            tab1.setLine(j, input);
                                            break;
                                        case 2:
                                            if(loadSteps){
                                                tab2.setUserSteps(j, prop.getProperty("userdefScanSteps"+j, "no user steps"));
                                                tab2.setLine(j, input);
                                            }
                                            break;
                                    }
                        
                                    VTF_IPC_GUI.time[j]=results_calc[0];
                                    VTF_IPC_GUI.dspace[j]=results_calc[1];
                                    //System.out.println("debug --- read in props controll: time="+VTF_IPC_GUI.time[j]+"  dspace="+VTF_IPC_GUI.dspace[j]);
                                    double[] resultTab = calcTimeT.CalculateTiming_Tab(VTF_IPC_GUI.time, VTF_IPC_GUI.dspace, Integer.parseInt(VTF_IPC_GUI.repeatsNumb_text.getText()), Double.parseDouble(VTF_IPC_GUI.delayObs_text.getText()));
                                    int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
                                    String roundRes=Double.toString(((double)roundval)/100.d);
                                    double nachkomma=(resultTab[0]*100.d)%100;
                                    durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
                                    DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
                        
                                } else {
                                    
                                }
                            }
                           //System.out.println("ms-debug (VTF_IPC_GUI) ---- readConfig -> set common line parameter (no line selected):\n    BIN:"+binningname+"\n    ROI:"+roiname+"\n    expTime:"+expTime);
                            ActionEvent fireEnter=new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "expChanged");
                            ActionListener[] alist=null;
                            switch(tabindex){
                                case 0:
                                    //whichLine=0;
                                    tab0.line1.setExpTime(Double.toString(expTime));
                                    
                                    alist=tab0.line1.expTime.getListeners(ActionListener.class);
                                    alist[0].actionPerformed(fireEnter);
        
                                    tab0.line1.setROI(roiname);
                                    tab0.line1.setBinning(binningname);
                                    break;
                                case 1:
                                    //whichLine=0;
                                    tab1.line1.setExpTime(Double.toString(expTime));
                                    
                                    alist=tab1.line1.expTime.getListeners(ActionListener.class);
                                   //System.out.println("ms-debug (VTF_IPC_GUI) ----- readConfig actionLister: "+Arrays.toString(alist));
                                    alist[0].actionPerformed(fireEnter);
                                    tab1.line1.setROI(roiname);
                                    tab1.line1.setBinning(binningname);
                                    break;
                                case 2:
                                    //whichLine=0;
                                    tab2.line1.setExpTime(Double.toString(expTime));
                                    alist=tab2.line1.getListeners(ActionListener.class);
                                    if (alist.length>0){
                                        alist[0].actionPerformed(fireEnter);
                                        tab2.line1.setROI(roiname);
                                        tab2.line1.setBinning(binningname);
                                    }
                                    break;
                                default:
                                   //System.out.println("ERROR: no tabindex set!!!!");
                                    break;
                            }
                        }
                    }
                
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_readConfigButtonActionPerformed
    
    
    private void ResetGUI_menuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetGUI_menuActionPerformed
        this.dispose();
        VTF_IPC_GUI gui = new VTF_IPC_GUI();
        gui.setVisible(true);
    }//GEN-LAST:event_ResetGUI_menuActionPerformed

    private void configMainStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_configMainStateChanged
        double [] inval,inval_0;
        int jump, counter;
        for (int i = 0; i<=9; i++) line_active[i] = false;
        int wc=0;
        jump=0;
        counter=0;
        inval=new double[input_nr_max];
        inval_0=new double[input_nr_max];
        double expTime=25.0;
        Object binningname="1x1", roiname="4128x4104";
        tabindex=configMain.getSelectedIndex(); 
        whichTab=tabindex;
        
        switch (tabindex) {
            case 2:
                whichTab = 2;
                if (!readConfig){
                    JCheckBox checkbox = new JCheckBox("don't ask me again");
                    
                    Object[] options = {"Of course", "Not sure", checkbox};
                    if (dontshow == false) {
                        int n = JOptionPane.showOptionDialog(null, "Are you sure you have enough experience?", "ADVANCED", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon_adv, options, options[0]);
                        dontshow = checkbox.isSelected();
                        if (n == 1) {               
                            configMain.setSelectedIndex(1);
                        }
                    }
                    readConfig=false;//reset boolean to default
                }
                break;
            case 1:
                whichTab = 1;
                break;
            case 0:
                whichTab = 0;
                break;
            default:
                break;
        }
        
        double[] results_calc;
        //System.out.println("debug ms VTF_IPC_GUI.configMainStateChange: tabidx = "+tabindex);
        //for (int i=0;i<maxLines;i++){
        for (int i=maxLines-1;i>-1;i--){
            if (tabindex==0){
            }else{
                if ((tabindex-1)==0)
                    inval=tab0.getLine(i);
                else
                    inval=tab1.getLine(i);
                if (inval[0]==0 && tabindex==2){
                       inval=tab0.getLine(i);// if basic tab is not set try to read from default tab
                       jump=1;
                }
                
                switch(tabindex){
                    case 1:
                        inval_0=tab1.getLine(i);
                        results_calc=calci.CalculateTiming_Line(inval_0,i);
                        inval_0[11]=results_calc[0];
                        VTF_IPC_GUI.time[i]=results_calc[0];
                        VTF_IPC_GUI.dspace[i]=results_calc[1];
                        counter++;
                        break;
                    case 2:
                        inval_0=tab2.getLine(i);
                        results_calc=calci.CalculateTiming_Line(inval_0,i);
                        inval_0[11]=results_calc[0];
                        VTF_IPC_GUI.time[i]=results_calc[0];
                        VTF_IPC_GUI.dspace[i]=results_calc[1];
                        counter++;
                        break;
                }
                
                if (inval[15]>0 && inval_0[15]<1){ //only copy line of old tab if the line in the selected tab is not set 
                    expTime=inval[1];
                    switch((int)inval[8]){
                        case 0:
                            binningname="1x1";
                            break;
                        case 1:
                            binningname="2x2";
                            break;
                        case 2:
                            binningname="4x4";
                            break;
                    }
                    switch((int)inval[12]){
                        case 0:
                            roiname="4128x4104";
                            break;
                        case 1:
                            roiname="2048x2048";
                            break;
                        case 2:
                            roiname="1024x1024";
                            break;
                    }
                   //System.out.println("ms-debug (VTF_IPC_GUI) ---- configMainStateChanged read common params:\n expTime="+expTime+"\n roiname="+roiname+"\n binningname="+binningname);
                    results_calc=calci.CalculateTiming_Line(inval,i);
                    inval[11]=results_calc[0];
                    VTF_IPC_GUI.time[i]=results_calc[0];
                    VTF_IPC_GUI.dspace[i]=results_calc[1];
                    switch(tabindex){
                        case 1:
                            tab1.setLine(i, inval);
                            break;
                        case 2:
                            tab2.setLine(i, inval);
                            break;
                    }
                    counter++;
                }
            }
        }
       //System.out.println("ms-debug (VTF_IPC_GUI) ---- congMainStateChanged main components ("+configMain.getComponents().length+")"+Arrays.toString(configMain.getComponents()));
        if (configMain.getComponents().length>1 && readConfig==false) {
           //System.out.println("ms-debug (VTF_IPC_GUI) ---- ConfigMainStateChanged -> set common line parameter (no line selected):\n    BIN:"+binningname+"\n    ROI:"+roiname+"\n    expTime:"+expTime);
        
            ActionEvent fireEnter=new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "expChanged");
            ActionListener[] alist=null;
            switch(tabindex){
                case 0:
                    //whichLine=0;
                    tab0.line1.setExpTime(Double.toString(expTime));

                    alist=tab0.line1.expTime.getListeners(ActionListener.class);
                    alist[0].actionPerformed(fireEnter);

                    tab0.line1.setROI(roiname);
                    tab0.line1.setBinning(binningname);
                    break;
                case 1:
                    //whichLine=0;
                    tab1.line1.setExpTime(Double.toString(expTime));

                    alist=tab1.line1.expTime.getListeners(ActionListener.class);
                    if (alist!=null){
                       //System.out.println("ms-debug (VTF_IPC_GUI) ---- configMainStateChanged set TAB1 actionListener:"+Arrays.toString(alist));
                       alist[0].actionPerformed(fireEnter);
                       tab1.line1.setROI(roiname);
                       tab1.line1.setBinning(binningname);
                    }
                    //else
                       //System.out.println("ms-debug (VTF_IPC_GUI) ---- configMainStateChanged set TAB1 actionListener: no defined!!!!!!!");
                    
                    break;
                case 2:
                    //whichLine=0;
                    tab2.line1.setExpTime(Double.toString(expTime));
                    alist=tab2.line1.getListeners(ActionListener.class);
                    if (alist.length>0){
                       //System.out.println("ms-debug (VTF_IPC_GUI) ---- configMainStateChanged set TAB2 actionListener:"+Arrays.toString(alist));
                        alist[0].actionPerformed(fireEnter);
                        tab2.line1.setROI(roiname);
                        tab2.line1.setBinning(binningname);
                    }//else
                       //System.out.println("ms-debug (VTF_IPC_GUI) ---- configMainStateChanged set TAB2 actionListener: no defined!!!!!!!");
                    
                    break;
                default:
                   //System.out.println("ERROR: no tabindex set!!!!");
                    break;
            }
        }
        
        if (counter!=0){
            //System.out.println("debug --- mainTab state changed to Tab"+tabindex+" (line counter="+counter+") inputarray:\n time: "+Arrays.toString(time)+"\n diskVol: "+Arrays.toString(dspace));
            double[] result;
            result=calcTimeT.CalculateTiming_Tab(time, dspace, Integer.parseInt(repeatsNumb_text.getText()), Double.parseDouble(delayObs_text.getText()));
            int roundval=(int)Math.rint((result[0])*100.d+0.5);
            String roundRes=Double.toString(((double)roundval)/100.d);
            double nachkomma=(result[0]*100.d)%100;
            VTF_IPC_GUI.durationObs_text.setText((int)result[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
            //VTF_IPC_GUI.durationObs_text.setText(Double.toString(result[0]));
            VTF_IPC_GUI.DiskSpaceText.setText(Double.toString(result[1])+" GB");
        }
    }//GEN-LAST:event_configMainStateChanged
            
    private void repeatsNumb_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repeatsNumb_textActionPerformed
        // TODO add your handling code here:
        if (repeatsNumb_text.getText().isEmpty()){
        } else {
            int tabidx=configMain.getSelectedIndex();
            double[] resultTab=calcTimeT.CalculateTiming_Tab(time, dspace, Integer.parseInt(repeatsNumb_text.getText()), Double.parseDouble(delayObs_text.getText()));
            int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
            String roundRes=Double.toString(((double)roundval)/100.d);
            double nachkomma=(resultTab[0]*100.d)%100;
            durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
            DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
            
        }
    }//GEN-LAST:event_repeatsNumb_textActionPerformed

    private void delayObs_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delayObs_textActionPerformed
        // TODO add your handling code here:
        if (delayObs_text.getText().isEmpty())
            delayObs_text.setText("0.0");
        
        int tabidx=configMain.getSelectedIndex();
        double[] resultTab=calcTimeT.CalculateTiming_Tab(time, dspace, Integer.parseInt(repeatsNumb_text.getText()), Double.parseDouble(delayObs_text.getText()));
        int roundval=(int)Math.rint((resultTab[0])*100.d);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(resultTab[0]*100.d)%100;
        durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");                    
    }//GEN-LAST:event_delayObs_textActionPerformed

    private void repeatsNumb_textFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_repeatsNumb_textFocusLost
        // TODO add your handling code here:
        if (repeatsNumb_text.getText().isEmpty()){
        } else {
            int tabidx=configMain.getSelectedIndex();
            double[] resultTab=calcTimeT.CalculateTiming_Tab(time, dspace, Integer.parseInt(repeatsNumb_text.getText()), Double.parseDouble(delayObs_text.getText()));
            int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
            String roundRes=Double.toString(((double)roundval)/100.d);
            double nachkomma=(resultTab[0]*100.d)%100;
            durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
            DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
            
        }
    }//GEN-LAST:event_repeatsNumb_textFocusLost

    private void delayObs_textFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_delayObs_textFocusLost
        // TODO add your handling code here:
        if (delayObs_text.getText().isEmpty())
            delayObs_text.setText("0.0");
        int tabidx=configMain.getSelectedIndex();
        double[] resultTab=calcTimeT.CalculateTiming_Tab(time, dspace, Integer.parseInt(repeatsNumb_text.getText()), Double.parseDouble(delayObs_text.getText()));
        int roundval=(int)Math.rint((resultTab[0])*100.d+0.5);
        String roundRes=Double.toString(((double)roundval)/100.d);
        double nachkomma=(resultTab[0]*100.d)%100;
        durationObs_text.setText((int)resultTab[0]+" min "+String.format("%.2f",nachkomma*60.d/100.d)+"sec");
        DiskSpaceText.setText(Double.toString(resultTab[1])+" GB");
    }//GEN-LAST:event_delayObs_textFocusLost

    private void MenuItem_SaveConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItem_SaveConfigActionPerformed
        
        Properties prop = new Properties();
        OutputStream output = null;
        Boolean adv = false;
        
        String filename="";
        JFileChooser chooser = new JFileChooser();
        if (null!=configlocation)
            chooser.setCurrentDirectory(new File(configlocation));
            
        int retrival = chooser.showSaveDialog(null);
        if (retrival == JFileChooser.APPROVE_OPTION) {
            filename = chooser.getSelectedFile().toString();
        }
        File f = new File(filename);
        String fdir=f.getAbsoluteFile().getParent();
        String fname=f.getName();
        configlocation=fdir;
        //System.out.println("debug --- SaveConfigs filedir = "+fdir+" - name="+fname);

        try {
            
        String datename = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss'.txt'").format(new Date());
        prop.setProperty("IPC_version",version_id);
        prop.setProperty("Instrument", "VTF");
        
        String running_time = durationObs_text.getText();
        String running_time_cut = running_time.split(" ")[0];
        String obs_mode = ObsModeGroup.getSelection( ).getActionCommand( );
        
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator('.');
        DecimalFormat df0 = new DecimalFormat("#.####", dfs);
        double cadence0 = 1./(Double.parseDouble(running_time_cut));
        String cadence = String.valueOf(df0.format(cadence0));
        double [] input_val=new double[input_nr_max];
        
        int activeLines = 0;
        /*for (int i = 0; i <maxLines; i++) {
            if (line_active[i] == true) {
                activeLines += 1;
            }
        }*/
        String dt = "";
        String level_of_light = LevelOfLight.getText();
        String datavol = DiskSpaceText.getText();
        String repeatsnumb = repeatsNumb_text.getText();
        String delayObs = delayObs_text.getText();

        boolean writeConfig = true;
        
        if (writeConfig == true) {
            PrintWriter out;
            try {
                out = new PrintWriter(fdir+"/ConfigFile_" + fname);
                output = new FileOutputStream(fdir+"/Config.properties_"+fname);
                
                out.println("Instrument:VTF");
                out.println("instrMode:" + obs_mode);
                //out.println("noLines:" + activeLines);
                
                prop.setProperty("instrMode", obs_mode);
                //prop.setProperty("noLines", Integer.toString(activeLines));
                prop.setProperty("levelOfLight", (level_of_light));
                prop.setProperty("numberOfRepeats", (repeatsnumb));
                prop.setProperty("repeatsDelay", delayObs);
                
                out.println("levelOfLight:" + level_of_light);
                out.println("totalRuntime:" + running_time);
                out.println("totalCadence:" + cadence);
                out.println("dataVolume:" + datavol);
                out.println("numberOfRepeats:" + repeatsnumb);
                out.println("repeatsDelay:" + delayObs);
                switch (configMain.getSelectedIndex()) {
                    case 0:
                        out.println("modus:" + "Default");
                        prop.setProperty("modus", "Default");
                        break;
                    case 1:
                        out.println("modus:" + "Basic");
                        prop.setProperty("modus", "Basic");
                        break;
                    case 2:
                        out.println("modus:" + "Advanced");
                        prop.setProperty("modus", "Advanced");
                        adv = true;
                        break;
                    default:
                        break;
                }
                for (int i = 0; i < maxLines; i++) {
                    switch(configMain.getSelectedIndex()){
                        case 0:
                            input_val = tab0.getLine(i);
                            break;
                        case 1:
                            input_val = tab1.getLine(i);
                            break;
                        case 2:
                            input_val = tab2.getLine(i);
                            break;
                    }
                    
                    if (input_val[0]>0.1) {
                        activeLines += 1;
                        out.println("___________________________________________________");
                        out.println("lineNumber:" + Integer.toString(i+1));
                        prop.setProperty("lineNumber" + i, Integer.toString(i+1));
                        //check if Line_ComboBoxIndex is lower than available number of filters
                        if (nrAvailableFilters<input_val[15]) {
                            out.println("PFname:" + "CustomMode");
                            prop.setProperty("PFname" + i, "CustomMode");
                                
                        } else{
                            String listentry=list0.get((int)input_val[15]-1).get(6);
                            String [] entryparts=listentry.split("\\_");
                            out.println("PFname:" + entryparts[0]);
                            prop.setProperty("PFname" + i, entryparts[0]);
                        }
                            
                        if (adv == true && configMain.getSelectedIndex()==2) {
                            prop.setProperty("userdefScanSteps" + i, tab2.getUserSteps(i));
                        }else{
                            prop.setProperty("userdefScanSteps"+i,"no user steps");
                        }
                        
                        
                        String[] wavelength_string = (String.valueOf(input_val[0])).split("\\.");
                        String decimal_place = wavelength_string[1];
                        if (decimal_place.length() == 1) {
                            decimal_place = decimal_place + "0";
                        }
                        out.println("wavelength:" + wavelength_string[0]+"."+decimal_place);
                        prop.setProperty("wavelength" + i, wavelength_string[0]+"."+decimal_place);
                        switch ((int)input_val[8]) { //binning
                            case 0:
                                out.println("binning:" + "1x1");
                                prop.setProperty("binning" + i, "1x1");
                                break;
                            case 1:
                                out.println("binning:" + "2x2");
                                prop.setProperty("binning" + i, "2x2");
                                break;
                            case 2:
                                out.println("binning:" + "4x4");
                                prop.setProperty("binning" + i, "4x4");
                                break;
                            default:
                                break;
                        }
                        switch ((int)input_val[12]) {  //ROI
                            case 0:
                                out.println("ROI:" + "4128x4104");
                                prop.setProperty("ROI" + i, "4128x4104");
                                break;
                            case 1:
                                out.println("ROI:" + "2048x2048");
                                prop.setProperty("ROI" + i, "2048x2048");
                                break;
                            case 2:
                                out.println("ROI:" + "1024x1024");
                                prop.setProperty("ROI" + i, "1024x1024");
                                break;
                        }
                        switch ((int)input_val[14]) {  //scanning mode
                            case 0:
                                out.println("scanMode:" + "red-blue");
                                prop.setProperty("scanMode" + i, "red-blue");
                                break;
                            case 1:
                                out.println("scanMode:" + "nested");
                                prop.setProperty("scanMode" + i, "nested");
                                break;
                        }
                        out.println("expTime:" + input_val[1]);
                        prop.setProperty("expTime" + i, Double.toString(input_val[1]));
                        if(udscansteps[i]!=1){
                            out.println("stepsLeft:" + input_val[2]);
                            prop.setProperty("stepsLeft" + i, Double.toString(input_val[2]));
                            out.println("stepsRight:" + input_val[17]);
                            prop.setProperty("stepsRight" + i, Double.toString(input_val[17]));
                            out.println("noScanSteps:" + input_val[18]);
                            prop.setProperty("noScanSteps" + i, Integer.toString((int)(input_val[18])));
                        }
                        switch ((int)input_val[16]) {
                            case 0:
                                dt = "x1";
                                break;
                            case 1:
                                dt = "x2";
                                break;
                            case 2:
                                dt = "x3";
                                break;
                        }
                        
                        out.println("spectralStep:" + input_val[3] + " " + dt);
                        out.println("noAcquisitions:" + input_val[4]);  //Acquisitions
                        out.println("SNR:" + input_val[5]);
                        
                        
                        prop.setProperty("spectralStep" + i, Double.toString(input_val[3]));
                        prop.setProperty("doubleSteps" + i, dt);
                        prop.setProperty("noAcquisitions" + i, Integer.toString((int)input_val[4]));
                        prop.setProperty("SNR" + i, Double.toString(input_val[5]));
                        
                        if ("MI".equals(obs_mode)) {
                            out.println("scanPositionInNM:" + input_val[13]);
                            prop.setProperty("scanPositionInNM" + i, Double.toString(input_val[13]));
                        }
                        out.println("iterations:" + input_val[10]);
                        out.println("lineTime:" + input_val[11]);
                        
                        prop.setProperty("iterations" + i, Integer.toString((int)input_val[10]));
                        prop.setProperty("lineTime" + i, Double.toString(input_val[11]));
                        
                    }
                    out.println("noLines:" + activeLines);
                    prop.setProperty("noLines", Integer.toString(activeLines));
                }
            out.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }
        prop.store(output,null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        }

                            
    }//GEN-LAST:event_MenuItem_SaveConfigActionPerformed

    private void HelpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_HelpMouseClicked
        // TODO add your handling code here:
        /*        String filename="";
        JFileChooser filechooser = new JFileChooser();
        int test=filechooser.showOpenDialog(null);

        if (test==JFileChooser.APPROVE_OPTION){
            File file = filechooser.getSelectedFile();
            filename = file.getAbsolutePath();
        }
        //String filepath=
        File file = new File(filename);
        if (file.toString().endsWith(".pdf"))
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
        } catch (IOException ex) {
            Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }//GEN-LAST:event_HelpMouseClicked

    private void writeDebug_checkboxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_writeDebug_checkboxStateChanged
        if (writeDebug_checkbox.isSelected() == true) {
            isselected = true;
        } else {
            isselected = false;
        }        
    }//GEN-LAST:event_writeDebug_checkboxStateChanged

    private void ShowHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShowHelpActionPerformed
        /*
        JTextPane showHtml = new JTextPane();
        showHtml.setEditable(false);
        JScrollPane js = new JScrollPane();
        js.getViewport().add(showHtml);
        JFrame jf = new JFrame();
        jf.getContentPane().add(js);
        jf.pack();
        jf.setSize(800,500);
        jf.setVisible(true);
        if (standalone){
            try {
                URL url = new URL("file:///ATST-KIS-VTF-VTF_IPC_Manual.html");
                showHtml.setPage(url);
            } 
            catch (Exception e) {
                e.printStackTrace();
            }
        }else{
           try {
                URL url = new URL("file:///Users/schubert/projects/vtf_ipc/ATST-KIS-VTF-VTF_IPC_Manual.html");
                showHtml.setPage(url);
            } 
            catch (Exception e) {
                e.printStackTrace();
            } 
        }
        */
        
        JFrame helpframe = new JFrame("Help Text");
        //helpframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JTextArea area = new JTextArea(9, 45);
        String readText="";
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        
        //ArrayList<ArrayList<String>> input0 = null;
        ArrayList<ArrayList<String>> input0= new ArrayList<>();
        
        if (standalone){
            //readList("/", "VTF_IPC_Manual.txt", input0,0);
            readList("/", "ATST-KIS-VTF-VTF_IPC_Manual.txt", input0,0);
        }else{
            File dirhelp=new File("");
            String dirname=dirhelp.getAbsolutePath();
            if (!OpSys.startsWith("Windows")){                
                //readList(dirname+"/", "VTF_IPC_Manual.txt", input0,0);
                readList(dirname+"/", "ATST-KIS-VTF-VTF_IPC_Manual.txt", input0,0);
            }else{
                //readList(dirname+"\\", "VTF_IPC_Manual.txt", input0,0);
                readList(dirname+"\\", "ATST-KIS-VTF-VTF_IPC_Manual.txt", input0,0);
            }
        }
        for (int kk=0;kk<input0.size();kk++){
            for (int ll=0;ll<input0.get(kk).size();ll++){
                readText=readText+(input0.get(kk).get(ll));
            }
            //System.out.println(readText);
            readText=readText+"\n";
            //area.append("\n");
        }
        area.setText(readText);
        helpframe.getContentPane().add(new JScrollPane(area), BorderLayout.CENTER);
        helpframe.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        helpframe.pack();
        helpframe.setVisible(true);
        
    }//GEN-LAST:event_ShowHelpActionPerformed

    private void LevelOfLight_SliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_LevelOfLight_SliderStateChanged
        double output=LevelOfLight_Slider.getValue()/100.d;
        LevelOfLight.setText(Double.toString(output));
        double[] input;
        double result;
        int w_index;
        
        for (int i=0;i<VTF_IPC_GUI.maxLines;i++){
            switch(configMain.getSelectedIndex()){
                case 0:
                    input=tab0.getLine(i);
                    w_index=(int)input[15]-1;
                    result=calcSNR.CalculateSNR_Line(tab0.lineIsCustom(i), w_index, input,output);
                    tab0.setSNR_Tab(i, (int)result);
                    input[5]=result;
                    break;
                case 1:
                    input=tab1.getLine(i);
                    w_index=(int)input[15]-1;
                    result=calcSNR.CalculateSNR_Line(tab1.lineIsCustom(i), w_index, input,output);
                    tab1.setSNR_Tab(i, (int)result);
                    input[5]=result;
                    break;
                case 2:
                    input=tab2.getLine(i);
                    w_index=(int)input[15]-1;
                    result=calcSNR.CalculateSNR_Line(tab2.lineIsCustom(i), w_index, input,output);
                    tab2.setSNR_Tab(i, (int)result);
                    input[5]=result;
                    break;
                default:
                    break;
            }
            //tab0.setLine(i, input);
        }
    }//GEN-LAST:event_LevelOfLight_SliderStateChanged
    
    
    public static int get_wavelength_nr(String wavename){
        int wselect,counter;
        wselect=0;
        counter=0;
        wavename = wavename + "0";
        for (int i=0;i<list0.size();i++){
            //if ((Double.parseDouble(wavename))==(Double.parseDouble(list0.get(i).get(0))))
            if ( wavename.contains(list0.get(i).get(0)) ) {
                wselect=counter;
            }
                counter++;
        }
        return wselect;
    }
    
    
/*    private int get_waveBox_index(String waveset){
        int idx;
        idx=0;
        
        for (int i=0;i<wavetable.length;i++){
            if (wavetable[i].equals(waveset))
                idx=i;
        }
        return idx;
    }
*/                    
    private void addTabListeners(){
        configMain.addChangeListener((ChangeEvent ce) -> {
            int currentTabIndex=configMain.getSelectedIndex();
            //DebugInfos.append("Current tab is: "+currentTabIndex+"\n");
        });
    }
    
    class ScanModeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String choice = ScanModeD.getSelection().getActionCommand();
            //DebugInfos.append("\n Selected Scan Mode: " + choice + "\n");
            out2.append("Selected Scan Mode: " + choice + "\n");
            out2.append(System.getProperty("line.separator"));
            scanmode = ScanModeD.getSelection().getActionCommand();
        }
    }
    

/*    class ObsModeItemListener implements ItemListener {
      @Override
      public void itemStateChanged(ItemEvent ev) {
        boolean selected = (ev.getStateChange( ) == ItemEvent.SELECTED);
        AbstractButton button = (AbstractButton)ev.getItemSelectable( );
        DebugInfos.append("ITEM Choice Selected: " + selected +
                           ", Selection: " + button.getActionCommand( )+"\n");
      }
    }
*/    
    
    public final void readList(String pathname, String filename, ArrayList<ArrayList<String>> input0, int userdef) {
        BufferedReader reader =null;
        InputStream in=null;
        //StringBuffer fileparam = new StringBuffer();
        String text = "";
        String locname="";
        //double[][] list0= new double[6][4];
        String numb;
        int il=0;
        
        int counter = 0;
        
        String path=null;
        
        String target_file=null;  // fileThatYouWantToFilter
        URL res = getClass().getResource("");
        
        /*if (userdef==0){
            for (int i = 0; i < filelist.length; i++) {
                target_file=filelist[i];
                //System.out.println("filelist "+i+": "+target_file);
                if (target_file.matches(filename)) {
                //You can add these files to fileList by using "list.add" here
                     //DebugInfos.append("found" + " " + target_file+"\n");
                     locname=pathname+target_file;
                }
            }
        } else {
            locname=pathname+filename;
        }*/
        
        locname=pathname+filename;
        
        // Scanner in = new Scanner(new FileReader("C:\\Users\\ellwarth\\Documents\\VTF\test_list.txt"));
        try {
            //reader = new BufferedReader(new FileReader(locname));
            if (standalone && userdef==0){
                //System.out.println("startswith jar");
                in=getClass().getResourceAsStream(locname);
                reader=new BufferedReader(new InputStreamReader(in));
            }else{
                reader = new BufferedReader(new FileReader(locname));
            }
            try {
                while ( (text=reader.readLine()) != null){
                    counter+=1;
                    String[] parts = text.split("\\t");
                    int dimp=parts.length;
                    input0.add(new ArrayList<>());
                    
                    for (int j = 0; j < dimp; j++) {
                        //numb = Double.parseDouble(parts[j]);
                        numb = parts[j];
                        input0.get(il).add(String.valueOf(numb));
                    }
                    il +=1;
                    //System.out.println("row (input): "+Integer.toString(il)+"\n");
                }
                reader.close();
                if (standalone && userdef==0){
                    //System.out.println("else 4");
                    in.close();
                    //System.out.println("else 5");
                }
            } catch(IOException e) {
                e.printStackTrace(); 
            }
        } catch(FileNotFoundException fnfe) {
             //System.out.println(fnfe.getMessage());
             out2.append(fnfe.getMessage()+ "\n");
             out2.append(System.getProperty("line.separator"));
        }
    }
    
    public static double[] minmax(double[] iarray){
        double[] minmax=new double[2];
        minmax[0]=0;
        minmax[1]=0;
        for (int i=0;i<iarray.length;i++){
            if(iarray[i]<minmax[0])
                minmax[0]=iarray[i];
            if(iarray[i]>minmax[1])
                minmax[1]=iarray[i];
        }
        return minmax;
    }
    
        /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VTF_IPC_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VTF_IPC_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VTF_IPC_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VTF_IPC_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new VTF_IPC_GUI().setVisible(true);
        });
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        public void run() {
            if (isselected == true) {
            String Debugs_name = new SimpleDateFormat("'Report_'yyyy-MM-dd_HH-mm-ss'.txt'").format(new Date());
            
            try {
                printw = new PrintWriter(Debugs_name);
                String info=out2.toString();
                printw.println(info);
                printw.close();// Do what you want when the application is stopping                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(VTF_IPC_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        //String info=out2.toString();
        //printw.println(info);
        //printw.close();// Do what you want when the application is stopping
        }
        }));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Advanced_Tab;
    private javax.swing.JPanel Basic_Tab;
    private javax.swing.ButtonGroup BinningModeGroup;
    private javax.swing.JPanel Default_Tab;
    protected static javax.swing.JTextField DiskSpaceText;
    protected static javax.swing.JRadioButton Doppler;
    private javax.swing.JMenu Help;
    protected static javax.swing.JRadioButton Intensity;
    public static javax.swing.JTextField LevelOfLight;
    private javax.swing.JSlider LevelOfLight_Slider;
    private javax.swing.JMenuItem MenuItem_OpenConfig;
    private javax.swing.JMenuItem MenuItem_SaveConfig;
    protected static javax.swing.ButtonGroup ObsModeGroup;
    private javax.swing.JPanel ObsModePanelD;
    protected static javax.swing.JRadioButton Polar;
    protected static javax.swing.JTextField ProjectName;
    private javax.swing.JMenuItem ResetGUI_menu;
    private javax.swing.ButtonGroup ScanModeD;
    private javax.swing.JMenuItem ShowHelp;
    private javax.swing.JTabbedPane configMain;
    public static javax.swing.JTextField dataRateDHS_text;
    private javax.swing.JLabel delayObs_label;
    protected static javax.swing.JTextField delayObs_text;
    private javax.swing.JLabel diskSpaceLabel;
    private javax.swing.JLabel duartion_label;
    protected static javax.swing.JTextField durationObs_text;
    protected static javax.swing.JPanel graphicWindow;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel repeatsNumb_label;
    protected static javax.swing.JTextField repeatsNumb_text;
    private javax.swing.JCheckBoxMenuItem writeDebug_checkbox;
    // End of variables declaration//GEN-END:variables
}
