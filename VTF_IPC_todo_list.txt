﻿Änderungen VTF_IPC:

- debug_file raus (report...), eventuell mit radio-Button (in Edit-Menü) oder nur ausgeben, wenn Fehler entsteht
- pdf in ausführbare datei übernehmen....

- poster in A3 oder A2 ausdrucken
- log datei von debug-fenster -> debug fenster raus aus gui -> log-file rausschreiben -> am Ende
- 1 cycle: einmal alle Linien abarbeiten
  -> delay between cycles, data volume 
  -> cycle time 



done:
- not advanced -> back to basic mode
- read config files adv tab -> Momo
- bandwidth double/triple -> Matze (hab ich gemacht[Momo])

- write config
- farben alles hübsch -> Momo
- default line Fe I 630.25 -> Momo
- Scrollbar am rechten Rand rausschmeißen -> Momo
- Größe anpassen (1080,750) -> Momo
- höhe: 768 ->750???  Breite: 1080 
- light level: 0.2-1.0, default 1.0 - caculate_snr  -> Momo

erledigt - delay betw. cycles (s)
erledigt - double spectral width in basic eingestellt wird in advanced das pulldown menue nicht gesetzt
erledigt - advanced: reset -> user def. scan steps set empty
erledigt - total time > 120min -> warnung "flatfield needed" -> am Ende
erledigt - exp time, accumulations, scans steps, snr -> focus lost + action performed (Basic/Advanced) -> Matze
erledigt - Tabwechsel: ComboBox Line soll den entsprecheneden Liniennamen anzeigen -> Matze 
erledigt - user defined scan steps 
erledigt - SNR vorgeben -> exposure time soll maximal bleiben und accumulation runter
erledigt - fast scan mode raus!
erledigt - Achtung: gleiche Linie -> kein Filterwechsel bei Zeitberechnung 
- scan positions in intensity mode werden nicht übernommen
erledigt - Seltsame  Sache:  der „plot Graph“ Knopf bewirkt nichts mehr. Zuletzt hatte ich die Linie 854.2 geplottet, mit 21 Positionen. Musste IPC neu starten, konnte den Fehler aber nicht reproduzieren.

Alex Idee:
- Linienauswahl direkt für jede Linie für Centralwavelength implementieren
- Linien setzten von links nach recht und rechts nach links
- bei jeder Änderung der Eingabewerte calculate + plot
 

erledigt - repeats funktionieren nicht (Basic)

erledigt -- Custom Mode basic/advanced is not editable.... -> Momo

erledigt -- spectral steps double/triple in advanced Funktion implementieren

erledigt -- if Custom Mode is set then set basic/advanced double/triple bandwidth to inactive -> Momo

erledigt -- Fenster für stepwidth sind zu schmal in Basic und Advanced!!! -> MOMO

erledigt -- Absturz nach custom wave und plot versuch->wave-wahl-button darf nicht editierba sein!!!

erledigt -- 656.3 hängt.....

erledigt -- Intensity Mode plot machen...

erledigt -- exposure time max: 25ms -> due to speckle reconstructions

erledigt -- Akkumulationen nur mit Integern

erledigt -- Fast Scan Mode in Default: Accumulations set to 1 in order to decrease cycle time

erledigt -- weniger Nachkommastellen bei den Zeiten

erledigt -- Flatfield beim Timing raus

erledigt - - oben links sollte der Text heißen „Selected VTF mode“: dieser Teil könnte, weil unveränderlich, im grauen Bereich stehen, nur der variable Bereich im weißen Fenster

erledigt - - statt „PF width" würde ich „Filter width“ vorschlagen (oder Prefilter width, falls der Platz reicht).

erledigt - - Bei „user defined scan steps“ würde ich im Eingabefeld schreiben: "enter scan positions in pm rel. to line center, separated by blanks (or commas)"

erledigt - - Abfrage nach Betriebssystem integrieren


erledigt - - im Default mode erscheint im Fenster unten rechts die Meldung „no wave length defined“, sobald man den button „plot graph“ drückt. Die Grafik wird aber korrekt erzeugt.

erledigt - - DEFAULT: es wird immer die Line 1 geplottet, egal, welche Linie angewählt ist (bei BASIC ist das nicht so).

erledigt - - Linie 709.0: Die 12 Scanpunkte liegen komplett neben der Spektrallinie. Vielleicht liegt es daran, dass die Linie bei 709.043 nm liegt?
